import 'dart:async';

import 'package:rxdart/rxdart.dart';

import '../../models3/XCard3.dart';
import '../../services/Session3.dart';
import '../../services/Sorting.dart';
import '../../utils/Logger.dart';

class PickLabelBloc {
  bool get noVaultAtAll => Session3.instance.theVault == null;
  bool get noCardsAtAll => Session3.instance.theVault?.cards.isEmpty ?? true;

  final _labels = BehaviorSubject<List<String>>();
  Stream<List<String>> get labels => _labels;

  late StreamSubscription _s1;

  // ---

  PickLabelBloc() {
    logger.log(Level.debug, '$runtimeType()');

    _s1 = Session3.instance.cardsStream.listen(_onData);
  }

  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    _s1.cancel();
    _labels.close();
  }

  // ---

  void _onData(List<XCard3> cards) {
    logger.log(Level.debug, '$runtimeType._onData()');

    final labels = cards
        .map((c) => c.label)
        .where((label) => label.isNotEmpty)
        .toSet()
        .toList();

    labels.add('');
    labels.sort(Sorting.sortLabels);

    _labels.add(labels);
  }
}
