import 'package:flutter/material.dart';

import '../../utils/Logger.dart';
import '../common/AppBarTitle.dart';
import '../common/EmptyListPane.dart';
import '../common/MyIcons.dart';
import '../common/Resources.dart';
import '../common/Ui.dart';
import 'PickLabelBloc.dart';

class PickLabelWidget extends StatefulWidget {
  PickLabelWidget({super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<StatefulWidget> createState() => _PickLabelState();
}

class _PickLabelState extends State<PickLabelWidget> {
  final _bloc = PickLabelBloc();

  _PickLabelState() {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    logger.log(Level.debug, '$runtimeType.build()');

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        title: const AppBarTitle(
          Resources.selectLabelTitle,
        ),
      ),
      body: SafeArea(
        child: _body(),
      ),
    );
  }

  Widget _body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          child: _listBuilder(),
        ),
      ],
    );
  }

  Widget _listBuilder() {
    return StreamBuilder(
      stream: _bloc.labels,
      builder: (context, AsyncSnapshot<List<String>> snapshot) {
        return _labelListBuilder(context, snapshot);
      },
    );
  }

  Widget _labelListBuilder(
      BuildContext context, AsyncSnapshot<List<String>> snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      logger.log(Level.debug, '$runtimeType._labelListBuilder() - waiting');
      return Ui.blankWidget();
    }

    return snapshot.data?.isNotEmpty == true
        ? _labelList(snapshot.data!)
        : _bloc.noCardsAtAll
            ? _noCards()
            : _noLabels();
  }

  Widget _noLabels() {
    return EmptyListPane(
      title: Resources.noLabelsTitle,
      text: Resources.addFirstCardText,
      child: Icon(
        MyIcons.label,
        size: 64.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  Widget _noCards() {
    return EmptyListPane(
      title: Resources.noCardsTitle,
      text: Resources.addFirstCardText,
      child: Icon(
        MyIcons.card,
        size: 64.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  ListView _labelList(List<String> labels) {
    logger.log(Level.debug, '$runtimeType._list() - ${labels.length}');

    return ListView.builder(
      itemCount: labels.length,
      itemBuilder: (c, i) => _tile(labels[i]),
    );
  }

  Widget _tile(String label) {
    return ListTile(
      leading: label == ''
          ? const Icon(null)
          : Icon(
              MyIcons.label,
              color: Theme.of(context).colorScheme.primary,
            ),
      title: Text(
        label,
        style: Theme.of(context).textTheme.titleLarge,
      ),
      onTap: () => _onTapLabel(label),
    );
  }

  void _onTapLabel(String label) {
    Navigator.of(context).pop(label);
  }
}
