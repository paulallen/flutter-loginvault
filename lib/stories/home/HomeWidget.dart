import 'dart:async';

import 'package:flutter/material.dart';

import '../../models3/Vault3.dart';
import '../../services/Session3.dart';
import '../../utils/Logger.dart';
import '../common/BiometricsPermissionDialog.dart';
import '../common/Dialogs.dart';
import '../common/Resources.dart';
import '../common/Ui.dart';
import '../password/SetPasswordWidget.dart';
import '../password/EnterPasswordWidget.dart';
import '../routes/Routes.dart';
import '../vault/VaultWidget.dart';
import 'HomeBloc.dart';

class HomeWidget extends StatefulWidget {
  HomeWidget({super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<HomeWidget> with WidgetsBindingObserver {
  static final _dialogs = Dialogs();

  final _bloc = HomeBloc();

  static StreamSubscription? _s1;

  _HomeState() {
    logger.log(Level.debug, '$runtimeType()');

    _s1 ??= _bloc.messages.listen(_showMessage);

    WidgetsBinding.instance.addObserver(this);

    WidgetsBinding.instance.addPostFrameCallback((_) => _forceVault());
  }

  @override
  void initState() {
    logger.log(Level.debug, '$runtimeType.initState()');
    super.initState();
  }

  @override
  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    WidgetsBinding.instance.removeObserver(this);
    _s1?.cancel();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    logger.log(Level.debug, '$runtimeType.didChangeAppLifecycleState($state)');
    super.didChangeAppLifecycleState(state);

    if (state == AppLifecycleState.paused) {
      _bloc.closeit();
      Navigator.of(context).pushNamedAndRemoveUntil(
        Routes.home,
        (route) => false,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    logger.log(Level.debug, '$runtimeType.build()');

    if (_bloc.vault == null) {
      logger.log(Level.warning, '$runtimeType.build() - vault is null');
      WidgetsBinding.instance.addPostFrameCallback((_) => _forceVault());
    }

    return StreamBuilder(
      stream: Session3.instance.vaultStream,
      builder: (context, AsyncSnapshot<Vault3?> snapshot) {
        return snapshot.data == null //
            ? _noVault()
            : VaultWidget();
      },
    );
  }

  Widget _noVault() {
    return TextButton(
      onPressed: _forceVault,
      child: const Text('go...'),
    );
  }

  void _showMessage(String message) {
    logger.log(Level.debug, '$runtimeType.showMessage() - $message');

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: const Duration(seconds: 1),
      ),
    );
  }

  // --

  static bool isInForceVault = false;

  Future<void> _forceVault() async {
    logger.log(Level.debug, '$runtimeType._forceVault()');

    try {
      if (isInForceVault) {
        logger.log(Level.warning, '$runtimeType._forceVault() - prevented!');
        return;
      }

      isInForceVault = true;

      final filePath = await _pickExistingFile();
      if (filePath != null) {
        await _openExistingFile(filePath);
      } else {
        await _createNewVault();
      }
    } catch (e, s) {
      logger.logException(Level.warning, '$runtimeType._forceVault()', e, s);
      await _dialogs.showErrorDialog(context, Resources.loadFailMessage);
    } finally {
      isInForceVault = false;
    }
  }

  Future<String?> _pickExistingFile() async {
    if (!Ui.kIsSingleVaultOnly) {
      return await Navigator.of(context).pushNamed(
        Routes.pickfile,
      ) as String?;
    } else {
      final filePath = _bloc.toVaultPath(Ui.kSingleVaultName);
      final exists = await _bloc.exists(filePath);
      return exists ? filePath : null;
    }
  }

  Future<void> _openExistingFile(String filePath) async {
    logger.log(Level.debug, '$runtimeType._openExistingVault()');

    final vaultName = _bloc.toVaultName(filePath);

    final result = await Navigator.of(context).pushNamed(
      Routes.enterpassword,
      arguments: EnterPasswordParameters(vaultName, filePath),
    ) as EnterPasswordResult?;

    if (result == null) {
      return;
    }

    await _bloc.openit(
      result.vault,
      result.filePath,
      result.password,
    );

    await _maybeStorePassword(filePath, result.password);
  }

  Future<void> _createNewVault() async {
    logger.log(Level.debug, '$runtimeType._createNewVault()');

    final vaultName = Ui.kIsSingleVaultOnly ? Ui.kSingleVaultName : '';

    final result = await Navigator.of(context).pushNamed(
      Routes.setpassword,
      arguments: vaultName,
    ) as SetPasswordResult?;

    if (result == null) {
      return;
    }

    await _bloc.createit(
      result.vaultName,
      result.password,
    );

    final filePath = _bloc.toVaultPath(vaultName);
    await _maybeStorePassword(filePath, result.password);
  }

  Future<void> _maybeStorePassword(String filePath, String password) async {
    final storedPassword = await _bloc.readPassword(filePath);
    final useBiometrics = storedPassword == null //
        ? await _askToUseBiometrics() //
        : true;

    if (useBiometrics) {
      await _bloc.writePassword(filePath, password);
    }
  }

  Future<bool> _askToUseBiometrics() async {
    final canWe = await _bloc.canWeUseBiometrics();

    if (canWe && context.mounted) {
      final dialog = BiometricsPermissionDialog();
      return dialog.askToUseBiometrics(context);
    } else {
      return false;
    }
  }
}
