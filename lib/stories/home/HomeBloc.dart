import '../../models3/Vault3.dart';
import '../../services/Biometrics.dart';
import '../../services/FileIo.dart';
import '../../services/NameMapper.dart';
import '../../services/SecureStorage.dart';
import '../../services/Session3.dart';
import '../../services/VaultIo3.dart';
import '../../utils/Logger.dart';

class HomeBloc {
  static final _fileIo = FileIo();
  static final _vaultio3 = VaultIo3();
  static final _nameMapper = NameMapper();
  static final _biometrics = Biometrics();
  static final _secureStorage = SecureStorage();

  Stream<String> get messages => Session3.instance.messages;

  Vault3? get vault => Session3.instance.theVault;

  String toVaultPath(String vaultName) {
    return _nameMapper.toFilePath(Session3.instance.thePath, vaultName);
  }

  String toVaultName(String filePath) {
    return _nameMapper.toVaultName(filePath);
  }

  Future<String?> readPassword(String filePath) async {
    return _secureStorage.read(filePath);
  }

  Future<void> writePassword(String filePath, String password) async {
    return _secureStorage.write(
      filePath,
      password,
    );
  }

  Future<bool> canWeUseBiometrics() async {
    final biometrics = await _biometrics.canUseBiometrics();
    final storage = await _secureStorage.canUseStorage();
    return biometrics && storage;
  }

  Future<bool> exists(String filePath) async {
    return _fileIo.exists(filePath);
  }

  Future<void> openit(
    Vault3 vault,
    String filePath,
    String password,
  ) async {
    logger.log(Level.debug, '$runtimeType.openit($filePath)');

    final vaultName = _nameMapper.toVaultName(filePath);

    Session3.instance.openVault(filePath, password, vaultName, vault);
  }

  Future<void> createit(
    String vaultName,
    String password,
  ) async {
    logger.log(Level.debug, '$runtimeType.createit($vaultName)');

    final filePath =
        _nameMapper.toFilePath(Session3.instance.thePath, vaultName);

    final vault = Vault3();
    await _vaultio3.saveVault(filePath, vault, password);

    Session3.instance.openVault(filePath, password, vaultName, vault);
  }

  Future<void> closeit() async {
    logger.log(Level.debug, '$runtimeType.closeit()');

    await Session3.instance.closeVault();
  }
}
