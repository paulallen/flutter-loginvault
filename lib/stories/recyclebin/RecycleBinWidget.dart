import 'package:flutter/material.dart';

import '../../models3/XCard3.dart';
import '../../services/Session3.dart';
import '../../utils/Logger.dart';
import '../card/CardWidget.dart';
import '../common/CardLoginTile.dart';
import '../common/Dialogs.dart';
import '../common/EmptyListPane.dart';
import '../common/MyIcons.dart';
import '../common/Resources.dart';
import '../common/Ui.dart';
import '../routes/Routes.dart';
import 'RecycleBinBloc.dart';

class RecycleBinWidget extends StatefulWidget {
  RecycleBinWidget({super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<RecycleBinWidget> createState() => _RecycleBinState();
}

class _RecycleBinState extends State<RecycleBinWidget> {
  static final _dialogs = Dialogs();

  final _bloc = RecycleBinBloc();

  _RecycleBinState() {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // logger.log(Level.debug, '$runtimeType.build()');

    return Scaffold(
      body: SafeArea(
        child: _body(),
      ),
    );
  }

  Widget _body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          child: _listBuilder(),
        ),
      ],
    );
  }

  Widget _listBuilder() {
    return StreamBuilder(
      stream: _bloc.cards,
      builder: _cardListBuilder,
    );
  }

  Widget _cardListBuilder(
      BuildContext context, AsyncSnapshot<List<XCard3>> snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      logger.log(Level.debug, '$runtimeType._cardListBuilder() - waiting');
      return Ui.blankWidget();
    }

    return snapshot.data?.isNotEmpty == true
        ? _list(snapshot.data!)
        : _bloc.noCardsAtAll
            ? _noCards()
            : _noDeletedCards();
  }

  Widget _noDeletedCards() {
    return EmptyListPane(
      title: Resources.noDeletedCardsTitle,
      child: Icon(
        MyIcons.card,
        size: 64.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  Widget _noCards() {
    return EmptyListPane(
      title: Resources.noCardsTitle,
      child: Icon(
        MyIcons.card,
        size: 64.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  Widget _list(List<XCard3> cards) {
    return ListView.builder(
      itemCount: cards.length,
      itemBuilder: (c, i) => _cardTile(cards[i]),
    );
  }

  Widget _cardTile(XCard3 card) {
    return CardLoginTile(
      card,
      onTap: () => _editCard(card, isEdit: false),
    );
  }

  Future<void> _editCard(XCard3 card, {required bool isEdit}) async {
    try {
      Session3.instance.theCard = card;

      await Navigator.of(context).pushNamed(
        Routes.card,
        arguments: CardParameters(
          isEdit: isEdit,
        ),
      );
    } catch (e, s) {
      logger.logException(Level.warning, '$runtimeType._editCard()', e, s);
      await _dialogs.showErrorDialog(context, e.toString());
    }
  }
}
