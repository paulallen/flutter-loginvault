import 'dart:async';

import 'package:rxdart/rxdart.dart';

import '../../models3/XCard3.dart';
import '../../utils/Logger.dart';
import '../../services/Session3.dart';

class RecycleBinBloc {
  bool get noVaultAtAll => Session3.instance.theVault == null;
  bool get noCardsAtAll => Session3.instance.theVault?.cards.isEmpty ?? true;

  final _cardVms = BehaviorSubject<List<XCard3>>();
  Stream<List<XCard3>> get cards => _cardVms;

  late StreamSubscription _s1;

  // ---

  RecycleBinBloc() {
    logger.log(Level.debug, '$runtimeType()');

    _s1 = Session3.instance.cardsStream.listen(_onData);
  }

  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    _s1.cancel();
    _cardVms.close();
  }

  // ---

  void _onData(List<XCard3> cards) {
    logger.log(Level.debug, '$runtimeType._onData()');

    final deletedCards = cards.where((c) => c.isDeleted).toList();
    _cardVms.add(deletedCards);
  }
}
