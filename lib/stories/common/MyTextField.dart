import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'MyIcons.dart';
import 'Ui.dart';

class MyTextField extends StatefulWidget {
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final void Function(String)? onChanged;
  final TextStyle? style;
  final String? hintText;
  final List<TextInputFormatter>? inputFormatters;
  final bool autofocus;

  MyTextField({
    Key? key,
    this.controller,
    this.keyboardType,
    this.onChanged,
    this.style,
    this.hintText,
    this.inputFormatters,
    this.autofocus = false,
  }) : super(key: key) {
    assert(controller != null);
  }

  @override
  State<StatefulWidget> createState() => MyTextFieldState();
}

class MyTextFieldState extends State<MyTextField> {
  final _focusNode = FocusNode();
  bool _canClear = false;

  MyTextFieldState();

  @override
  Widget build(BuildContext context) {
    final hasFocus = _focusNode.hasFocus;

    return TextField(
      focusNode: _focusNode,
      autofocus: widget.autofocus,
      controller: widget.controller,
      keyboardType: widget.keyboardType,
      key: widget.key,
      decoration: InputDecoration(
        isDense: true,
        hintText: widget.hintText,
        suffixIcon: hasFocus
            ? IconButton(
                icon: const Icon(MyIcons.clear),
                onPressed:
                    _canClear ? () => _clearTextAndTriggerOnChanged() : null,
              )
            : const SizedBox.square(
                dimension: Ui.kIconButtonDimension,
              ),
      ),
      style: widget.style,
      onChanged: _onChanged,
      inputFormatters: widget.inputFormatters,
    );
  }

  void _onChanged(String value) {
    if (widget.onChanged != null) {
      widget.onChanged!(value);
    }

    _updateCanClear(value);
  }

  void _clearTextAndTriggerOnChanged() {
    if (widget.controller != null) {
      widget.controller!.clear();
    }

    if (widget.onChanged != null) {
      widget.onChanged!('');
    }

    _updateCanClear('');
  }

  void _updateCanClear(String text) {
    setState(() {
      _canClear = text.isNotEmpty;
    });
  }
}
