import 'package:flutter/material.dart';

import 'Resources.dart';

class ErrorDialog extends StatelessWidget {
  final String text;
  final void Function() onPressed;

  const ErrorDialog(this.text, {super.key, required this.onPressed});

  @override
  Widget build(BuildContext context) => AlertDialog(
        content: Text(text),
        actions: <Widget>[
          TextButton(
            onPressed: onPressed,
            child: const Text(Resources.ok),
          ),
        ],
      );
}
