class Resources {
  static const String appTitle = 'LoginVault';

  static const String enterPasswordTitle = 'Enter the password';
  static const String vaultNameLabel = 'Vault name';
  static const String passwordLabel = 'Password';

  static const String setPasswordTitle = 'Set a password';
  static const String repeatPasswordLabel = 'Repeat the password';

  static const String searchPageTitle = 'Search';
  static const String labelsPageTitle = 'Labels';
  static const String recyclingPageTitle = 'Recycling';
  static const String settingsPageTitle = 'Settings';

  static const String selectIconTitle = 'Select Icon';
  static const String selectLabelTitle = 'Select Label';
  static const String selectUsernameTitle = 'Select Username';

  static const String noVaultsTitle = 'There are no vaults here.';
  static const String addFirstVaultText = 'Add a vault with the \'+\' button.';

  static const String noLabelsTitle = 'There are no cards with labels.';
  static const String noUsernamesTitle = 'There are no cards with usernames.';
  static const String noCardsTitle = 'There are no cards here.';
  static const String noDeletedCardsTitle = 'There are no deleted cards.';
  static const String noSearchFilterTitle = 'Type some text to find cards.';
  static const String noMatchingCardsTitle = 'There are no matching cards.';
  static const String addFirstCardText = 'Add a card with the \'+\' button.';

  static const String searchEditHint = 'Type to search';

  static const String colourLabel = 'Primary colour';
  static const String themeLabel = 'Theme';
  static const String darkThemeLabel = 'Dark';
  static const String lightThemeLabel = 'Light';

  static const String viewCardTitle = 'View';
  static const String editCardTitle = 'Edit';

  static const String editUrlLabel = 'Url';
  static const String editUsernameLabel = 'Username';
  static const String editPasswordLabel = passwordLabel;
  static const String editRepeatLabel = repeatPasswordLabel;
  static const String editLabelLabel = 'Label';
  static const String editIconLabel = 'Icon';
  static const String editNotesLabel = 'Notes';

  static const String pickUsernameButtonText = 'Usernames...';
  static const String pickIconButtonText = 'Icons...';
  static const String pickLabelButtonText = 'Labels...';

  static const String urlEmptyValidation = 'Url is mandatory';
  static const String urlWhitespaceValidation = 'Url cannot be blank';
  static const String vaultNameEmptyValidation = 'Name is mandatory';
  static const String vaultNameWhitespaceValidation = 'Name cannot be blank';
  static const String passwordEmptyValidation = 'Password is mandatory';
  static const String repeatNotSameValidation = 'Must match password';

  static const String savedVaultMessage = 'Saved vault!';
  static const String copiedPasswordMessage = 'Copied to Clipboard';

  static const String biometricsButtonText = 'biometrics...';

  static const String no = 'no';
  static const String yes = 'yes';
  static const String ok = 'ok';
  static const String close = 'close';
  static const String delete = 'delete';
  static const String cancel = 'cancel';

  static const String useBiometricsQuestion =
      'Do you want to use biometrics to open this vault?';
  static const String biometricsPrompt = 'To unlock this vault';
  static const String confirmDeleteQuestion =
      'Are you sure you want to delete this vault?';
  static const loadFailMessage =
      'That was not the correct password.  Please try again.';
}
