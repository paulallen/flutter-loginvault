import 'package:flutter/material.dart';

import '../../models3/XCard3.dart';
import 'IconNameData.dart';

class CardLoginTile extends StatelessWidget {
  final XCard3 card;
  final void Function() onTap;

  const CardLoginTile(
    this.card, {super.key, 
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final title = Text(
      card.url,
      style: Theme.of(context).textTheme.titleLarge,
      maxLines: 1,
      overflow: TextOverflow.fade,
    );

    final subtitle =
        Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
      Expanded(
        child: Text(
          card.username,
          textAlign: TextAlign.end,
          style: Theme.of(context).textTheme.bodyMedium,
          softWrap: false,
          maxLines: 1,
          overflow: TextOverflow.fade,
        ),
      ),
      const SizedBox(
        width: 8.0,
      ),
    ]);

    return ListTile(
      leading: Icon(
        _getIconData(card.icon),
        color: Theme.of(context).colorScheme.primary,
      ),
      title: title,
      subtitle: subtitle,
      onTap: onTap,
    );
  }

  static IconData? _getIconData(String name) {
    final data = IconNameData.getIconNameData(name);
    return data?.iconData;
  }
}
