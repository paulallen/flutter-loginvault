import 'package:flutter/material.dart';

import '../common/MyIcons.dart';
import '../common/Resources.dart';

class BiometricsPermissionDialog {
  Future<bool> askToUseBiometrics(BuildContext context) async {
    final useThem = await showDialog(
      barrierDismissible: false,
      barrierColor: const Color.fromARGB(128, 128, 128, 128),
      context: context,
      builder: (c) => AlertDialog(
        title: const Text(Resources.useBiometricsQuestion),
        content: const Padding(
          padding: EdgeInsets.all(12),
          child: Icon(
            MyIcons.fingerprint,
            size: 48,
          ),
        ),
        actions: [
          TextButton(
            child: const Text(Resources.no),
            onPressed: () => Navigator.of(c).pop(false),
          ),
          TextButton(
            child: const Text(Resources.yes),
            onPressed: () => Navigator.of(c).pop(true),
          ),
        ],
      ),
    ) as bool;

    return useThem;
  }
}
