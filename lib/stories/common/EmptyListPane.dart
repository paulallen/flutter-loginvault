import 'package:flutter/material.dart';

class EmptyListPane extends StatelessWidget {
  final Widget child;
  final String title;
  final String text;

  EmptyListPane({super.key, required this.child, required this.title, this.text = ''}) {
    assert(title.isNotEmpty);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          child,
          Text(title, style: Theme.of(context).textTheme.titleLarge),
          Text(text, style: Theme.of(context).textTheme.bodyMedium),
        ],
      ),
    );
  }
}
