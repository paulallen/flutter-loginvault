import 'package:flutter/material.dart';

class MyIcons {
  static const IconData add = Icons.add;
  static const IconData arrowBack = Icons.arrow_back_outlined;
  static const IconData arrowUp = Icons.arrow_upward_outlined;
  static const IconData clear = Icons.clear;
  static const IconData card = Icons.credit_card;
  static const IconData copy = Icons.content_copy;
  static const IconData label = Icons.label_outline;
  static const IconData delete = Icons.delete_outline;
  static const IconData deleteForever = Icons.delete_forever_outlined;
  static const IconData done = Icons.done;
  static const IconData edit = Icons.edit;
  static const IconData enlarge = Icons.aspect_ratio_outlined;
  static const IconData fingerprint = Icons.fingerprint_outlined;
  static const IconData folder = Icons.folder_outlined;
  static const IconData remove = Icons.highlight_remove_outlined;
  static const IconData undelete = Icons.restore_from_trash_outlined;
  static const IconData search = Icons.search;
  static const IconData settings = Icons.settings_outlined;
  static const IconData username = Icons.person_outline;
  static const IconData visibilityOn = Icons.visibility_outlined;
  static const IconData visibilityOff = Icons.visibility_off_outlined;
}
