import 'Resources.dart';

class Validators {
  static String? vaultNameValidator(String? name) {
    return name == null || name.isEmpty
        ? Resources.vaultNameEmptyValidation
        : name.trim().isEmpty
            ? Resources.vaultNameWhitespaceValidation
            : null;
  }

  static String? urlValidator(String? url) {
    return url == null || url.isEmpty
        ? Resources.urlEmptyValidation
        : url.trim().isEmpty
            ? Resources.urlWhitespaceValidation
            : null;
  }

  static String? passwordValidator(String? password) {
    return password == null || password.isEmpty
        ? Resources.passwordEmptyValidation
        : null;
  }

  static String? repeatValidator(String? repeat, String password) {
    return repeat != password ? Resources.repeatNotSameValidation : null;
  }
}
