import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  final Icon icon;
  final void Function()? onPressed;

  const ActionButton({super.key, required this.icon, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: icon,
      color: Colors.white,
      onPressed: onPressed,
    );
  }
}
