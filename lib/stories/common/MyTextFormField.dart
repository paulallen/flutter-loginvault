import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'MyIcons.dart';
import 'Ui.dart';

class MyTextFormField extends StatefulWidget {
  final bool autocorrect;
  final bool autofocus;
  final bool readOnly;
  final bool obscureText;
  final int maxLines;
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final String? Function(String?)? validator;
  final void Function(String)? onChanged;
  final TextStyle? style;
  final String? hintText;
  final List<TextInputFormatter>? inputFormatters;

  MyTextFormField({
    Key? key,
    this.autofocus = false,
    this.autocorrect = false,
    this.readOnly = false,
    this.obscureText = false,
    this.maxLines = 1,
    this.controller,
    this.keyboardType,
    this.validator,
    this.onChanged,
    this.style,
    this.hintText,
    this.inputFormatters,
  }) : super(key: key) {
    assert(controller != null);
  }

  @override
  State<StatefulWidget> createState() => MyTextFormFieldState();
}

class MyTextFormFieldState extends State<MyTextFormField> {
  final _focusNode = FocusNode();
  bool _canClear = false;

  MyTextFormFieldState();

  @override
  Widget build(BuildContext context) {
    // Is this only needed when we setState() them in the container?
    final readonly = widget.readOnly;
    final obscureText = widget.obscureText;
    final hasFocus = _focusNode.hasFocus;

    return TextFormField(
      focusNode: _focusNode,
      autocorrect: widget.autocorrect,
      autofocus: widget.autofocus,
      readOnly: readonly,
      obscureText: obscureText,
      maxLines: widget.maxLines,
      controller: widget.controller,
      keyboardType: widget.keyboardType,
      key: widget.key,
      validator: widget.validator,
      decoration: InputDecoration(
        isDense: true,
        hintText: widget.hintText,
        border: Ui.inputBorder(isReadOnly: readonly),
        suffixIcon: !readonly && hasFocus
            ? IconButton(
                icon: const Icon(MyIcons.clear),
                onPressed: !readonly && _canClear
                    ? () => _clearTextAndTriggerOnChanged()
                    : null,
              )
            : const SizedBox.square(
                dimension: Ui.kIconButtonDimension,
              ),
      ),
      style: widget.style,
      onChanged: _onChanged,
      inputFormatters: widget.inputFormatters,
    );
  }

  void _onChanged(String value) {
    if (widget.onChanged != null) {
      widget.onChanged!(value);
    }

    _updateCanClear(value);
  }

  void _clearTextAndTriggerOnChanged() {
    if (widget.controller != null) {
      widget.controller!.clear();
    }

    if (widget.onChanged != null) {
      widget.onChanged!('');
    }

    _updateCanClear('');
  }

  void _updateCanClear(String text) {
    setState(() {
      _canClear = text.isNotEmpty;
    });
  }
}
