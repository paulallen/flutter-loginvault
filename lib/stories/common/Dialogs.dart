import 'package:flutter/material.dart';

import 'ErrorDialog.dart';

class Dialogs {
  Future showErrorDialog(BuildContext context, String text) {
    return showDialog<void>(
      context: context,
      builder: (c) => ErrorDialog(
        text,
        onPressed: () => Navigator.of(c).pop(),
      ),
    );
  }
}
