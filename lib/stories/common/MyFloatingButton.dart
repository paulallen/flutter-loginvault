import 'package:flutter/material.dart';

class MyFloatingButton extends StatelessWidget {
  final IconData icon;
  final void Function() onPressed;

  const MyFloatingButton({super.key, required this.icon, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: Theme.of(context).colorScheme.primary,
      onPressed: onPressed,
      child: Icon(
        icon,
        color: Colors.white,
      ),
    );
  }
}
