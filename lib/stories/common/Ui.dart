import 'dart:io';

import 'package:flutter/material.dart';

import 'MyIcons.dart';

class Ui {
  static const int kMaxFieldLength = 512;
  static const int kMaxOtherLength = 4096;

  static const String kPrimaryColourKey = 'primaryColour';

  static final kIsSingleVaultOnly = !Platform.isWindows;
  static const kSingleVaultName = 'My Login Vault';

  static const double kThemeImageWidth = 108.0;
  static const double kIconButtonDimension = 32;
  static const double kHsvSaturation = 0.8;
  static const double kHsvValueLight = 0.5;
  static const double kHsvValueThreshold = 0.6;
  static const double kHsvValueDark = 0.8;

  static const String fontFamily = 'Quicksand';
  static const String darkImage = 'lib/assets/images/theme-BnY-108x192.png';
  static const String lightImage = 'lib/assets/images/theme-WnB-108x192.png';
  static const String vaultImage21x24 = 'lib/assets/images/vault-21x24.png';
  static const String vaultImage100x100 = 'lib/assets/images/vault-100x100.png';

  static final HSVColor goldenrod =
      HSVColor.fromColor(const Color.fromARGB(255, 218, 165, 32));

  static const EdgeInsets insets5AboveBelow =
      EdgeInsets.symmetric(vertical: 5.0);

  static const EdgeInsets insets16LeftAboveRight =
      EdgeInsets.only(left: 16, top: 16, right: 16);

  static InputBorder inputBorder({required bool isReadOnly}) {
    return isReadOnly ? InputBorder.none : const UnderlineInputBorder();
  }

  static Widget blankWidget() => const SizedBox.shrink();

  static Icon upIcon(BuildContext context) {
    return const Icon(
      MyIcons.arrowUp,
      color: Colors.white,
    );
  }

  static IconButton backIconButton(BuildContext context) {
    return IconButton(
      icon: Ui._backIcon(context),
      onPressed: () => Navigator.of(context).maybePop(),
    );
  }

  static Icon _backIcon(BuildContext context) {
    return const Icon(
      MyIcons.arrowBack,
      color: Colors.white,
    );
  }

  static double valueForBrightness(Brightness brightness) {
    return brightness == Brightness.dark ? kHsvValueDark : kHsvValueLight;
  }

  static Brightness brightnessForValue(double value) {
    return value > kHsvValueThreshold ? Brightness.dark : Brightness.light;
  }

  static ThemeData buildThemeData(HSVColor hsvColour) {
    final brightness = brightnessForValue(hsvColour.value);

    final basicColorScheme = brightness == Brightness.dark
        ? const ColorScheme.dark()
        : const ColorScheme.light();

    final rgbColour = hsvColour.toColor();

    final colorScheme = basicColorScheme.copyWith(
      primary: rgbColour,
    );

    final textSelectionThemeData = TextSelectionThemeData(
      selectionHandleColor: rgbColour,
      cursorColor: rgbColour,
    );

    return ThemeData(
      colorScheme: colorScheme,
      fontFamily: fontFamily,
      textSelectionTheme: textSelectionThemeData,
      // toggleableActiveColor: rgbColour,
    );
  }
}
