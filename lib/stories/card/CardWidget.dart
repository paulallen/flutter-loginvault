import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../utils/Logger.dart';
import '../../stories/routes/Routes.dart';
import '../../services/Session3.dart';
import '../../stories/common/Resources.dart';
import '../common/ActionButton.dart';
import '../common/AppBarTitle.dart';
import '../common/Dialogs.dart';
import '../common/Label.dart';
import '../common/MyFloatingButton.dart';
import '../common/MyIcons.dart';
import '../common/MyTextFormField.dart';
import '../common/IconNameData.dart';
import '../common/Ui.dart';
import '../common/Validators.dart';
import 'CardBloc.dart';
import 'ViewPasswordDialog.dart';

class CardParameters {
  final bool isEdit;

  CardParameters({required this.isEdit});
}

class CardWidget extends StatefulWidget {
  final bool isEdit;
  final String? icon;

  CardWidget(CardParameters params, {super.key})
      : isEdit = params.isEdit,
        icon = Session3.instance.theCard!.icon {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<StatefulWidget> createState() => _CardState();
}

class _CardState extends State<CardWidget> {
  static final _dialogs = Dialogs();

  final _bloc = CardBloc();

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  late TextEditingController _urlController;
  late TextEditingController _usernameController;
  late TextEditingController _passwordController;
  late TextEditingController _repeatController;
  late TextEditingController _labelController;
  late TextEditingController _notesController;

  final _urlKey = UniqueKey();
  final _usernameKey = UniqueKey();
  final _passwordKey = UniqueKey();
  final _repeatKey = UniqueKey();
  final _labelKey = UniqueKey();
  final _notesKey = UniqueKey();

  bool _isEdit = false;
  bool _isRevealed = false;
  IconNameData? _theIconData;

  _CardState();

  @override
  void initState() {
    logger.log(Level.debug, '$runtimeType.initState()');
    super.initState();

    _urlController =
        TextEditingController(text: Session3.instance.theCard!.url);
    _usernameController =
        TextEditingController(text: Session3.instance.theCard!.username);
    _passwordController =
        TextEditingController(text: Session3.instance.theCard!.password);
    _repeatController =
        TextEditingController(text: Session3.instance.theCard!.password);
    _labelController =
        TextEditingController(text: Session3.instance.theCard!.label);
    _notesController =
        TextEditingController(text: Session3.instance.theCard!.other);

    _isEdit = widget.isEdit;
  }

  @override
  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    _urlController.dispose();
    _usernameController.dispose();
    _passwordController.dispose();
    _repeatController.dispose();
    _labelController.dispose();
    _notesController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    logger.log(Level.debug, '$runtimeType.build()');

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        leading: Ui.backIconButton(context),
        title: AppBarTitle(
          _isEdit ? Resources.editCardTitle : Resources.viewCardTitle,
        ),
        actions: <Widget>[
          if (!_isEdit && _bloc.card.isDeleted) _undeleteAction(),
          if (!_isEdit && _passwordController.text != '') _copyAction(),
          if (_isEdit) _deleteAction(),
          if (_isEdit) _revealAction(),
          if (!_isEdit) _toggleEditAction(),
          // _toggleEditAction(),
        ],
      ),
      body: SafeArea(
        child: _body(),
      ),
      floatingActionButton: _floatingButton(),
    );
  }

  Widget? _floatingButton() {
    return _isEdit
        ? MyFloatingButton(
            icon: MyIcons.done,
            onPressed: _onPressedSubmit,
          )
        : _passwordController.text != ''
            ? MyFloatingButton(
                icon: MyIcons.enlarge,
                onPressed: _onPressedEnlarge,
              )
            : null;
  }

  Widget _deleteAction() {
    return ActionButton(
      icon: _bloc.card.isDeleted
          ? const Icon(
              MyIcons.deleteForever,
            )
          : const Icon(
              MyIcons.delete,
            ),
      onPressed: _onPressedDelete,
    );
  }

  Widget _undeleteAction() {
    return ActionButton(
      icon: const Icon(
        MyIcons.undelete,
      ),
      onPressed: _onPressedUndelete,
    );
  }

  Widget _copyAction() {
    return ActionButton(
      icon: const Icon(
        MyIcons.copy,
      ),
      onPressed: _onPressedCopy,
    );
  }

  Widget _revealAction() {
    return ActionButton(
      icon: Icon(_isRevealed ? MyIcons.visibilityOff : MyIcons.visibilityOn),
      onPressed: _setReveal,
    );
  }

  void _setReveal() {
    setState(() {
      _isRevealed = !_isRevealed;
    });
  }

  Widget _toggleEditAction() {
    return ActionButton(
      icon: const Icon(
        MyIcons.edit,
      ),
      onPressed: _onPressedToggleEdit,
    );
  }

  Widget _body() {
    final showRepeat = _isEdit && !_isRevealed;

    return Padding(
      padding: Ui.insets16LeftAboveRight,
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Label(Resources.editUrlLabel),
              _urlField(),
              _pair(const Label(Resources.editUsernameLabel)),
              _usernameField(),
              _pair(
                const Label(Resources.editPasswordLabel),
                button: _pickUsernameBtn(),
              ),
              _passwordField(_isRevealed),
              if (showRepeat) ...[
                _pair(const Label(Resources.editRepeatLabel)),
                _repeatField(),
              ],
              _pair(const Label(Resources.editLabelLabel)),
              _labelField(),
              _pair(
                const Label(Resources.editIconLabel),
                button: _pickLabelBtn(),
              ),
              _iconField(),
              _pair(
                const Label(Resources.editNotesLabel),
                button: _pickIconBtn(),
              ),
              _notesField(),
              const SizedBox(height: 28.0, width: 28.0),
            ],
          ),
        ),
      ),
    );
  }

  Widget _pair(Widget label, {Widget? button}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: label,
        ),
        if (_isEdit && button != null) button,
      ],
    );
  }

  MyTextFormField _notesField() {
    return MyTextFormField(
      controller: _notesController,
      key: _notesKey,
      style: Theme.of(context).textTheme.titleLarge,
      maxLines: 10,
      readOnly: !_isEdit,
      inputFormatters: [
        LengthLimitingTextInputFormatter(Ui.kMaxOtherLength),
      ],
    );
  }

  Padding _iconField() {
    return Padding(
      padding: const EdgeInsets.only(top: 4.0),
      child: Icon(
        _theIconData?.iconData ??
            IconNameData.getIconNameData(widget.icon ?? '')?.iconData,
        color: Theme.of(context).colorScheme.primary,
      ),
    );
  }

  MyTextFormField _labelField() {
    return MyTextFormField(
      autocorrect: false,
      controller: _labelController,
      key: _labelKey,
      style: Theme.of(context).textTheme.titleLarge,
      readOnly: !_isEdit,
      inputFormatters: [
        LengthLimitingTextInputFormatter(Ui.kMaxFieldLength),
      ],
    );
  }

  MyTextFormField _repeatField() {
    return MyTextFormField(
      autocorrect: false,
      controller: _repeatController,
      key: _repeatKey,
      obscureText: true,
      style: Theme.of(context).textTheme.titleLarge,
      readOnly: !_isEdit,
      validator: (value) => Validators.repeatValidator(
        value,
        _passwordController.text,
      ),
      inputFormatters: [
        LengthLimitingTextInputFormatter(Ui.kMaxFieldLength),
      ],
    );
  }

  MyTextFormField _passwordField(bool isReveal) {
    return MyTextFormField(
      autocorrect: false,
      controller: _passwordController,
      key: _passwordKey,
      style: Theme.of(context).textTheme.titleLarge,
      obscureText: !isReveal,
      readOnly: !_isEdit,
      inputFormatters: [
        LengthLimitingTextInputFormatter(Ui.kMaxFieldLength),
      ],
    );
  }

  MyTextFormField _usernameField() {
    return MyTextFormField(
      autocorrect: false,
      controller: _usernameController,
      key: _usernameKey,
      style: Theme.of(context).textTheme.titleLarge,
      readOnly: !_isEdit,
      inputFormatters: [
        LengthLimitingTextInputFormatter(Ui.kMaxFieldLength),
      ],
    );
  }

  MyTextFormField _urlField() {
    return MyTextFormField(
      autofocus: _urlController.text == '',
      autocorrect: false,
      controller: _urlController,
      keyboardType: TextInputType.url,
      key: _urlKey,
      style: Theme.of(context).textTheme.titleLarge,
      readOnly: !_isEdit,
      validator: Validators.urlValidator,
      inputFormatters: [
        LengthLimitingTextInputFormatter(Ui.kMaxFieldLength),
      ],
    );
  }

  MaterialButton _pickUsernameBtn() {
    return _pickButton(
      Resources.pickUsernameButtonText,
      _onPressedPickUsername,
    );
  }

  MaterialButton _pickIconBtn() {
    return _pickButton(
      Resources.pickIconButtonText,
      _onPressedPickIcon,
    );
  }

  MaterialButton _pickLabelBtn() {
    return _pickButton(
      Resources.pickLabelButtonText,
      _onPressedPickLabel,
    );
  }

  MaterialButton _pickButton(String text, void Function() onPressed) {
    return MaterialButton(
      onPressed: onPressed,
      child: Align(
          alignment: Alignment.centerRight,
          child: Text(
            text,
            style: TextStyle(
              color: Theme.of(context).colorScheme.primary,
              fontWeight: FontWeight.bold,
            ),
          )),
    );
  }

  void _onPressedDelete() {
    _bloc.deleteCard();
    Navigator.pop(context);
  }

  void _onPressedUndelete() {
    _bloc.undeleteCard();
    Navigator.pop(context);
  }

  void _onPressedCopy() {
    if (_passwordController.text.isEmpty) return;

    Clipboard.setData(ClipboardData(text: _passwordController.text));

    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text(Resources.copiedPasswordMessage),
        duration: Duration(seconds: 1),
      ),
    );
  }

  Future<void> _onPressedEnlarge() async {
    final viewPasswordDialog = ViewPasswordDialog();
    await viewPasswordDialog.show(context, _passwordController.text);
  }

  void _onPressedToggleEdit() {
    setState(() => _isEdit = !_isEdit);
  }

  void _onPressedPickUsername() async {
    final username =
        await Navigator.pushNamed(context, Routes.pickusername) as String?;
    if (username != null) {
      _usernameController.text = username;
    }
  }

  void _onPressedPickLabel() async {
    final label =
        await Navigator.pushNamed(context, Routes.picklabel) as String?;
    if (label != null) {
      _labelController.text = label;
    }
  }

  void _onPressedPickIcon() async {
    final icon =
        await Navigator.pushNamed(context, Routes.pickicon) as IconNameData?;
    if (icon != null) {
      setState(() {
        _theIconData = icon;
      });
    }
  }

  void _onPressedSubmit() async {
    try {
      final isValid = _formKey.currentState!.validate();
      if (!isValid) {
        logger.log(Level.debug, '$runtimeType._onPressedSubmit() - not valid');
        return;
      }

      final isUpdated = _bloc.updateCard(
        _urlController.text,
        _usernameController.text,
        _passwordController.text,
        _repeatController.text,
        _labelController.text,
        _theIconData?.name ?? '',
        _notesController.text,
        isDeleted: _bloc.card.isDeleted,
        isRevealed: _isRevealed,
      );

      if (isUpdated) {
        await Session3.instance.theAutoSaver!.saveIfChanged();

        if (context.mounted) {
          Navigator.of(context).pop(true);
        }
      }
    } catch (e, s) {
      logger.logException(
          Level.warning, '$runtimeType._onPressedSubmit()', e, s);
      await _dialogs.showErrorDialog(context, e.toString());
    }
  }
}
