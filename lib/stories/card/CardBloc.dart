import '../../models3/XCard3.dart';
import '../../services/Session3.dart';
import '../../utils/Logger.dart';
import '../common/Validators.dart';

class CardBloc {
  XCard3 get card => Session3.instance.theCard!;

  // ---

  CardBloc() {
    logger.log(Level.debug, '$runtimeType()');
  }

  // ---

  bool updateCard(
    String url,
    String username,
    String password,
    String repeat,
    String label,
    String icon,
    String other, {
    required bool isDeleted,
    required bool isRevealed,
  }) {
    logger.log(Level.debug, '$runtimeType.updateCard() - replacing');

    if (Validators.urlValidator(url) != null) {
      return false;
    }

    if (!isRevealed && Validators.repeatValidator(repeat, password) != null) {
      return false;
    }

    url = url.trim();
    username = username.trim();
    label = label.trim();

    final newcard = XCard3()
      ..url = url
      ..username = username
      ..password = password
      ..other = other
      ..label = label
      ..icon = icon
      ..isDeleted = isDeleted;

    Session3.instance.putCard(
      card,
      newcard,
    );

    return true;
  }

  void deleteCard() {
    Session3.instance.deleteCard(
      card,
      delete: true,
    );
  }

  void undeleteCard() {
    Session3.instance.deleteCard(
      card,
      delete: false,
    );
  }
}
