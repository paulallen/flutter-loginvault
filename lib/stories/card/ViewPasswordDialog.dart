import 'package:flutter/material.dart';

import '../../stories/common/Resources.dart';
import '../../utils/Logger.dart';

class ViewPasswordDialog {
  Future<void> show(BuildContext context, String password) {
    logger.log(Level.debug, '$runtimeType.show()');

    const int modulus = 20;
    const String numbers = '12345678901234567890';

    final numberedCharacters = <Widget>[];
    for (var i = 0; i < password.runes.length; ++i) {
      final char = _numberedCharacterWidget(
        context,
        numbers[i % modulus],
        password[i],
      );
      numberedCharacters.add(char);
    }

    final numberOfRows = (numberedCharacters.length / modulus).ceil();

    final rows = List.generate(
      numberOfRows,
      (j) => getRow(
        numberedCharacters,
        j * modulus,
        j * modulus + modulus,
      ),
      growable: false,
    );

    return showDialog(
      context: context,
      builder: (c) => AlertDialog(
        contentPadding: EdgeInsets.zero,
        content: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: rows,
        ),
        actions: [
          TextButton(
            child: const Text(Resources.close),
            onPressed: () => Navigator.pop(c),
          ),
        ],
      ),
    );
  }

  Widget _numberedCharacterWidget(
    BuildContext context,
    String index,
    String character,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(index, style: Theme.of(context).textTheme.labelSmall),
        Text(character, style: Theme.of(context).textTheme.titleLarge),
      ],
    );
  }

  Widget getRow(
    List<Widget> numberedCharacters,
    int start,
    int end,
  ) {
    return Padding(
      padding: const EdgeInsets.only(top: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: getRange(
          numberedCharacters,
          start,
          end,
        ),
      ),
    );
  }

  List<Widget> getRange(
    List<Widget> numberedCharacters,
    int start,
    int end,
  ) {
    return numberedCharacters
        .getRange(start,
            end > numberedCharacters.length ? numberedCharacters.length : end)
        .toList();
  }
}
