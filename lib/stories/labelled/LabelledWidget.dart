import 'package:flutter/material.dart';

import '../../models3/XCard3.dart';
import '../../services/Session3.dart';
import '../../utils/Logger.dart';
import '../card/CardWidget.dart';
import '../common/AppBarTitle.dart';
import '../common/MyIcons.dart';
import '../common/Ui.dart';
import '../common/CardLoginTile.dart';
import '../common/Dialogs.dart';
import '../common/EmptyListPane.dart';
import '../common/MyFloatingButton.dart';
import '../common/Resources.dart';
import '../routes/Routes.dart';
import 'LabelledBloc.dart';

class LabelledWidget extends StatefulWidget {
  LabelledWidget({super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<StatefulWidget> createState() => _LabelledState();
}

class _LabelledState extends State<LabelledWidget> {
  final _bloc = LabelledBloc();
  static final _dialogs = Dialogs();

  _LabelledState() {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    logger.log(Level.debug, '$runtimeType.build()');

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        title: AppBarTitle(
          _bloc.label,
        ),
      ),
      body: SafeArea(
        child: _body(),
      ),
      floatingActionButton: MyFloatingButton(
        icon: MyIcons.add,
        onPressed: _onPressedAdd,
      ),
    );
  }

  Widget _body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          child: _listBuilder(),
        ),
      ],
    );
  }

  Widget _listBuilder() {
    return StreamBuilder(
      stream: _bloc.cards,
      builder: (context, AsyncSnapshot<List<XCard3>> snapshot) {
        return _cardListBuilder(context, snapshot);
      },
    );
  }

  Widget _cardListBuilder(
      BuildContext context, AsyncSnapshot<List<XCard3>> snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      logger.log(Level.debug, '$runtimeType._cardListBuilder() - waiting');
      return Ui.blankWidget();
    }

    return snapshot.data?.isNotEmpty == true
        ? _list(snapshot.data!)
        : _emptyList();
  }

  Widget _emptyList() {
    return EmptyListPane(
      title: Resources.noCardsTitle,
      text: Resources.addFirstCardText,
      child: Icon(
        MyIcons.card,
        size: 64.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  ListView _list(List<XCard3> cards) {
    logger.log(Level.debug, '$runtimeType._list() - ${cards.length}');

    return ListView.builder(
      itemCount: cards.length,
      itemBuilder: (c, i) => _cardTile(cards[i]),
    );
  }

  Widget _cardTile(XCard3 vm) {
    return CardLoginTile(
      vm,
      onTap: () => _editCard(vm, isEdit: false),
    );
  }

  Future<void> _onPressedAdd() async {
    final card = XCard3()..label = _bloc.label;
    return _editCard(card, isEdit: true);
  }

  Future<void> _editCard(XCard3 card, {required bool isEdit}) async {
    try {
      Session3.instance.theCard = card;

      await Navigator.of(context).pushNamed(
        Routes.card,
        arguments: CardParameters(
          isEdit: isEdit,
        ),
      );
    } catch (e, s) {
      logger.logException(Level.warning, '$runtimeType._editCard()', e, s);
      await _dialogs.showErrorDialog(context, e.toString());
    }
  }
}
