import 'dart:async';

import 'package:rxdart/rxdart.dart';

import '../../models3/XCard3.dart';
import '../../services/Session3.dart';
import '../../utils/Logger.dart';

class LabelledBloc {
  String get label => Session3.instance.theSelectedLabel;

  final _cardVms = BehaviorSubject<List<XCard3>>();
  Stream<List<XCard3>> get cards => _cardVms;

  late StreamSubscription _s1;

  // ---

  LabelledBloc() {
    logger.log(Level.debug, '$runtimeType()');

    _s1 = Session3.instance.cardsStream.listen(_onData);
  }

  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    _s1.cancel();
    _cardVms.close();
  }

  // ---

  void _onData(List<XCard3> cards) {
    logger.log(Level.debug, '$runtimeType._onData()');

    final labelledCards = cards
        .where((c) => !c.isDeleted)
        .where((c) => c.label == label)
        .toList();
    _cardVms.add(labelledCards);
  }
}
