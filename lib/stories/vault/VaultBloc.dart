import '../../models3/Vault3.dart';
import '../../services/SecureStorage.dart';
import '../../services/Session3.dart';
import '../../services/VaultIo3.dart';
import '../../utils/Logger.dart';

class VaultBloc {
  static final _vaultio3 = VaultIo3();
  static final _secureStorage = SecureStorage();

  Stream<String> get messages => Session3.instance.messages;

  Vault3? get vault => Session3.instance.theVault;

  Future<void> deleteit() async {
    logger.log(Level.debug, '$runtimeType.deleteit()');
    assert(Session3.instance.theVaultPath! != '');

    await _secureStorage.delete(Session3.instance.theVaultPath!);
    await _vaultio3.deleteVault(Session3.instance.theVaultPath!);
    await Session3.instance.closeVault();
  }
}
