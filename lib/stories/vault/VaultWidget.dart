import 'dart:async';

import 'package:flutter/material.dart';

import '../../utils/Logger.dart';
import '../common/ActionButton.dart';
import '../common/AppBarTitle.dart';
import '../common/Resources.dart';
import '../common/MyIcons.dart';
import '../common/Ui.dart';
import '../recyclebin/RecycleBinWidget.dart';
import '../search/SearchWidget.dart';
import '../settings/SettingsWidget.dart';
import '../labels/LabelsWidget.dart';
import 'VaultBloc.dart';

class VaultWidget extends StatefulWidget {
  VaultWidget({super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<StatefulWidget> createState() => _VaultState();
}

class _VaultState extends State<VaultWidget> {
  final _bloc = VaultBloc();

  final List<String> _titles = <String>[
    Resources.searchPageTitle,
    Resources.labelsPageTitle,
    Resources.recyclingPageTitle,
    Resources.settingsPageTitle,
  ];

  static final List<Widget> _pages = <Widget>[
    SearchWidget(),
    LabelsWidget(),
    RecycleBinWidget(),
    SettingsWidget(),
  ];

  List<BottomNavigationBarItem> _navBarItems(BuildContext context) {
    return [
      BottomNavigationBarItem(
        icon: Icon(
          MyIcons.search,
          color: Theme.of(context).colorScheme.primary,
        ),
        label: _titles[0],
      ),
      BottomNavigationBarItem(
        icon: Icon(
          MyIcons.label,
          color: Theme.of(context).colorScheme.primary,
        ),
        label: _titles[1],
      ),
      BottomNavigationBarItem(
        icon: Icon(
          MyIcons.delete,
          color: Theme.of(context).colorScheme.primary,
        ),
        label: _titles[2],
      ),
      BottomNavigationBarItem(
        icon: Icon(
          MyIcons.settings,
          color: Theme.of(context).colorScheme.primary,
        ),
        label: _titles[3],
      ),
    ];
  }

  int _selectedIndex = 0;

  _VaultState() {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  Widget build(BuildContext context) {
    logger.log(Level.debug, '$runtimeType.build()');

    if (_bloc.vault == null) {
      logger.log(Level.warning, '$runtimeType.build() - vault is null');
      return Ui.blankWidget();
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        leading: Image.asset(Ui.vaultImage21x24),
        title: AppBarTitle(
          _titles[_selectedIndex],
        ),
        actions: <Widget>[
          _deleteAction(),
        ],
      ),
      body: SafeArea(
        child: Center(
          child: _pages.elementAt(_selectedIndex),
        ),
      ),
      bottomNavigationBar: _navigationBar(),
    );
  }

  Widget _deleteAction() {
    return ActionButton(
      icon: const Icon(
        MyIcons.delete,
      ),
      onPressed: _onPressedDelete,
    );
  }

  BottomNavigationBar _navigationBar() {
    return BottomNavigationBar(
      items: _navBarItems(context),
      currentIndex: _selectedIndex,
      type: BottomNavigationBarType.fixed,
      showSelectedLabels: true,
      showUnselectedLabels: true,
      selectedItemColor: Theme.of(context).colorScheme.primary,
      onTap: _onNavigationItemTapped,
    );
  }

  void _onNavigationItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void _onPressedDelete() async {
    final deleteIt = await _showConfirmDeleteDialog();

    if (deleteIt != true) {
      return;
    }

    await _bloc.deleteit();
  }

  Future<bool?> _showConfirmDeleteDialog() {
    return showDialog<bool>(
      context: context,
      builder: (c) => AlertDialog(
        content: const Text(Resources.confirmDeleteQuestion),
        actions: [
          TextButton(
            child: const Text(Resources.delete),
            onPressed: () => Navigator.of(c).pop(true),
          ),
          TextButton(
            child: const Text(Resources.cancel),
            onPressed: () => Navigator.of(c).pop(false),
          )
        ],
      ),
    );
  }
}
