import 'package:flutter/material.dart';

import '../../utils/Logger.dart';
import '../common/AppBarTitle.dart';
import '../common/Resources.dart';
import '../common/IconNameData.dart';

class PickIconWidget extends StatefulWidget {
  PickIconWidget({super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<StatefulWidget> createState() => _PickIconState();
}

class _PickIconState extends State<PickIconWidget> {
  _PickIconState() {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  Widget build(BuildContext context) {
    logger.log(Level.debug, '$runtimeType.build()');

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        title: const AppBarTitle(
          Resources.selectIconTitle,
        ),
      ),
      body: SafeArea(
        child: _body(),
      ),
    );
  }

  Widget _body() {
    final width = MediaQuery.of(context).size.width;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          child: GridView.count(
            crossAxisCount: (width / 64).floor(),
            children: List.generate(
              IconNameData.icons.length,
              (i) => _tile(IconNameData.icons[i]),
            ),
          ),
        ),
      ],
    );
  }

  Widget _tile(IconNameData icon) {
    return MaterialButton(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          icon.name.isEmpty
              ? const Icon(null)
              : Icon(
                  icon.iconData,
                  color: Theme.of(context).colorScheme.primary,
                ),
        ],
      ),
      onPressed: () => _onTapIcon(icon),
    );
  }

  void _onTapIcon(IconNameData icon) {
    Navigator.of(context).pop(icon);
  }
}
