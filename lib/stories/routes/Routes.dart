import 'package:flutter/material.dart';

import '../../utils/Logger.dart';
import '../card/CardWidget.dart';
import '../pickfile/PickFileWidget.dart';
import '../picklabel/PickLabelWidget.dart';
import '../pickicon/PickIconWidget.dart';
import '../pickusername/PickUsernameWidget.dart';
import '../labelled/LabelledWidget.dart';
import '../home/HomeWidget.dart';
import '../password/EnterPasswordWidget.dart';
import '../password/SetPasswordWidget.dart';

class Routes {
  static const String card = 'card';
  static const String labelled = 'labelled';
  static const String home = '/';
  static const String pickfile = 'pickfile';
  static const String picklabel = 'picklabel';
  static const String pickicon = 'pickicon';
  static const String pickusername = 'pickusername';
  static const String enterpassword = 'enterpassword';
  static const String setpassword = 'setpassword';

  static Route<dynamic> onGenerateRoute(RouteSettings routeSettings) {
    return MaterialPageRoute<dynamic>(
      builder: (_) => builder(routeSettings),
      settings: routeSettings,
    );
  }

  static Widget builder(RouteSettings routeSettings) {
    logger.log(Level.debug, 'Routes.builder(${routeSettings.name})');

    switch (routeSettings.name) {
      case card:
        return CardWidget(routeSettings.arguments as CardParameters);
      case labelled:
        return LabelledWidget();
      case home:
        return HomeWidget();
      case pickfile:
        return PickFileWidget();
      case pickicon:
        return PickIconWidget();
      case picklabel:
        return PickLabelWidget();
      case pickusername:
        return PickUsernameWidget();
      case enterpassword:
        return EnterPasswordWidget(
            routeSettings.arguments as EnterPasswordParameters);
      case setpassword:
        return SetPasswordWidget(routeSettings.arguments as String);
    }

    throw Exception('Invalid route.name: ${routeSettings.name}');
  }
}
