import 'package:flutter/material.dart';

import '../../services/ColourStrings.dart';
import '../../services/Preferences.dart';
import '../../services/Session3.dart';
import '../../utils/Logger.dart';
import '../common/Label.dart';
import '../common/Resources.dart';
import '../common/Ui.dart';

class SettingsWidget extends StatefulWidget {
  SettingsWidget({super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<StatefulWidget> createState() => _SettingsState();
}

class _SettingsState extends State<SettingsWidget> {
  final _preferences = Preferences();
  final _colourStrings = ColourStrings();

  final kAlpha = 1.0;
  final kHueMax = 320;
  final kHueDelta = 5;
  final kPaletteHeight = 20.0;
  final kSliderLineWidthCorrection = 80.0;

  double _hueSliderValue = 0;
  Brightness? _brightness = Brightness.dark;

  _SettingsState() {
    logger.log(Level.debug, '$runtimeType()');

    final hsvColour = Session3.instance.primaryColourValue;
    if (hsvColour != null) {
      _hueSliderValue = hsvColour.hue;
      if (_hueSliderValue > kHueMax) {
        _hueSliderValue = kHueMax * .10;
      }

      _brightness = Ui.brightnessForValue(hsvColour.value);

      logger.log(Level.debug,
          '$runtimeType() - hue: ${hsvColour.hue} value: ${hsvColour.value}, brightness: $_brightness');
    }
  }

  @override
  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    logger.log(Level.debug, '$runtimeType.build()');

    return Scaffold(
      body: SafeArea(
        child: _body(),
      ),
    );
  }

  Widget _body() {
    return Padding(
      padding: Ui.insets16LeftAboveRight,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const Label(Resources.themeLabel),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _brightnessRadio(Brightness.dark),
              _brightnessRadio(Brightness.light),
            ],
          ),
          const SizedBox(
            height: 48.0,
          ),
          const Label(Resources.colourLabel),
          Padding(
            padding: const EdgeInsets.only(
              left: 24.0,
              top: 12.0,
            ),
            child: _palette(),
          ),
          _hueSlider(),
        ],
      ),
    );
  }

  Widget _brightnessRadio(Brightness? brightness) {
    return GestureDetector(
      child: SizedBox(
        width: Ui.kThemeImageWidth,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Radio<Brightness?>(
                  value: brightness,
                  groupValue: _brightness,
                  onChanged: _onChangedBrightness,
                ),
                Text(brightness == Brightness.dark
                    ? Resources.darkThemeLabel
                    : Resources.lightThemeLabel),
              ],
            ),
            Image.asset(
                brightness == Brightness.dark ? Ui.darkImage : Ui.lightImage),
          ],
        ),
      ),
      onTap: () => _onChangedBrightness(brightness),
    );
  }

  Widget _palette() {
    final hsvValue = Ui.valueForBrightness(_brightness!);

    final nboxes = (kHueMax / kHueDelta).ceil();
    final width = MediaQuery.of(context).size.width;
    final widthEach = (width - kSliderLineWidthCorrection) / nboxes;

    final boxes = List.generate(
        nboxes,
        (hueIndex) => ColoredBox(
              color: HSVColor.fromAHSV(
                kAlpha,
                hueIndex * kHueDelta * 1.0,
                Ui.kHsvSaturation,
                hsvValue,
              ).toColor(),
              child: SizedBox(
                width: widthEach,
                height: kPaletteHeight,
              ),
            ));

    return Row(
      children: boxes,
    );
  }

  Widget _hueSlider() {
    final nboxes = (kHueMax / kHueDelta).ceil();

    return Slider(
      value: _hueSliderValue,
      min: 0,
      max: kHueMax * 1.0,
      divisions: nboxes,
      label: _hueSliderValue.round().toString(),
      onChanged: _onChangedHue,
    );
  }

  void _onChangedHue(double hue) {
    logger.log(Level.debug, '_onChangedHue(hue: $hue)');

    setState(() {
      _hueSliderValue = hue;
    });
    _updatePrimaryColour();
  }

  void _onChangedBrightness(Brightness? brightness) {
    logger.log(Level.debug, '_onChangedBrightness(brightness: $brightness)');

    setState(() {
      _brightness = brightness;
    });
    _updatePrimaryColour();
  }

  Future<void> _updatePrimaryColour() async {
    logger.log(Level.debug,
        '_updatePrimaryColour(hue: $_hueSliderValue, brightness: $_brightness)');

    final value = Ui.valueForBrightness(_brightness!);
    final hsvColour =
        HSVColor.fromAHSV(kAlpha, _hueSliderValue, Ui.kHsvSaturation, value);

    logger.log(Level.debug,
        '_updatePrimaryColour(hue: $_hueSliderValue, brightness: $_brightness) - value: $value');

    Session3.instance.setPrimaryColour(hsvColour);

    final argb = _colourStrings.toHashArgb(hsvColour);
    return _preferences.write(Ui.kPrimaryColourKey, argb);
  }
}
