import 'dart:io';

class SelectedEntityVm {
  final FileSystemEntity entity;
  final String name;
  final bool isSelected;

  SelectedEntityVm(this.entity, this.name, {this.isSelected = false});
}
