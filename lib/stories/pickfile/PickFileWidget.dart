import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';

import '../../utils/Logger.dart';
import '../common/AppBarTitle.dart';
import '../common/EmptyListPane.dart';
import '../common/Resources.dart';
import '../common/MyFloatingButton.dart';
import '../common/MyIcons.dart';
import '../common/Ui.dart';
import 'SelectedEntityVm.dart';
import 'PickFileBloc.dart';

class PickFileWidget extends StatefulWidget {
  PickFileWidget({super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<StatefulWidget> createState() => _PickFileState();
}

class _PickFileState extends State<PickFileWidget> {
  final _bloc = PickFileBloc();
  bool _canGoUp = false;

  _PickFileState() {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  Widget build(BuildContext context) {
    // logger.log(Level.debug, '$runtimeType.build()');

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        leading: _upButtonOrVaultImage(),
        title: AppBarTitle(
          _canGoUp ? _bloc.folderName : Resources.appTitle,
        ),
      ),
      body: SafeArea(
        child: _body(),
      ),
      floatingActionButton: MyFloatingButton(
        icon: MyIcons.add,
        onPressed: _onPressedAdd,
      ),
    );
  }

  Widget _upButtonOrVaultImage() {
    return _canGoUp
        ? IconButton(
            icon: Ui.upIcon(context),
            onPressed: _onPressedUp,
          )
        : Image.asset(Ui.vaultImage21x24);
  }

  Widget _body() {
    return StreamBuilder(
      stream: _bloc.vaults,
      builder: _listBuilder,
    );
  }

  Widget _listBuilder(
      BuildContext context, AsyncSnapshot<List<SelectedEntityVm>> snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      logger.log(Level.debug, '$runtimeType._listBuilder() - waiting');
      return Ui.blankWidget();
    }

    return snapshot.data?.isNotEmpty == true
        ? _list(snapshot.data!)
        : _emptyList();
  }

  Widget _emptyList() {
    return EmptyListPane(
      title: Resources.noVaultsTitle,
      text: Resources.addFirstVaultText,
      child: Image.asset(Ui.vaultImage100x100),
    );
  }

  Widget _list(List<SelectedEntityVm> vaults) {
    return ListView.builder(
      itemCount: vaults.length,
      itemBuilder: (c, i) => _vaultTile(vaults[i]),
    );
  }

  Widget _vaultTile(SelectedEntityVm vm) {
    return ListTile(
      leading: _leadingIcon(vm),
      title: Text(vm.name, style: Theme.of(context).textTheme.titleLarge),
      // value: vm.isSelected,
      onTap: () => _onTapTile(vm),
    );
  }

  Widget? _leadingIcon(SelectedEntityVm vm) {
    return vm.entity is Directory
        ? Icon(
            MyIcons.folder,
            color: Theme.of(context).colorScheme.primary,
          )
        : vm.entity.path.endsWith(_bloc.vaultExtension)
            ? Image.asset(Ui.vaultImage21x24)
            : null;
  }

  Future<void> _onPressedUp() async {
    await _bloc.goUp();
    setState(() {
      _canGoUp = _bloc.canGoUp();
    });
  }

  Future<void> _onTapTile(SelectedEntityVm vm) async {
    logger.log(Level.debug, '$runtimeType._onTapTile(name: ${vm.name}))');

    if (vm.entity is Directory) {
      await _bloc.selectFolder(vm.entity.path);
      setState(() {
        _canGoUp = _bloc.canGoUp();
      });
    } else if (vm.entity is File) {
      _pop(vm);
    }
  }

  void _pop(SelectedEntityVm vm) {
    logger.log(Level.debug, '$runtimeType._pop()');

    Navigator.of(context).pop(vm.entity.path);
  }

  Future<void> _onPressedAdd() async {
    logger.log(Level.debug, '$runtimeType._onPressedAdd()');

    Navigator.of(context).pop(null);
  }
}
