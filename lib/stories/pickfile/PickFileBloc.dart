import 'dart:io';

import 'package:path/path.dart' as path;
import 'package:rxdart/rxdart.dart';

import '../../services/FileIo.dart';
import '../../services/NameMapper.dart';
import '../../services/Session3.dart';
import '../../utils/Logger.dart';
import 'SelectedEntityVm.dart';

class PickFileBloc {
  static final _fileio = FileIo();
  static final _nameMapper = NameMapper();

  final _vaultVms = BehaviorSubject<List<SelectedEntityVm>>();

  Stream<List<SelectedEntityVm>> get vaults => _vaultVms;

  Stream<bool> get anySelected =>
      _vaultVms.map((list) => list.any((vm) => vm.isSelected));

  String get folderName => path.basename(Session3.instance.thePath);
  String get vaultExtension => NameMapper.extension;

  // ---

  PickFileBloc() {
    logger.log(Level.debug, '$runtimeType()');

    final _ = refresh();
  }

  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    _vaultVms.close();
  }

  // ---

  Future<void> refresh() {
    return _loadVaultList(Session3.instance.thePath);
  }

  bool canGoUp() {
    return path.isWithin(
      Session3.instance.theRootPath,
      Session3.instance.thePath,
    );
  }

  Future<void> goUp() async {
    final parentPath = path.dirname(Session3.instance.thePath);
    await _loadVaultList(parentPath);
  }

  Future<void> selectFolder(String folderPath) async {
    await _loadVaultList(folderPath);
  }

  void selectVault(String name, {required bool isSelected}) {
    logger.log(Level.debug, '$runtimeType.selectVault(name: $name)');

    final vms = _vaultVms.value
        .map((v) => SelectedEntityVm(
              v.entity,
              v.name,
              isSelected: v.entity.path == name ? isSelected : v.isSelected,
            ))
        .toList();

    _vaultVms.add(vms);
  }

  // ---

  Future<void> _loadVaultList(String folderPath) async {
    logger.log(Level.debug, '$runtimeType._loadVaultList()');

    try {
      folderPath = path.isWithin(Session3.instance.theRootPath, folderPath)
          ? folderPath
          : Session3.instance.theRootPath;

      final entities = await _fileio.listFileSystemEntities(folderPath);
      final vaults = _foldersAndVaults(entities);
      vaults.sort(_sortByTypeAndName);

      final vms = vaults
          .map((e) => SelectedEntityVm(e, _vaultName(e.path, folderPath)))
          .toList();

      _vaultVms.add(vms);

      Session3.instance.thePath = folderPath;
    } catch (e, s) {
      logger.logException(
          Level.warning, '$runtimeType._loadVaultList($folderPath)', e, s);
    }
  }

  // ---

  static List<FileSystemEntity> _foldersAndVaults(List<FileSystemEntity> all) {
    return all
        .where((fse) =>
            fse is Directory || fse.path.endsWith(NameMapper.extension))
        .toList();
  }

  static String _vaultName(String filePath, String folder) {
    final startIndex =
        folder.endsWith(path.separator) ? folder.length : folder.length + 1;
    final fileName = filePath.substring(startIndex);
    return _nameMapper.toVaultName(fileName);
  }

  static int _sortByTypeAndName(
    FileSystemEntity entity1,
    FileSystemEntity entity2,
  ) {
    if (entity1 is Directory && entity2 is! Directory) return -1;
    if (entity1 is! Directory && entity2 is Directory) return 1;

    final upper1 = entity1.path.toUpperCase();
    final upper2 = entity1.path.toUpperCase();
    return upper1.compareTo(upper2);
  }
}
