import '../../models3/Vault3.dart';
import '../../services/VaultIo3.dart';
import '../../services/Biometrics.dart';
import '../../services/Session3.dart';
import '../../services/SecureStorage.dart';
import '../../services/NameMapper.dart';
import '../../utils/Logger.dart';

class EnterPasswordBloc {
  static final _biometrics = Biometrics();
  static final _secureStorage = SecureStorage();
  static final _vaultio3 = VaultIo3();
  static final _nameMapper = NameMapper();

  String toVaultPath(String vaultName) {
    return _nameMapper.toFilePath(Session3.instance.thePath, vaultName);
  }

  String toVaultName(String filePath) {
    return _nameMapper.toVaultName(filePath);
  }

  Future<bool> canWeUseBiometrics() async {
    final biometrics = await _biometrics.canUseBiometrics();
    final storage = await _secureStorage.canUseStorage();
    return biometrics && storage;
  }

  Future<String?> readPassword(String filePath) async {
    return _secureStorage.read(filePath);
  }

  Future<bool> authenticate(String reason) async {
    return _biometrics.authenticate(
      reason: reason,
    );
  }

  Future<Vault3?> openit(
    String filePath,
    String password,
  ) async {
    logger.log(Level.debug, '$runtimeType.openit($filePath)');

    return _vaultio3.loadVault(
      filePath,
      password,
    );
  }
}
