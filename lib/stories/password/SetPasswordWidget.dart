import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../utils/Logger.dart';
import '../common/AppBarTitle.dart';
import '../common/Label.dart';
import '../common/MyFloatingButton.dart';
import '../common/MyTextFormField.dart';
import '../common/MyIcons.dart';
import '../common/Resources.dart';
import '../common/Ui.dart';
import '../common/Validators.dart';

class SetPasswordResult {
  final String vaultName;
  final String password;

  SetPasswordResult(
    this.vaultName,
    this.password,
  );
}

class SetPasswordWidget extends StatefulWidget {
  final String vaultName;

  SetPasswordWidget(this.vaultName, {super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<StatefulWidget> createState() => _SetPasswordState();
}

class _SetPasswordState extends State<SetPasswordWidget> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _nameController;
  late TextEditingController _passwordController;
  late TextEditingController _repeatController;

  final bool canCancel = !Ui.kIsSingleVaultOnly;
  final bool isFixedName = false;

  _SetPasswordState();

  @override
  void initState() {
    logger.log(Level.debug, '$runtimeType.initState()');
    super.initState();

    _nameController = TextEditingController(text: widget.vaultName);
    _passwordController = TextEditingController();
    _repeatController = TextEditingController();
  }

  @override
  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    _nameController.dispose();
    _passwordController.dispose();
    _repeatController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    logger.log(Level.debug, '$runtimeType.build()');

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        leading: canCancel
            ? Ui.backIconButton(context)
            : Image.asset(Ui.vaultImage21x24),
        title: const AppBarTitle(
          Resources.setPasswordTitle,
        ),
      ),
      body: SafeArea(
        child: _body(),
      ),
      floatingActionButton: MyFloatingButton(
        icon: MyIcons.done,
        onPressed: _onPressedSubmit,
      ),
    );
  }

  Widget _body() {
    return Padding(
      padding: Ui.insets16LeftAboveRight,
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Label(Resources.vaultNameLabel),
              _nameField(),
              const Label(Resources.passwordLabel),
              _passwordField(),
              const Label(Resources.repeatPasswordLabel),
              _repeatField(),
            ],
          ),
        ),
      ),
    );
  }

  MyTextFormField _repeatField() {
    return MyTextFormField(
      controller: _repeatController,
      obscureText: true,
      style: Theme.of(context).textTheme.titleLarge,
      validator: (value) => Validators.repeatValidator(
        value,
        _passwordController.text,
      ),
      inputFormatters: [
        LengthLimitingTextInputFormatter(Ui.kMaxFieldLength),
      ],
    );
  }

  MyTextFormField _passwordField() {
    return MyTextFormField(
      controller: _passwordController,
      obscureText: true,
      style: Theme.of(context).textTheme.titleLarge,
      validator: Validators.passwordValidator,
      inputFormatters: [
        LengthLimitingTextInputFormatter(Ui.kMaxFieldLength),
      ],
    );
  }

  MyTextFormField _nameField() {
    return MyTextFormField(
      autofocus: true,
      readOnly: isFixedName,
      controller: _nameController,
      style: Theme.of(context).textTheme.titleLarge,
      validator: Validators.vaultNameValidator,
      inputFormatters: [
        LengthLimitingTextInputFormatter(Ui.kMaxFieldLength),
      ],
    );
  }

  void _onPressedSubmit() async {
    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      logger.log(Level.debug, '$runtimeType._onPressedSubmit() - not valid');
      return;
    }

    final name = _nameController.text.trim();
    final password = _passwordController.text;

    Navigator.of(context).pop(SetPasswordResult(name, password));
  }
}
