import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../models3/Vault3.dart';
import '../../utils/Logger.dart';
import '../common/AppBarTitle.dart';
import '../common/Dialogs.dart';
import '../common/Label.dart';
import '../common/MyFloatingButton.dart';
import '../common/MyTextFormField.dart';
import '../common/MyIcons.dart';
import '../common/Resources.dart';
import '../common/Ui.dart';
import '../common/Validators.dart';
import 'EnterPasswordBloc.dart';

class EnterPasswordParameters {
  final String vaultName;
  final String filePath;

  EnterPasswordParameters(
    this.vaultName,
    this.filePath,
  ) {
    assert(vaultName.isNotEmpty);
    assert(filePath.isNotEmpty);
  }
}

class EnterPasswordResult {
  final Vault3 vault;
  final String filePath;
  final String password;

  EnterPasswordResult(
    this.vault,
    this.filePath,
    this.password,
  ) {
    assert(filePath.isNotEmpty);
    assert(password.isNotEmpty);
  }
}

class EnterPasswordWidget extends StatefulWidget {
  final EnterPasswordParameters parameters;

  EnterPasswordWidget(this.parameters, {super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<StatefulWidget> createState() => _EnterPasswordState();
}

class _EnterPasswordState extends State<EnterPasswordWidget> {
  static final _dialogs = Dialogs();

  FocusNode? _focusNode;
  late TextEditingController _nameController;
  late TextEditingController _passwordController;

  final _bloc = EnterPasswordBloc();
  final _formKey = GlobalKey<FormState>();
  final bool canCancel = !Ui.kIsSingleVaultOnly;

  String? storedPassword;
  String _message = '';

  _EnterPasswordState();

  @override
  void initState() {
    logger.log(Level.debug, '$runtimeType.initState()');
    super.initState();

    _focusNode = FocusNode();
    _nameController = TextEditingController(text: widget.parameters.vaultName);
    _passwordController = TextEditingController();

    _bloc.readPassword(widget.parameters.filePath).then((password) {
      setState(() {
        storedPassword = password;
        if (storedPassword != null) {
          WidgetsBinding.instance
              .addPostFrameCallback((_) => _onAuthenticate());
        }
      });
    });
  }

  @override
  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    _focusNode?.dispose();
    _nameController.dispose();
    _passwordController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    logger.log(Level.debug, '$runtimeType.build()');

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.primary,
          leading: canCancel
              ? Ui.backIconButton(context)
              : Image.asset(Ui.vaultImage21x24),
          title: const AppBarTitle(
            Resources.enterPasswordTitle,
          ),
        ),
        body: SafeArea(
          child: _body(),
        ),
        floatingActionButton: MyFloatingButton(
          icon: MyIcons.done,
          onPressed: _onPressedSubmit,
        ),
      ),
    );
  }

  Widget _body() {
    return Padding(
      padding: Ui.insets16LeftAboveRight,
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Label(Resources.vaultNameLabel),
              _nameField(context),
              const Label(Resources.passwordLabel),
              _trackedPasswordField(),
              _capsMessageField(),
              _biometricsButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _biometricsButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        TextButton(
          onPressed: storedPassword != null ? _onAuthenticate : null,
          child: const Text(Resources.biometricsButtonText),
        ),
      ],
    );
  }

  Widget _nameField(BuildContext context) {
    return MyTextFormField(
      controller: _nameController,
      style: Theme.of(context).textTheme.titleLarge,
      readOnly: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(Ui.kMaxFieldLength),
      ],
    );
  }

  Widget _trackedPasswordField() {
    return RawKeyboardListener(
      focusNode: _focusNode!,
      onKey: _handleKeyEvent,
      child: AnimatedBuilder(
        animation: _focusNode!,
        builder: (BuildContext c, Widget? child) {
          return _buildPasswordField(c);
        },
      ),
    );
  }

  Widget _buildPasswordField(BuildContext context) {
    return MyTextFormField(
      autofocus: true,
      controller: _passwordController,
      obscureText: true,
      style: Theme.of(context).textTheme.titleLarge,
      validator: Validators.passwordValidator,
      inputFormatters: [
        LengthLimitingTextInputFormatter(Ui.kMaxFieldLength),
      ],
    );
  }

  Widget _capsMessageField() {
    return Container(
      alignment: Alignment.center,
      child: DefaultTextStyle(
        style: Theme.of(context).textTheme.bodyLarge!,
        child: Text(_message),
      ),
    );
  }

  void _handleKeyEvent(RawKeyEvent event) {
    if (event.character == null) return;

    final newMessage = event.logicalKey != LogicalKeyboardKey.goBack &&
            event.character!.isNotEmpty &&
            event.character != ' ' &&
            event.character != '  ' &&
            event.character == event.character!.toUpperCase() &&
            !event.isShiftPressed
        ? 'CAPS LOCK is on'
        : '';

    if (newMessage == _message) return;

    setState(() {
      _message = newMessage;
    });
  }

  Future<void> _onAuthenticate() async {
    logger.log(Level.debug, '$runtimeType._onAuthenticate()');

    if (storedPassword == null) {
      return;
    }

    final isAuthenticated =
        await _bloc.authenticate(Resources.biometricsPrompt);

    if (!isAuthenticated) {
      return;
    }

    await _checkPassword(storedPassword!);
  }

  Future<void> _onPressedSubmit() async {
    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      logger.log(Level.debug, '$runtimeType._onPressedSubmit() - not valid');
      return;
    }

    final password = _passwordController.text;

    await _checkPassword(password);
  }

  Future<void> _checkPassword(String password) async {
    logger.log(Level.debug, '$runtimeType._checkPassword()');

    try {
      final vault = await _bloc.openit(
        widget.parameters.filePath,
        password,
      );

      if (vault != null && context.mounted) {
        var params = EnterPasswordResult(
          vault,
          widget.parameters.filePath,
          password,
        );

        Navigator.of(context).pop(params);
      }
    } catch (e, s) {
      logger.logException(Level.warning, '$runtimeType._checkPassword()', e, s);
      await _dialogs.showErrorDialog(context, Resources.loadFailMessage);
    }
  }
}
