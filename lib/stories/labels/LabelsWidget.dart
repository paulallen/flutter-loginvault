import 'dart:async';

import 'package:flutter/material.dart';

import '../../models3/XCard3.dart';
import '../../services/Session3.dart';
import '../../utils/Logger.dart';
import '../card/CardWidget.dart';
import '../common/Dialogs.dart';
import '../common/EmptyListPane.dart';
import '../common/Resources.dart';
import '../common/MyIcons.dart';
import '../common/MyFloatingButton.dart';
import '../common/Ui.dart';
import '../routes/Routes.dart';
import 'LabelsBloc.dart';

class LabelsWidget extends StatefulWidget {
  LabelsWidget({super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<StatefulWidget> createState() => _LabelsState();
}

class _LabelsState extends State<LabelsWidget> {
  static final _dialogs = Dialogs();

  final _bloc = LabelsBloc();

  _LabelsState() {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // logger.log(Level.debug, '$runtimeType.build()');

    return Scaffold(
      body: SafeArea(
        child: _body(),
      ),
      floatingActionButton: MyFloatingButton(
        icon: MyIcons.add,
        onPressed: _onPressedAdd,
      ),
    );
  }

  Widget _body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          child: _listBuilder(),
        ),
      ],
    );
  }

  Widget _listBuilder() {
    return StreamBuilder(
      stream: _bloc.labels,
      builder: _labelListBuilder,
    );
  }

  Widget _labelListBuilder(
      BuildContext context, AsyncSnapshot<List<String>> snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      logger.log(Level.debug, '$runtimeType._labelListBuilder() - waiting');
      return Ui.blankWidget();
    }

    return snapshot.data?.isNotEmpty == true
        ? _labelList(snapshot.data!)
        : _bloc.noCardsAtAll
            ? _noCards()
            : _noLabels();
  }

  Widget _noLabels() {
    return EmptyListPane(
      title: Resources.noLabelsTitle,
      text: Resources.addFirstCardText,
      child: Icon(
        MyIcons.label,
        size: 64.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  Widget _noCards() {
    return EmptyListPane(
      title: Resources.noCardsTitle,
      text: Resources.addFirstCardText,
      child: Icon(
        MyIcons.card,
        size: 64.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  Widget _labelList(List<String> labels) {
    return ListView.builder(
      itemCount: labels.length,
      itemBuilder: (c, i) => _labelTile(labels[i]),
    );
  }

  Widget _labelTile(String label) {
    return ListTile(
      leading: Icon(
        MyIcons.label,
        color: Theme.of(context).colorScheme.primary,
      ),
      title: Text(
        label,
        style: Theme.of(context).textTheme.titleLarge,
      ),
      onTap: () => _showByLabel(label),
    );
  }

  Future<void> _onPressedAdd() async {
    final card = XCard3();
    return _editCard(card, isEdit: true);
  }

  Future<void> _editCard(XCard3 card, {required bool isEdit}) async {
    try {
      Session3.instance.theCard = card;

      await Navigator.of(context).pushNamed(
        Routes.card,
        arguments: CardParameters(
          isEdit: isEdit,
        ),
      );
    } catch (e, s) {
      logger.logException(Level.warning, '$runtimeType._editCard()', e, s);
      await _dialogs.showErrorDialog(context, e.toString());
    }
  }

  Future<void> _showByLabel(String label) async {
    try {
      Session3.instance.theSelectedLabel = label;

      await Navigator.of(context).pushNamed(
        Routes.labelled,
      );
    } catch (e, s) {
      logger.logException(Level.warning, '$runtimeType._editFolder()', e, s);
      await _dialogs.showErrorDialog(context, e.toString());
    }
  }
}
