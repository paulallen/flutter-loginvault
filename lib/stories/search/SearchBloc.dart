import 'dart:async';

import 'package:rxdart/rxdart.dart';

import '../../models3/XCard3.dart';
import '../../utils/Logger.dart';
import '../../services/Session3.dart';
import 'CardFilter.dart';

class SearchBloc {
  bool get noVaultAtAll => Session3.instance.theVault == null;
  bool get noCardsAtAll => Session3.instance.theVault?.cards.isEmpty ?? true;

  final _searchText = BehaviorSubject<String>.seeded('');
  String get searchText => Session3.instance.theSearchText;

  void setSearchText(String filter) {
    Session3.instance.theSearchText = filter;
    _searchText.add(filter);
  }

  final _cardVms = BehaviorSubject<List<XCard3>>.seeded([]);
  Stream<List<XCard3>> get cards => _cardVms;

  late StreamSubscription _s1;
  late StreamSubscription _s2;

  // ---

  SearchBloc() {
    logger.log(Level.debug, '$runtimeType()');

    _s1 = Session3.instance.cardsStream.listen(_onData);

    _s2 = _searchText
        .debounceTime(const Duration(milliseconds: 500))
        .listen(_onFilterText);
  }

  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    _s1.cancel();
    _s2.cancel();

    _searchText.close();
    _cardVms.close();
  }

  // ---

  void _onFilterText(String text) {
    final cards = Session3.instance.theVault?.cards ?? <XCard3>[];
    _onData(cards);
  }

  void _onData(List<XCard3> cards) {
    logger.log(Level.debug, '$runtimeType._onData()');
    final cardFilter = CardFilter();

    final matchingCards = cards
        .where((c) => !c.isDeleted)
        .where((c) => cardFilter.matches(c, _searchText.value))
        .toList();
    _cardVms.add(matchingCards);
  }
}
