import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../models3/XCard3.dart';
import '../../services/Session3.dart';
import '../../utils/Logger.dart';
import '../card/CardWidget.dart';
import '../common/CardLoginTile.dart';
import '../common/Dialogs.dart';
import '../common/EmptyListPane.dart';
import '../common/MyIcons.dart';
import '../common/MyTextField.dart';
import '../common/MyFloatingButton.dart';
import '../common/Resources.dart';
import '../common/Ui.dart';
import '../routes/Routes.dart';
import 'SearchBloc.dart';

class SearchWidget extends StatefulWidget {
  SearchWidget({super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<SearchWidget> createState() => _SearchState();
}

class _SearchState extends State<SearchWidget> {
  static final _dialogs = Dialogs();

  final _bloc = SearchBloc();
  late TextEditingController _filterController;

  _SearchState() {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  void initState() {
    logger.log(Level.debug, '$runtimeType.initState()');
    super.initState();

    _filterController = TextEditingController(text: _bloc.searchText);
  }

  @override
  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    _filterController.dispose();
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    logger.log(Level.debug, '$runtimeType.build()');

    return Scaffold(
      body: SafeArea(
        child: _body(),
      ),
      floatingActionButton: MyFloatingButton(
        icon: MyIcons.add,
        onPressed: _onPressedAdd,
      ),
    );
  }

  Widget _body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        _searchEditField(),
        Expanded(
          child: _filteredListBuilder(),
        ),
      ],
    );
  }

  Widget _searchEditField() {
    return Padding(
      padding: Ui.insets16LeftAboveRight,
      child: MyTextField(
        controller: _filterController,
        style: Theme.of(context).textTheme.titleLarge,
        onChanged: (value) => _bloc.setSearchText(value),
        hintText: Resources.searchEditHint,
        inputFormatters: [
          LengthLimitingTextInputFormatter(Ui.kMaxFieldLength),
        ],
      ),
    );
  }

  Widget _filteredListBuilder() {
    return StreamBuilder(
      stream: _bloc.cards,
      builder: _cardListBuilder,
    );
  }

  Widget _cardListBuilder(
      BuildContext context, AsyncSnapshot<List<XCard3>> snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      logger.log(Level.debug, '$runtimeType._cardListBuilder() - waiting');
      return Ui.blankWidget();
    }

    if (_bloc.noVaultAtAll) {
      logger.log(Level.debug, '$runtimeType._cardListBuilder() - no vault');
      return Ui.blankWidget();
    }

    return snapshot.data?.isNotEmpty == true
        ? _cardList(snapshot.data!)
        : _bloc.noCardsAtAll
            ? _noCards()
            : _filterController.text == ''
                ? _noFilter()
                : _noMatches();
  }

  Widget _noFilter() {
    return EmptyListPane(
      title: Resources.noSearchFilterTitle,
      child: Icon(
        MyIcons.card,
        size: 64.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  Widget _noMatches() {
    return EmptyListPane(
      title: Resources.noMatchingCardsTitle,
      child: Icon(
        MyIcons.card,
        size: 64.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  Widget _noCards() {
    return EmptyListPane(
      title: Resources.noCardsTitle,
      text: Resources.addFirstCardText,
      child: Icon(
        MyIcons.card,
        size: 64.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  Widget _cardList(List<XCard3> cards) {
    return ListView.builder(
      itemCount: cards.length,
      itemBuilder: (c, i) => _cardTile(cards[i]),
    );
  }

  Widget _cardTile(XCard3 card) {
    return CardLoginTile(
      card,
      onTap: () => _editCard(card, isEdit: false),
    );
  }

  Future<void> _onPressedAdd() async {
    final card = XCard3();
    return _editCard(card, isEdit: true);
  }

  Future<void> _editCard(XCard3 card, {required bool isEdit}) async {
    try {
      Session3.instance.theCard = card;

      await Navigator.of(context).pushNamed(
        Routes.card,
        arguments: CardParameters(
          isEdit: isEdit,
        ),
      );
    } catch (e, s) {
      logger.logException(Level.warning, '$runtimeType._editCard()', e, s);
      await _dialogs.showErrorDialog(context, e.toString());
    }
  }
}
