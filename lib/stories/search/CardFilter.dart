import '../../models3/XCard3.dart';

class CardFilter {
  bool matches(XCard3 card, String filter) {
    if (filter.isEmpty) return true;

    final upper = filter.toUpperCase();

    return card.url.toUpperCase().contains(upper) ||
        card.username.toUpperCase().contains(upper) ||
        card.password.toUpperCase().contains(upper) ||
        card.label.toUpperCase().contains(upper) ||
        card.other.toUpperCase().contains(upper);
  }
}
