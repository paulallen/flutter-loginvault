import 'dart:async';

import 'package:rxdart/rxdart.dart';

import '../../models3/XCard3.dart';
import '../../services/Session3.dart';
import '../../services/Sorting.dart';
import '../../utils/Logger.dart';

class PickUsernameBloc {
  bool get noVaultAtAll => Session3.instance.theVault == null;
  bool get noCardsAtAll => Session3.instance.theVault?.cards.isEmpty ?? true;

  final _usernames = BehaviorSubject<List<String>>();
  Stream<List<String>> get usernames => _usernames;

  late StreamSubscription _s1;

  // ---

  PickUsernameBloc() {
    logger.log(Level.debug, '$runtimeType()');

    _s1 = Session3.instance.cardsStream.listen(_onData);
  }

  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');

    _s1.cancel();
    _usernames.close();
  }

  // ---

  void _onData(List<XCard3> cards) {
    logger.log(Level.debug, '$runtimeType._onData()');

    final usernames = cards
        .map((c) => c.username)
        .where((username) => username.isNotEmpty)
        .toSet()
        .toList();

    usernames.add('');
    usernames.sort(Sorting.sortAlphabetic);

    _usernames.add(usernames);
  }
}
