import 'package:flutter/material.dart';

import '../../utils/Logger.dart';
import '../common/AppBarTitle.dart';
import '../common/EmptyListPane.dart';
import '../common/MyIcons.dart';
import '../common/Resources.dart';
import '../common/Ui.dart';
import 'PickUsernameBloc.dart';

class PickUsernameWidget extends StatefulWidget {
  PickUsernameWidget({super.key}) {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  State<StatefulWidget> createState() => _PickUsernameState();
}

class _PickUsernameState extends State<PickUsernameWidget> {
  final _bloc = PickUsernameBloc();

  _PickUsernameState() {
    logger.log(Level.debug, '$runtimeType()');
  }

  @override
  void dispose() {
    logger.log(Level.debug, '$runtimeType.dispose()');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    logger.log(Level.debug, '$runtimeType.build()');

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        title: const AppBarTitle(
          Resources.selectUsernameTitle,
        ),
      ),
      body: SafeArea(
        child: _body(),
      ),
    );
  }

  Widget _body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          child: _listBuilder(),
        ),
      ],
    );
  }

  Widget _listBuilder() {
    return StreamBuilder(
      stream: _bloc.usernames,
      builder: (context, AsyncSnapshot<List<String>> snapshot) {
        return _usernamesListBuilder(context, snapshot);
      },
    );
  }

  Widget _usernamesListBuilder(
      BuildContext context, AsyncSnapshot<List<String>> snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      logger.log(Level.debug, '$runtimeType._usernamesListBuilder() - waiting');
      return Ui.blankWidget();
    }

    return snapshot.data?.isNotEmpty == true
        ? _usernameList(snapshot.data!)
        : _bloc.noCardsAtAll
            ? _noCards()
            : _noUsernames();
  }

  Widget _noUsernames() {
    return EmptyListPane(
      title: Resources.noUsernamesTitle,
      text: Resources.addFirstCardText,
      child: Icon(
        MyIcons.username,
        size: 64.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  Widget _noCards() {
    return EmptyListPane(
      title: Resources.noCardsTitle,
      text: Resources.addFirstCardText,
      child: Icon(
        MyIcons.card,
        size: 64.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  ListView _usernameList(List<String> usernames) {
    logger.log(Level.debug, '$runtimeType._list() - ${usernames.length}');

    return ListView.builder(
      itemCount: usernames.length,
      itemBuilder: (c, i) => _tile(usernames[i]),
    );
  }

  Widget _tile(String username) {
    return ListTile(
      leading: username == ''
          ? const Icon(null)
          : Icon(
              MyIcons.username,
              color: Theme.of(context).colorScheme.primary,
            ),
      title: Text(
        username,
        style: Theme.of(context).textTheme.titleLarge,
      ),
      onTap: () => _onTapLabel(username),
    );
  }

  void _onTapLabel(String username) {
    Navigator.of(context).pop(username);
  }
}
