import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'app/App.dart';
import 'services/ColourStrings.dart';
import 'services/Preferences.dart';
import 'services/RootFolder.dart';
import 'services/Session3.dart';
import 'stories/common/Ui.dart';
import 'utils/Logger.dart';

void main() {
  final rootfolder = RootFolder();
  final preferences = Preferences();
  final colourStrings = ColourStrings();

  try {
    FlutterError.onError =
        (FlutterErrorDetails fed) => logger.log(Level.error, 'main() - $fed');

    logger.minLevel = kReleaseMode ? Level.info : Level.debug;
    logger.log(Level.info, 'main()');

    WidgetsFlutterBinding.ensureInitialized();
    rootfolder.getPath().then((rootPath) {
      preferences.read(Ui.kPrimaryColourKey).then((argb) {
        final hsvColour = colourStrings.fromHashArgb(argb) ?? Ui.goldenrod;
        Session3.instance.initState(rootPath, hsvColour);

        runApp(const App());
      });
    });
  } catch (e, s) {
    logger.logException(Level.error, 'main()', e, s);
  }
}
