import 'package:flutter/material.dart';

import '../services/Session3.dart';
import '../stories/common/Resources.dart';
import '../stories/common/Ui.dart';
import '../stories/routes/Routes.dart';
import '../utils/Logger.dart';

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<StatefulWidget> createState() => _AppState();
}

class _AppState extends State<App> {
  final _navKey = GlobalKey<NavigatorState>(debugLabel: 'AppNavigator');

  @override
  Widget build(BuildContext context) {
    logger.log(Level.debug, '$runtimeType.build()');

    return StreamBuilder(
      initialData: Session3.instance.primaryColourValue!,
      stream: Session3.instance.primaryColour,
      builder: (context, AsyncSnapshot<HSVColor> snapshot) {
        logger.log(
            Level.debug, '$runtimeType.build() - ${snapshot.connectionState}');

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const SizedBox.shrink();
        }

        return MaterialApp(
          debugShowCheckedModeBanner: false,
          navigatorKey: _navKey,
          theme: Ui.buildThemeData(snapshot.data!),
          title: Resources.appTitle,
          initialRoute: Routes.home,
          onGenerateRoute: (routeSettings) =>
              Routes.onGenerateRoute(routeSettings),
        );
      },
    );
  }
}
