import '../utils/Logger.dart';
import 'XCard3.dart';

class Vault3 {
  final cards = <XCard3>[];

  Vault3({Iterable<XCard3> cards = const []}) {
    this.cards.addAll(cards);
  }

  Vault3 clone() {
    return Vault3(cards: cards.map((c) => c.clone()));
  }

  @override
  bool operator ==(Object other) {
    final vault = other as Vault3;

    if (cards.length != vault.cards.length) {
      logger.log(
          Level.debug, 'length: ${cards.length} != ${vault.cards.length}');
      return false;
    }

    for (int n = 0; n < cards.length; ++n) {
      if (cards[n] != vault.cards[n]) return false;
    }

    return true;
  }

  @override
  String toString() => '{$runtimeType '
      'cards.length: ${cards.length}'
      '}';
}
