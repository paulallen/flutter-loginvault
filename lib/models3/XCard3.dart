import '../utils/Logger.dart';

class XCard3 {
  String url = '';
  String username = '';
  String password = '';
  String label = '';
  String icon = '';
  String other = '';
  bool isDeleted = false;

  XCard3 clone() {
    return XCard3()
      ..url = url
      ..username = username
      ..password = password
      ..label = label
      ..icon = icon
      ..other = other
      ..isDeleted = isDeleted;
  }

  @override
  bool operator ==(Object other) {
    final card = other as XCard3;

    if (url != card.url) {
      logger.log(Level.debug, 'url: $url != ${card.url}');
      return false;
    }

    if (username != card.username) {
      logger.log(Level.debug, 'username: $username != ${card.username}');
      return false;
    }

    if (password != card.password) {
      logger.log(Level.debug, 'password: $password != ${card.password}');
      return false;
    }

    if (label != card.label) {
      logger.log(Level.debug, 'label: $label != ${card.label}');
      return false;
    }

    if (icon != card.icon) {
      logger.log(Level.debug, 'icon: $icon != ${card.icon}');
      return false;
    }

    if (this.other != card.other) {
      logger.log(Level.debug, 'other: ${this.other} != ${card.other}');
      return false;
    }

    if (isDeleted != card.isDeleted) {
      logger.log(Level.debug, 'isDeleted: $isDeleted != ${card.isDeleted}');
      return false;
    }

    return true;
  }

  @override
  String toString() => '{$runtimeType '
      'url: $url, '
      'username: $username, '
      'password: ********, '
      'label: $label, '
      'icon: $icon, '
      'other: $other, '
      'isDeleted: $isDeleted'
      '}';
}
