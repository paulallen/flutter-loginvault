import 'Folder.dart';

class Vault {
  String name = '';
  final folders = <Folder>[];

  Vault({this.name = '', Iterable<Folder> folders = const []}) {
    this.folders.addAll(folders);
  }

  Vault clone() {
    return Vault(name: name, folders: folders.map((f) => f.clone()));
  }

  @override
  bool operator ==(Object other) {
    final vault = other as Vault;
    if (name != vault.name) return false;
    if (folders.length != vault.folders.length) return false;
    for (int n = 0; n < folders.length; ++n) {
      if (folders[n] != vault.folders[n]) return false;
    }

    return true;
  }

  @override
  String toString() => '{$runtimeType '
      'name: $name, '
      'folders: ${folders.length}'
      '}';
}
