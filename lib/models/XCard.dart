class XCard {
  String url = '';
  String username = '';
  String password = '';
  String other = '';

  XCard clone() {
    return XCard()
      ..url = url
      ..username = username
      ..password = password
      ..other = other;
  }

  @override
  bool operator ==(Object other) {
    final card = other as XCard;
    if (url != card.url) return false;
    if (username != card.username) return false;
    if (password != card.password) return false;
    if (this.other != card.other) return false;
    return true;
  }

  @override
  String toString() => '{$runtimeType '
      'url: $url, '
      'username: $username, '
      'password: ********, '
      'other: $other'
      '}';
}
