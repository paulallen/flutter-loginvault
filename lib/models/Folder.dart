import 'XCard.dart';

class Folder {
  String name = '';
  final cards = <XCard>[];

  Folder({this.name = '', Iterable<XCard> cards = const []}) {
    this.cards.addAll(cards);
  }

  Folder clone() {
    return Folder(name: name, cards: cards.map((c) => c.clone()));
  }

  @override
  bool operator ==(Object other) {
    final folder = other as Folder;
    if (name != folder.name) return false;
    if (cards.length != folder.cards.length) return false;
    for (int n = 0; n < cards.length; ++n) {
      if (cards[n] != (folder.cards[n])) return false;
    }

    return true;
  }

  @override
  String toString() => '{$runtimeType '
      'name: $name, '
      'cards: ${cards.length}'
      '}';
}
