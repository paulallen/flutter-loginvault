class DtoCard3 {
  String url = '';
  String username = '';
  String password = '';
  String other = '';
  String label = '';
  String icon = '';
  bool isDeleted = false;

  static DtoCard3 fromJson(Map<String, dynamic> json) {
    return DtoCard3()
      ..url = json['url'].toString()
      ..username = json['username'].toString()
      ..password = json['password'].toString()
      ..other = json['other'].toString()
      ..label = json['label'].toString()
      ..icon = json['icon'].toString()
      ..isDeleted = json['isDeleted'].toString().toUpperCase() == 'TRUE';
  }

  Map<String, dynamic> toJson() {
    return {
      'url': url,
      'username': username,
      'password': password,
      'other': other,
      'label': label,
      'icon': icon,
      'isDeleted': isDeleted,
    };
  }

  @override
  String toString() => '{$runtimeType '
      'url: $url, '
      'username: $username, '
      'password: ********, '
      'other: $other, '
      'label: $label, '
      'icon: $icon, '
      'isDeleted: $isDeleted'
      '}';
}
