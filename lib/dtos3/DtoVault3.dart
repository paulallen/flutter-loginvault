import 'DtoCard3.dart';

class DtoVault3 {
  final cards = <DtoCard3>[];

  DtoVault3(Iterable<DtoCard3> cards) {
    this.cards.addAll(cards);
  }

  static DtoVault3 fromJson(Map<String, dynamic> json) {
    final cards = json['cards'] as List<dynamic>;

    final c = cards.map((j) => DtoCard3.fromJson(j as Map<String, dynamic>));

    return DtoVault3(c);
  }

  Map<String, dynamic> toJson() {
    return {
      'cards': cards.map((f) => f.toJson()).toList(),
    };
  }

  @override
  String toString() => '{$runtimeType '
      'cards: ${cards.length}'
      '}';
}
