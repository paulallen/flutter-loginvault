import 'package:intl/intl.dart';

enum Level { debug, info, warning, error, none }

class Logger {
  final _formatter = DateFormat('HH:mm:ss.S');

  void Function(Object object) implementation = print;
  Level minLevel = Level.none;

  void logException(Level level, String message, dynamic e, StackTrace s) {
    final msg = '$message: $e\n$s';
    log(level, msg);
  }

  void log(Level level, String message) {
    if (level.index >= minLevel.index) {
      implementation(format(level, message));
    }
  }

  String format(Level level, String message) {
    final lvl = levelName(level);
    final now = _formatter.format(DateTime.now());

    return '$now $lvl $message';
  }

  String levelName(Level level) {
    switch (level) {
      case Level.debug:
        return 'D';
      case Level.info:
        return 'I';
      case Level.warning:
        return 'W';
      case Level.error:
        return 'E';
      case Level.none:
        return '-';
    }
  }
}

final Logger logger = Logger();
