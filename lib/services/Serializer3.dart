import 'dart:convert';

import '../dtos3/DtoVault3.dart';

class Serializer3 {
  String serialize(DtoVault3 vault) {
    final jsonVault = vault.toJson();
    return json.encode(jsonVault);
  }

  DtoVault3 deserialize(String string) {
    final jsonVault = json.decode(string) as Map<String, dynamic>;
    return DtoVault3.fromJson(jsonVault);
  }
}
