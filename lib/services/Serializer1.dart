import 'package:xml/xml.dart';

import '../dtos/DtoCard.dart';
import '../dtos/DtoFolder.dart';
import '../dtos/DtoVault.dart';

class Serializer1 {
  String serialize(DtoVault vault) {
    final foldersElement = XmlElement(XmlName('Folders'));

    for (final f in vault.folders) {
      final cardsElement = XmlElement(XmlName('Cards'));
      for (final c in f.cards) {
        final urlElement = XmlElement(XmlName('Url'));
        urlElement.children.add(XmlText(c.url));

        final usernameElement = XmlElement(XmlName('Username'));
        usernameElement.children.add(XmlText(c.username));

        final passwordElement = XmlElement(XmlName('Password'));
        passwordElement.children.add(XmlText(c.password));

        final otherElement = XmlElement(XmlName('Other'));
        otherElement.children.add(XmlText(c.other));

        final cardElement = XmlElement(XmlName('Card'));
        cardElement.children.add(urlElement);
        cardElement.children.add(usernameElement);
        cardElement.children.add(passwordElement);
        cardElement.children.add(otherElement);

        cardsElement.children.add(cardElement);
      }

      final folderNameElement = XmlElement(XmlName('Name'));
      folderNameElement.children.add(XmlText(f.name));

      final folderElement = XmlElement(XmlName('Folder'));
      folderElement.children.add(folderNameElement);
      folderElement.children.add(cardsElement);

      foldersElement.children.add(folderElement);
    }

    final boxNameElement = XmlElement(XmlName('Name'));
    boxNameElement.children.add(XmlText(vault.name));

    final boxElement = XmlElement(XmlName('Box'));
    boxElement.children.add(boxNameElement);
    boxElement.children.add(foldersElement);

    final document = XmlDocument();
    document.children.add(boxElement);

    return document.toXmlString(pretty: true);
  }

  DtoVault deserialize(String string) {
    final document = XmlDocument.parse(string);
    final boxElement = document.getElement('Box');
    final boxName = boxElement?.getElement('Name')?.firstChild?.value ?? '';

    final folders = <DtoFolder>[];

    final foldersElement = boxElement!.getElement('Folders');
    final folderElements = foldersElement!.findAllElements('Folder');
    for (final f in folderElements) {
      final folderName = f.getElement('Name')?.firstChild?.value ?? '';
      final cards = <DtoCard>[];

      final cardsElement = f.findAllElements('Cards').firstOrNull;
      final cardElements = cardsElement!.findAllElements('Card');
      for (final c in cardElements) {
        final url = c.getElement('Url')?.firstChild?.value ?? '';
        final username = c.getElement('Username')?.firstChild?.value ?? '';
        final password = c.getElement('Password')?.firstChild?.value ?? '';
        final other = c.getElement('Other')?.firstChild?.value ?? '';

        final card = DtoCard()
          ..url = url
          ..username = username
          ..password = password
          ..other = other;

        cards.add(card);
      }

      folders.add(DtoFolder(folderName, cards));
    }

    return DtoVault(boxName, folders);
  }
}
