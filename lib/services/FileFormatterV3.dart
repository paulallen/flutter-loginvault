import 'dart:math';

class FileFormatterV3 {
  bool isV3(String content) => content.startsWith(v3Header);

  String parse(String content) {
    final raw = content.split('\n');
    final lines = removeCarriageReturn(raw);

    if (lines[0] != v3Header) throw Exception('Header');
    if (lines[lines.length - 1] != v3Footer) throw Exception('Footer');

    var payload = '';
    for (int i = 1; i < lines.length - 1; ++i) {
      payload += lines[i];
    }

    return payload;
  }

  String format(String payload) {
    var content = v3Header;
    content += '\n';

    var remaining = payload.length;
    for (int i = 0; i < payload.length; i += 64) {
      final line = payload.substring(i, i + min<int>(64, remaining));
      content += line;
      content += '\n';
      remaining -= 64;
    }

    content += v3Footer;
    content += '\n';

    return content;
  }

  List<String> removeCarriageReturn(List<String> lines) {
    final normal = <String>[];

    for (final line in lines) {
      if (line.endsWith('\r')) {
        final trimCR = line.substring(0, line.length - 1);
        normal.add(trimCR);
      } else {
        normal.add(line);
      }
    }

    if (normal[normal.length - 1] == '') {
      normal.removeLast();
    }

    return normal;
  }

  static const String v3Header = '===== BEGIN LOGINVAULT V3 =====';
  static const String v3Footer = '===== END LOGINVAULT V3 =====';
}
