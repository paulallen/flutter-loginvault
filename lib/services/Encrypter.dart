import 'dart:convert';
import 'dart:typed_data';
import 'BinaryEncrypter.dart';

class Encrypter {
  final binary = BinaryEncrypter();

  String encrypt(String plaintext, String password) {
    final payload = utf8.encode(plaintext);
    final pwd = utf8.encode(password);
    final cipher = binary.encryptBuffer(Uint8List.fromList(payload), Uint8List.fromList(pwd));
    return base64.encode(cipher);
  }

  String decrypt(String ciphertext, String password) {
    final payload = base64.decode(ciphertext);
    final pwd = utf8.encode(password);
    final plain = binary.decryptBuffer(payload, Uint8List.fromList(pwd));
    return utf8.decode(plain);
  }
}
