import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../utils/Logger.dart';

class SecureStorage {
  static bool _initialized = false;
  static FlutterSecureStorage? _storage;

  Future<String?> read(String key) async {
    logger.log(Level.debug, '$runtimeType.read(key: $key)');
    assert(key != '');

    final storage = await _init();
    if (storage == null) return null;

    return storage.read(key: key);
  }

  Future<void> write(String key, String value) async {
    logger.log(Level.debug, '$runtimeType.write(key: $key)');
    assert(key != '');

    final storage = await _init();
    if (storage == null) return;

    await storage.write(key: key, value: value);
  }

  Future<void> delete(String key) async {
    logger.log(Level.debug, '$runtimeType.delete(key: $key)');
    assert(key != '');

    final storage = await _init();
    if (storage == null) return;

    await storage.delete(key: key);
  }

  Future<bool> canUseStorage() async {
    logger.log(Level.debug, '$runtimeType.canUseStorage()');
    final storage = await _init();
    return storage != null;
  }

  static Future<FlutterSecureStorage?> _init() async {
    if (!_initialized) {
      try {
        const testStorage = FlutterSecureStorage();

        const unlikelyKey = '7c16099c-6baa-4ff0-87aa-3b98266d72dc';
        final _ = await testStorage.read(key: unlikelyKey);

        _storage = testStorage;
      } on MissingPluginException catch (e, s) {
        logger.logException(Level.warning, 'No secure storage plugin', e, s);
      }
    }
    _initialized = true;
    return _storage;
  }
}
