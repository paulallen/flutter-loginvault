import 'dart:io';

class FileIo {
  Future<List<FileSystemEntity>> listFileSystemEntities(String folderPath) {
    final folder = Directory(folderPath);
    return (folder.list()).toList();
  }

  Future<void> deleteFile(String filePath) async {
    final file = File(filePath);
    if (file.existsSync()) {
      await file.delete();
    }
  }

  Future<bool> exists(String filePath) async {
    return File(filePath).exists();
  }

  Future<String> readFile(String filePath) async {
    final file = File(filePath);
    return file.readAsString();
  }

  Future<void> writeFile(String filePath, String content) async {
    final file = File(filePath);
    file.writeAsStringSync(content, flush: true);
  }
}
