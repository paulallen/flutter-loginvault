import 'package:path/path.dart' as path;

class NameMapper {
  static const String extension = '.lv1';

  String toFileName(String vaultName) {
    return vaultName + extension;
  }

  String toFilePath(String folderPath, String vaultName) {
    final fileName = vaultName + extension;
    return path.join(folderPath, fileName);
  }

  String toVaultName(String filePath) {
    final fileName = path.basename(filePath);
    final index = fileName.indexOf(extension);
    return index > 0 ? fileName.substring(0, index) : fileName;
  }
}
