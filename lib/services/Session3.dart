import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../../utils/Logger.dart';
import '../models3/Vault3.dart';
import '../models3/XCard3.dart';
import '../services/AutoSaver.dart';
import 'Sorting.dart';

class Session3 {
  static final Session3 _instance = Session3();
  static Session3 get instance => _instance;

  // --

  String thePath = '';
  String theSearchText = '';
  String theSelectedLabel = '';
  XCard3? theCard;

  // --

  String _theRootPath = '';
  String get theRootPath => _theRootPath;

  String? _theVaultName;
  String? get theVaultName => _theVaultName;

  String? _theVaultPath;
  String? get theVaultPath => _theVaultPath;

  AutoSaver? _theAutoSaver;
  AutoSaver? get theAutoSaver => _theAutoSaver;

  Vault3? _theVault;
  Vault3? get theVault => _theVault;

  final _vaultSubject = BehaviorSubject<Vault3?>();
  Stream<Vault3?> get vaultStream => _vaultSubject;

  final _cardsSubject = BehaviorSubject<List<XCard3>>();
  Stream<List<XCard3>> get cardsStream => _cardsSubject;

  void setVault(Vault3? vault) {
    _theVault = vault;
    _cardsSubject.add(_theVault?.cards ?? []);
    _vaultSubject.add(vault);
  }

  // --

  final _messages = BehaviorSubject<String>();
  Stream<String> get messages => _messages.distinct();
  void showMessage(String message) {
    _messages.add(message);
  }

  // --

  final _primaryColour = BehaviorSubject<HSVColor>();
  Stream<HSVColor> get primaryColour => _primaryColour;
  HSVColor? get primaryColourValue => _primaryColour.valueOrNull;

  void setPrimaryColour(HSVColor colour) {
    _primaryColour.add(colour);
  }

  // --

  void initState(String rootPath, HSVColor primaryColour) {
    logger.log(Level.debug,
        '$runtimeType.initState(rootPath: $rootPath, primaryColour: $primaryColour)');

    setPrimaryColour(primaryColour);

    _theRootPath = rootPath;
    thePath = _theRootPath;
  }

  void dispose() {
    _messages.close();
    _primaryColour.close();
    _cardsSubject.close();
    _vaultSubject.close();
  }

  void openVault(
    String filePath,
    String password,
    String vaultName,
    Vault3 vault,
  ) {
    logger.log(Level.debug, '$runtimeType.openVault(vaultName: $vaultName)');

    _theVaultName = vaultName;
    _theVaultPath = filePath;
    _theAutoSaver = AutoSaver(
      vault,
      filePath,
      password,
    );
    final _ = theAutoSaver!.run();
    setVault(vault);
  }

  Future<void> closeVault() async {
    logger.log(Level.debug, '$runtimeType.closeVault()');

    if (theAutoSaver != null) {
      await theAutoSaver!.stop();
    }

    _theVaultName = '';
    _theVaultPath = "";
    _theAutoSaver = null;
    theSelectedLabel = '';
    theCard = null;
    setVault(null);
  }

  void putCard(XCard3 card, XCard3 newcard) {
    if (theVault == null) {
      logger.log(Level.error, '$runtimeType.putCard() - no theVault');
      return;
    }

    if (theVault!.cards.where((c) => c.hashCode == card.hashCode).isNotEmpty) {
      theVault!.cards.removeWhere((c) => c.hashCode == card.hashCode);
    }

    theVault!.cards.add(newcard);
    theVault!.cards.sort(Sorting.sortCards);

    _cardsSubject.add(theVault!.cards);
  }

  void deleteCard(XCard3 card, {required bool delete}) {
    if (theVault == null) {
      logger.log(Level.error, '$runtimeType.deleteCard() - no theVault');
      return;
    }

    if (theVault!.cards.where((c) => c.hashCode == card.hashCode).isEmpty) {
      logger.log(Level.debug, 'deleteCard() - no matching card');
      return;
    }

    logger.log(Level.debug, 'deleteCard() - Deleting card $card');

    if (card.isDeleted && delete) {
      theVault!.cards.removeWhere((c) => c.hashCode == card.hashCode);
    } else {
      card.isDeleted = delete;
    }

    _cardsSubject.add(theVault!.cards);
  }
}
