import '../dtos/DtoCard.dart';
import '../dtos/DtoVault.dart';
import '../models3/Vault3.dart';
import '../models3/XCard3.dart';

class Model3Upgrader {
  static const String deletedCardsFolderName = 'Deleted Cards';

  XCard3 toModelCard(String label, DtoCard card) {
    return XCard3()
      ..url = card.url
      ..username = card.username
      ..password = card.password
      ..label = label
      ..other = card.other
      ..isDeleted = false;
  }

  Vault3 toModelVault(DtoVault vault) {
    final vault3 = Vault3();

    for (final dtoFolder in vault.folders) {
      for (final dtoCard in dtoFolder.cards) {
        vault3.cards.add(toModelCard(dtoFolder.name, dtoCard));
      }
    }

    for (final card in vault3.cards
        .where((element) => element.label == deletedCardsFolderName)) {
      card.isDeleted = true;
      card.label = 'Lost and found';
    }

    return vault3;
  }
}
