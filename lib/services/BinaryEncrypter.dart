import 'dart:math';
import 'dart:typed_data';
import 'package:pointycastle/export.dart';

class BinaryEncrypter {
  final assember = PayloadAssember();

  Uint8List encryptBuffer(Uint8List payload, Uint8List password) {
    // Salts, IV
    final random = Random.secure();
    final saltA = Uint8List.fromList(List<int>.generate(
        PayloadAssember.payloadSaltALength, (i) => random.nextInt(256)));
    final pwdSalt = Uint8List.fromList(List<int>.generate(
        PayloadAssember.passwordSaltLength, (i) => random.nextInt(256)));
    final iv = Uint8List.fromList(List<int>.generate(
        PayloadAssember.initialVectorLength, (i) => random.nextInt(256)));
    final saltB = Uint8List.fromList(List<int>.generate(
        PayloadAssember.payloadSaltBLength, (i) => random.nextInt(256)));

    // Encrypt
    final saltedPwd = saltedPassword(password, pwdSalt);
    final cipher = blockCipher(saltedPwd, iv, isForEncryption: true);
    final ciphertext = cipher.process(payload);

    // Assemble
    return assember.assemblePayload(saltA, pwdSalt, iv, ciphertext, saltB);
  }

  Uint8List decryptBuffer(Uint8List payload, Uint8List password) {
    // Disassemble
    final components = assember.disassemblePayload(payload);

    // Decrypt
    final saltedPwd = saltedPassword(password, components.passwordSalt);
    final cipher =
        blockCipher(saltedPwd, components.iv, isForEncryption: false);
    return cipher.process(components.ciphertext);
  }

  PaddedBlockCipherImpl blockCipher(
    Uint8List saltedPassword,
    Uint8List iv, {
    required bool isForEncryption,
  }) {
    final params = PaddedBlockCipherParameters(
      ParametersWithIV(
        KeyParameter(saltedPassword),
        iv,
      ),
      null,
    );

    final aes = PaddedBlockCipherImpl(
        PKCS7Padding(),
        CBCBlockCipher(
          AESEngine(),
        ));

    aes.init(
      isForEncryption,
      params,
    );

    return aes;
  }

  Uint8List saltedPassword(Uint8List password, Uint8List pwdSalt) {
    final salted = <int>[];

    salted.addAll(password);
    salted.addAll(pwdSalt);
    assert(salted.length == password.length + pwdSalt.length);

    final saltedPwd = Uint8List.fromList(salted.sublist(0, keysizeBytes));
    // logger.log(Level.debug, 'saltedPassword() - saltedPwd: ${base64.encode(saltedPwd)}');

    return saltedPwd;
  }
}

class PayloadComponents {
  Uint8List saltA;
  Uint8List passwordSalt;
  Uint8List iv;
  Uint8List ciphertext;
  Uint8List saltB;

  PayloadComponents(
      this.saltA, this.passwordSalt, this.iv, this.ciphertext, this.saltB);
}

class PayloadAssember {
  Uint8List assemblePayload(Uint8List saltA, Uint8List pwdSalt, Uint8List iv,
      Uint8List ciphertext, Uint8List saltB) {
    final payload = <int>[];

    payload.addAll(saltA);
    payload.addAll(pwdSalt);
    payload.addAll(iv);
    payload.addAll(ciphertext);
    payload.addAll(saltB);

    assert(payload.length ==
        saltA.length +
            pwdSalt.length +
            iv.length +
            ciphertext.length +
            saltB.length);

    // logger.log(Level.debug, 'assemblePayload() - saltA: ${base64.encode(saltA)}');
    // logger.log(Level.debug, 'assemblePayload() - pwdSalt: ${base64.encode(pwdSalt)}');
    // logger.log(Level.debug, 'assemblePayload() - iv: ${base64.encode(iv)}');
    // logger.log(Level.debug, 'assemblePayload() - ciphertext: ${base64.encode(ciphertext)}');
    // logger.log(Level.debug, 'assemblePayload() - saltB: ${base64.encode(saltB)}');
    // logger.log(Level.debug, 'assemblePayload() - payload: ${base64.encode(payload)}');

    return Uint8List.fromList(payload);
  }

  PayloadComponents disassemblePayload(Uint8List payload) {
    assert(payload.length >
        payloadSaltALength +
            payloadSaltBLength +
            passwordSaltLength +
            initialVectorLength +
            1);

    final msgLen = payload.length -
        payloadSaltALength -
        payloadSaltBLength -
        passwordSaltLength -
        initialVectorLength;
    var start = 0;

    final saltA = payload.getRange(0, payloadSaltALength).toList();
    start += payloadSaltALength;

    final pwdSalt =
        payload.getRange(start, start + passwordSaltLength).toList();
    start += passwordSaltLength;

    final iv = payload.getRange(start, start + initialVectorLength).toList();
    start += initialVectorLength;

    final ciphertext = payload.getRange(start, start + msgLen).toList();
    start += msgLen;

    final saltB = payload.getRange(start, start + payloadSaltBLength).toList();
    start += payloadSaltBLength;

    assert(start == payload.length);

    // logger.log(Level.debug, 'disassemblePayload() - payload: ${base64.encode(payload)}');
    // logger.log(Level.debug, 'disassemblePayload() - saltA: ${base64.encode(saltA)}');
    // logger.log(Level.debug, 'disassemblePayload() - pwdSalt: ${base64.encode(pwdSalt)}');
    // logger.log(Level.debug, 'disassemblePayload() - iv: ${base64.encode(iv)}');
    // logger.log(Level.debug, 'disassemblePayload() - ciphertext: ${base64.encode(ciphertext)}');
    // logger.log(Level.debug, 'disassemblePayload() - saltB: ${base64.encode(saltB)}');

    return PayloadComponents(
      Uint8List.fromList(saltA),
      Uint8List.fromList(pwdSalt),
      Uint8List.fromList(iv),
      Uint8List.fromList(ciphertext),
      Uint8List.fromList(saltB),
    );
  }

  static const int initialVectorLength = blocksizeBytes;
  static const int passwordSaltLength = keysizeBytes;
  static const int payloadSaltALength = 13;
  static const int payloadSaltBLength = 7;
}

const int blocksizeBytes = 16;
const int keysizeBytes = 32;
