import 'dart:math';

class FileFormatterV1 {
  bool isV1(String content) => content.startsWith(v1Header);

  String parse(String content) {
    final raw = content.split('\n');
    final lines = normalize(raw);

    if (lines[0] != v1Header) throw Exception('Header');
    if (lines[lines.length - 1] != v1Footer) throw Exception('Footer');

    var payload = '';
    for (int i = 1; i < lines.length - 1; ++i) {
      payload += lines[i];
    }

    return payload;
  }

  String format(String payload) {
    var content = v1Header;
    content += '\n';

    var remaining = payload.length;
    for (int i = 0; i < payload.length; i += 64) {
      final line = payload.substring(i, i + min<int>(64, remaining));
      content += line;
      content += '\n';
      remaining -= 64;
    }

    content += v1Footer;
    content += '\n';

    return content;
  }

  List<String> normalize(List<String> lines) {
    final normal = <String>[];

    for (final line in lines) {
      if (line.endsWith('\r')) {
        final trimCR = line.substring(0, line.length - 1);
        normal.add(trimCR);
      } else {
        normal.add(line);
      }
    }

    if (normal[normal.length - 1] == '') {
      normal.removeLast();
    }

    return normal;
  }

  static const String v1Header = '===== BEGIN LOGINVAULT V1 =====';
  static const String v1Footer = '===== END LOGINVAULT V1 =====';
}
