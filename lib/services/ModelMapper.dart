import '../dtos/DtoCard.dart';
import '../dtos/DtoFolder.dart';
import '../dtos/DtoVault.dart';
import '../models/Folder.dart';
import '../models/Vault.dart';
import '../models/XCard.dart';

class ModelMapper {
  DtoCard toDtoCard(XCard card) {
    return DtoCard()
      ..url = card.url
      ..username = card.username
      ..password = card.password
      ..other = card.other;
  }

  XCard toModelCard(DtoCard card) {
    return XCard()
      ..url = card.url
      ..username = card.username
      ..password = card.password
      ..other = card.other;
  }

  DtoFolder toDtoFolder(Folder folder) {
    return DtoFolder(folder.name, folder.cards.map(toDtoCard));
  }

  Folder toModelFolder(DtoFolder folder) {
    return Folder(name: folder.name, cards: folder.cards.map(toModelCard));
  }

  DtoVault toDtoVault(Vault vault) {
    return DtoVault(vault.name, vault.folders.map(toDtoFolder));
  }

  Vault toModelVault(DtoVault vault) {
    return Vault(name: vault.name, folders: vault.folders.map(toModelFolder));
  }
}
