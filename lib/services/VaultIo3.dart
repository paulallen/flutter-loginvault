import '../models3/Vault3.dart';
import '../utils/Logger.dart';
import 'Encrypter.dart';
import 'FileFormatterV1.dart';
import 'FileFormatterV2.dart';
import 'FileFormatterV3.dart';
import 'FileIo.dart';
import 'Model3Mapper.dart';
import 'Model3Upgrader.dart';
import 'Serializer1.dart';
import 'Serializer2.dart';
import 'Serializer3.dart';
import 'Session3.dart';

class VaultIo3 {
  static final _model3mapper = Model3Mapper();
  static final _model3upgrader = Model3Upgrader();
  static final _serializer1 = Serializer1();
  static final _serializer2 = Serializer2();
  static final _serializer3 = Serializer3();
  static final _formatterV1 = FileFormatterV1();
  static final _formatterV2 = FileFormatterV2();
  static final _formatterV3 = FileFormatterV3();
  static final _encrypter = Encrypter();
  static final _fileio = FileIo();

  Future<Vault3> loadVault(String filePath, String password) async {
    assert(filePath != '');

    final content = await _fileio.readFile(filePath);
    final isV3 = _formatterV3.isV3(content);
    final isV2 = _formatterV2.isV2(content);
    final isV1 = _formatterV1.isV1(content);

    if (isV3) {
      final ciphertext = _formatterV3.parse(content);
      final plaintext = _encrypter.decrypt(ciphertext, password);
      final dto3 = _serializer3.deserialize(plaintext);
      final vault3 = _model3mapper.toModelVault(dto3);
      logger.log(Level.info, 'Loaded V3 vault filePath: $filePath');
      return vault3;
    } else if (isV2) {
      final ciphertext = _formatterV2.parse(content);
      final plaintext = _encrypter.decrypt(ciphertext, password);
      final dto = _serializer2.deserialize(plaintext);
      final vault3 = _model3upgrader.toModelVault(dto);

      Session3.instance.showMessage('Upgraded V2 vault');
      logger.log(Level.info, 'Loaded V2 vault filePath: $filePath');
      return vault3;
    } else if (isV1) {
      final ciphertext = _formatterV1.parse(content);
      final plaintext = _encrypter.decrypt(ciphertext, password);
      final dto = _serializer1.deserialize(plaintext);
      final vault3 = _model3upgrader.toModelVault(dto);

      Session3.instance.showMessage('Upgraded V1 vault');
      logger.log(Level.info, 'Loaded V1 vault filePath: $filePath');
      return vault3;
    } else {
      throw Exception('Invalid file header');
    }
  }

  Future<void> saveVault(String filePath, Vault3 vault, String password) async {
    assert(filePath != '');

    final dto = _model3mapper.toDtoVault(vault);
    final plaintext = _serializer3.serialize(dto);
    final ciphertext = _encrypter.encrypt(plaintext, password);
    final content = _formatterV3.format(ciphertext);
    await _fileio.writeFile(filePath, content);

    logger.log(Level.info, 'Saved vault filePath: $filePath');
  }

  Future<void> deleteVault(String filePath) async {
    assert(filePath != '');

    return _fileio.deleteFile(filePath);
  }
}
