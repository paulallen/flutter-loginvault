import '../dtos3/DtoCard3.dart';
import '../dtos3/DtoVault3.dart';
import '../models3/Vault3.dart';
import '../models3/XCard3.dart';

class Model3Mapper {
  DtoCard3 toDtoCard(XCard3 card) {
    return DtoCard3()
      ..url = card.url
      ..username = card.username
      ..password = card.password
      ..other = card.other
      ..label = card.label
      ..icon = card.icon
      ..isDeleted = card.isDeleted;
  }

  XCard3 toModelCard(DtoCard3 dtoCard) {
    return XCard3()
      ..url = dtoCard.url
      ..username = dtoCard.username
      ..password = dtoCard.password
      ..other = dtoCard.other
      ..label = dtoCard.label
      ..icon = dtoCard.icon
      ..isDeleted = dtoCard.isDeleted;
  }

  DtoVault3 toDtoVault(Vault3 vault) {
    final dtoCards = vault.cards.map((c) => toDtoCard(c));
    return DtoVault3(dtoCards);
  }

  Vault3 toModelVault(DtoVault3 dtoVault) {
    final cards = dtoVault.cards.map((c) => toModelCard(c));
    return Vault3(cards: cards);
  }
}
