import '../models3/Vault3.dart';
import '../stories/common/Resources.dart';
import '../utils/Logger.dart';
import 'Session3.dart';
import 'VaultIo3.dart';

class AutoSaver {
  static final _vaultio3 = VaultIo3();

  final Vault3 _vault;
  final String _filePath;
  final String _password;

  Vault3 _lastSavedVault;
  bool _isActive;

  AutoSaver(this._vault, this._filePath, this._password)
      : _lastSavedVault = _vault.clone(),
        _isActive = true;

  Future<void> run() async {
    logger.log(Level.debug, '$runtimeType.run()');

    do {
      await Future.delayed(const Duration(seconds: 10));

      if (_isActive) {
        await saveIfChanged();
      }
    } while (_isActive);
  }

  Future<void> stop() async {
    logger.log(Level.debug, '$runtimeType.stop()');

    _isActive = false;
    await saveIfChanged();
  }

  Future<void> saveIfChanged() async {
    final localClone = _vault.clone();
    if (localClone != _lastSavedVault) {
      await _saveVault(localClone, _filePath, _password);
      _lastSavedVault = localClone;
    }
  }

  Future<void> _saveVault(
    Vault3 vault,
    String filePath,
    String password,
  ) async {
    await _vaultio3.deleteVault(filePath);
    await _vaultio3.saveVault(filePath, vault, password);
    Session3.instance.showMessage(Resources.savedVaultMessage);
  }
}
