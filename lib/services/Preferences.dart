import 'package:shared_preferences/shared_preferences.dart';

import '../utils/Logger.dart';

class Preferences {
  static bool _initialized = false;
  static SharedPreferences? _sharedPreferences;

  Future<String?> read(String key) async {
    logger.log(Level.debug, '$runtimeType.read(key: $key)');
    final prefs = await _init();

    return prefs.getString(key);
  }

  Future<void> write(String key, String value) async {
    logger.log(Level.debug, '$runtimeType.write(key: $key, value: $value)');
    final prefs = await _init();

    await prefs.setString(key, value);
  }

  Future<void> delete(String key) async {
    logger.log(Level.debug, '$runtimeType.delete(key: $key)');
    final prefs = await _init();

    await prefs.remove(key);
  }

  static Future<SharedPreferences> _init() async {
    if (!_initialized) {
      _sharedPreferences = await SharedPreferences.getInstance();
    }
    _initialized = true;
    return _sharedPreferences!;
  }
}
