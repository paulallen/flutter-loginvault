import '../models3/XCard3.dart';

class Sorting {
  static int sortCards(XCard3 card1, XCard3 card2) {
    return sortAlphabetic(card1.url, card2.url);
  }

  static int sortLabels(String label1, String label2) {
    return sortAlphabetic(label1, label2);
  }

  static int sortAlphabetic(String name1, String name2) {
    final upper1 = name1.toUpperCase();
    final upper2 = name2.toUpperCase();

    return upper1.compareTo(upper2);
  }
}
