import 'dart:convert';

import '../dtos/DtoVault.dart';

class Serializer2 {
  String serialize(DtoVault vault) {
    final jsonVault = vault.toJson();
    return json.encode(jsonVault);
  }

  DtoVault deserialize(String string) {
    final jsonVault = json.decode(string) as Map<String, dynamic>;
    return DtoVault.fromJson(jsonVault);
  }
}
