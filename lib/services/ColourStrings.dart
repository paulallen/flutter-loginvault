import 'package:flutter/material.dart';

class ColourStrings {
  String toHashArgb(HSVColor hsvColour) {
    final colour = hsvColour.toColor();
    final hex = colour.value.toRadixString(16);
    final hashArgb = '#${hex.padLeft(8, '0')}';

    assert(hashArgb.startsWith('#'));
    assert(hashArgb.length == 9);

    return hashArgb;
  }

  HSVColor? fromHashArgb(String? hashArgb) {
    if (hashArgb == null || hashArgb.length != 9 || hashArgb[0] != '#') {
      return null;
    }

    final argb = hashArgb.substring(1);

    final colourValue = int.parse(argb, radix: 16);
    final a = (colourValue & 0xff000000) >> 24;
    final r = (colourValue & 0x00ff0000) >> 16;
    final g = (colourValue & 0x0000ff00) >> 8;
    final b = colourValue & 0x000000ff;

    return HSVColor.fromColor(Color.fromARGB(a, r, g, b));
  }
}
