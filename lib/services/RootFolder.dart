import 'dart:io' show Directory;

import 'package:path_provider/path_provider.dart';

class RootFolder {
  static Directory? _directory;

  Future<String> getPath() async {
    _directory ??= await _getDirectory();

    return _directory!.path;
  }

  Future<Directory?> _getDirectory() async {
    return getApplicationDocumentsDirectory();
  }
}
