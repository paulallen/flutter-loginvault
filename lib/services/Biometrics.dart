import 'dart:io';

import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';

import '../utils/Logger.dart';

class Biometrics {
  static bool _initialized = false;
  static bool _isInAuthenticate = false;
  static LocalAuthentication? _localAuth;

  Future<bool> authenticate({
    required String reason,
  }) async {
    logger.log(Level.debug, '$runtimeType.authenticate()');

    if (_isInAuthenticate) {
      logger.log(Level.warning, '$runtimeType.authenticate() - reentry');
      return false;
    }

    await _oneTimeInitialize();
    if (_localAuth == null) return false;

    try {
      _isInAuthenticate = true;

      final isAuthenticated = await _localAuth!.authenticate(
          localizedReason: reason,
          options: const AuthenticationOptions(
            biometricOnly: false,
            stickyAuth: false,
          ));

      return isAuthenticated;
    } finally {
      _isInAuthenticate = false;
    }
  }

  Future<bool> canUseBiometrics() async {
    logger.log(Level.debug, '$runtimeType.canUseBiometrics()');

    await _oneTimeInitialize();
    return _localAuth != null;
  }

  Future<List<BiometricType>> listAvailableBiometricTypes() async {
    logger.log(Level.debug, '$runtimeType.listAvailableBiometricTypes()');

    await _oneTimeInitialize();
    if (_localAuth == null) return [];

    return _localAuth!.getAvailableBiometrics();
  }

  static Future _oneTimeInitialize() async {
    if (_initialized) return;
    if (Platform.isWindows) return;

    try {
      final testLocal = LocalAuthentication();

      final canWe = await testLocal.canCheckBiometrics;
      final isSupported = await testLocal.isDeviceSupported();

      if (canWe && isSupported) {
        logger.log(Level.debug, 'Biometrics._init() - yes we can');
        _localAuth = testLocal;
      } else {
        logger.log(Level.debug, 'Biometrics._init() - no we can not');
      }
    } on MissingPluginException catch (e, s) {
      logger.logException(Level.warning, 'No biometrics plugin', e, s);
    } finally {
      _initialized = true;
    }
  }
}
