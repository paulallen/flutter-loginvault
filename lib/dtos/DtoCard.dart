class DtoCard {
  String url = '';
  String username = '';
  String password = '';
  String other = '';

  static DtoCard fromJson(Map<String, dynamic> json) {
    return DtoCard()
      ..url = json['url'].toString()
      ..username = json['username'].toString()
      ..password = json['password'].toString()
      ..other = json['other'].toString();
  }

  Map<String, dynamic> toJson() {
    return {
      'url': url,
      'username': username,
      'password': password,
      'other': other,
    };
  }

  @override
  String toString() => '{$runtimeType '
      'url: $url, '
      'username: $username, '
      'password: ********, '
      'other: $other'
      '}';
}
