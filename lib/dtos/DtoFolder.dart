import 'DtoCard.dart';

class DtoFolder {
  String name = '';
  final cards = <DtoCard>[];

  DtoFolder(this.name, Iterable<DtoCard> cards) {
    this.cards.addAll(cards);
  }

  static DtoFolder fromJson(Map<String, dynamic> json) {
    final n = json['name'].toString();

    final cards = json['cards'] as List<dynamic>;
    final c = cards.map((j) => DtoCard.fromJson(j as Map<String, dynamic>));

    return DtoFolder(n, c);
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'cards': cards.map((c) => c.toJson()).toList(),
    };
  }

  @override
  String toString() => '{$runtimeType '
      'name: $name, '
      'cards: ${cards.length}'
      '}';
}
