import 'DtoFolder.dart';

class DtoVault {
  String name = '';
  final folders = <DtoFolder>[];

  DtoVault(this.name, Iterable<DtoFolder> folders) {
    this.folders.addAll(folders);
  }

  static DtoVault fromJson(Map<String, dynamic> json) {
    final n = json['name'].toString();

    final folders = json['folders'] as List<dynamic>;
    final f = folders.map((j) => DtoFolder.fromJson(j as Map<String, dynamic>));

    return DtoVault(n, f);
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'folders': folders.map((f) => f.toJson()).toList(),
    };
  }

  @override
  String toString() => '{$runtimeType '
      'name: $name, '
      'folders: ${folders.length}'
      '}';
}
