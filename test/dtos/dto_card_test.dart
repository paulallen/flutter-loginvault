import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/dtos/DtoCard.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data.dart';

void main() {
  logger.minLevel = Level.warning;

  group('Card', () {
    test('toString() works', () {
      final string = testDtoCard2.toString();

      expect(string,
          '{DtoCard url: ${testDtoCard2.url}, username: ${testDtoCard2.username}, password: ********, other: ${testDtoCard2.other}}');
    });

    test('Json round trip works', () {
      final json = testDtoCard2.toJson();

      final card = DtoCard.fromJson(json);

      expect(card.url, testDtoCard2.url);
      expect(card.username, testDtoCard2.username);
      expect(card.password, testDtoCard2.password);
      expect(card.other, testDtoCard2.other);
    });
  });
}
