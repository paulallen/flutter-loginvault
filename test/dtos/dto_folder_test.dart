import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/dtos/DtoFolder.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data.dart';

void main() {
  logger.minLevel = Level.warning;

  group('Folder', () {
    test('toString() works', () {
      final string = testDtoFolder2.toString();

      expect(string,
          '{DtoFolder name: ${testDtoFolder2.name}, cards: ${testDtoFolder2.cards.length}}');
    });

    test('Json round trip works', () {
      final json = testDtoFolder2.toJson();

      final folder = DtoFolder.fromJson(json);

      expect(folder.name, testDtoFolder2.name);
    });
  });
}
