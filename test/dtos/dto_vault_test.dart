import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/dtos/DtoVault.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data.dart';

void main() {
  logger.minLevel = Level.warning;

  group('Vault', () {
    test('toString() works', () {
      final string = testDtoVault.toString();

      expect(string,
          '{DtoVault name: ${testDtoVault.name}, folders: ${testDtoVault.folders.length}}');
    });

    test('Json round trip works', () {
      final json = testDtoVault.toJson();

      final vault = DtoVault.fromJson(json);

      expect(vault.name, testDtoVault.name);
      expect(vault.folders.length, testDtoVault.folders.length);

      for (var f = 0; f < vault.folders.length; ++f) {
        expect(vault.folders[f].cards.length,
            testDtoVault.folders[f].cards.length);

        for (var c = 0; c < vault.folders[f].cards.length; ++c) {
          expect(vault.folders[f].cards[c].url,
              testDtoVault.folders[f].cards[c].url);
          expect(vault.folders[f].cards[c].username,
              testDtoVault.folders[f].cards[c].username);
          expect(vault.folders[f].cards[c].password,
              testDtoVault.folders[f].cards[c].password);
          expect(vault.folders[f].cards[c].other,
              testDtoVault.folders[f].cards[c].other);
        }
      }
    });
  });
}
