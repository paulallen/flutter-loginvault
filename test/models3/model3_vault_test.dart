import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data3.dart';

String changed(String original) => 'xx-$original-xx';

void main() {
  logger.minLevel = Level.warning;

  group('Vault3', () {
    test('clone() works', () {
      final copy = testModel3Vault.clone();

      expect(copy.hashCode == testModel3Vault.hashCode, isFalse);
      expect(copy.cards.length, testModel3Vault.cards.length);

      for (var n = 0; n < copy.cards.length; ++n) {
        expect(copy.cards[n] == testModel3Vault.cards[n], true);
      }
    });

    group('equality()', () {
      test('works', () {
        final copy = testModel3Vault.clone();

        final isSame = copy == testModel3Vault;

        expect(isSame, isTrue);
      });

      test('spots cards removed', () {
        final copy = testModel3Vault.clone();
        copy.cards.clear();

        final isSame = copy == testModel3Vault;

        expect(isSame, isFalse);
      });

      test('spots card different', () {
        final copy = testModel3Vault.clone();
        copy.cards.first.label = 'this was not what I expected';

        final isSame = copy == testModel3Vault;

        expect(isSame, isFalse);
      });
    });

    test('toString() works', () {
      final string = testModel3Vault.toString();

      expect(
        string,
        '{Vault3 cards.length: ${testModel3Vault.cards.length}}',
      );
    });
  });
}
