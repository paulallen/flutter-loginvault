import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data3.dart';

String changed(String original) => 'xx-$original-xx';

void main() {
  logger.minLevel = Level.warning;

  group('XCard3', () {
    test('clone() works', () {
      for (final card in [
        testModel3Card1,
        testModel3Card2,
        testModel3Card3,
        testModel3Card4
      ]) {
        final copy = card.clone();

        expect(copy.hashCode == card.hashCode, isFalse);
        expect(copy.url, card.url);
        expect(copy.username, card.username);
        expect(copy.password, card.password);
        expect(copy.label, card.label);
        expect(copy.other, card.other);
        expect(copy.isDeleted, card.isDeleted);
      }
    });

    group('equality()', () {
      test('works', () {
        final copy = testModel3Card1.clone();

        final isSame = copy == testModel3Card1;

        expect(isSame, isTrue);
      });

      test('spots url', () {
        final copy = testModel3Card1.clone()
          ..url = changed(testModel3Card1.url);

        final isSame = copy == testModel3Card1;

        expect(isSame, isFalse);
      });

      test('spots username', () {
        final copy = testModel3Card1.clone()
          ..username = changed(testModel3Card1.username);

        final isSame = copy == testModel3Card1;

        expect(isSame, isFalse);
      });

      test('spots password', () {
        final copy = testModel3Card1.clone()
          ..password = changed(testModel3Card1.password);

        final isSame = copy == testModel3Card1;

        expect(isSame, isFalse);
      });

      test('spots other', () {
        final copy = testModel3Card1.clone()
          ..other = changed(testModel3Card1.other);

        final isSame = copy == testModel3Card1;

        expect(isSame, isFalse);
      });

      test('spots label', () {
        final copy = testModel3Card1.clone()
          ..label = changed(testModel3Card1.label);

        final isSame = copy == testModel3Card1;

        expect(isSame, isFalse);
      });

      test('spots icon', () {
        final copy = testModel3Card1.clone()
          ..icon = changed(testModel3Card1.icon);

        final isSame = copy == testModel3Card1;

        expect(isSame, isFalse);
      });

      test('spots isDeleted', () {
        final copy = testModel3Card1.clone()
          ..isDeleted = !testModel3Card1.isDeleted;

        final isSame = copy == testModel3Card1;

        expect(isSame, isFalse);
      });
    });

    test('toString() works', () {
      final string = testModel3Card1.toString();

      expect(string,
          '{XCard3 url: ${testModel3Card1.url}, username: ${testModel3Card1.username}, password: ********, label: ${testModel3Card1.label}, icon: ${testModel3Card1.icon}, other: ${testModel3Card1.other}, isDeleted: ${testModel3Card1.isDeleted}}');
    });
  });
}
