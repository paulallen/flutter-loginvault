import 'package:loginvault/dtos/DtoCard.dart';
import 'package:loginvault/dtos/DtoFolder.dart';
import 'package:loginvault/dtos/DtoVault.dart';
import 'package:loginvault/models/Folder.dart';
import 'package:loginvault/models/Vault.dart';
import 'package:loginvault/models/XCard.dart';

final testDtoCard1 = DtoCard()
  ..url = 'url1'
  ..username = 'username'
  ..password = 'password'
  ..other = 'other';

final testDtoCard2 = DtoCard()
  ..url = 'url2'
  ..username = 'username'
  ..password = 'password'
  ..other = 'other';

final testDtoCard3 = DtoCard()
  ..url = 'url3'
  ..username = 'username'
  ..password = 'password'
  ..other = 'other';

final testDtoCard4 = DtoCard()
  ..url = 'url4'
  ..username = 'username'
  ..password = 'password'
  ..other = 'other';

final testDtoFolder1 = DtoFolder('bert', [testDtoCard1, testDtoCard2]);
final testDtoFolder2 = DtoFolder('ethel', [testDtoCard3, testDtoCard4]);

final testDtoVault = DtoVault('ethel', [testDtoFolder1, testDtoFolder2]);

final testModelCard1 = XCard()
  ..url = testDtoCard1.url
  ..username = testDtoCard1.username
  ..password = testDtoCard1.password
  ..other = testDtoCard1.other;

final testModelCard2 = XCard()
  ..url = testDtoCard2.url
  ..username = testDtoCard2.username
  ..password = testDtoCard2.password
  ..other = testDtoCard2.other;

final testModelCard3 = XCard()
  ..url = testDtoCard3.url
  ..username = testDtoCard3.username
  ..password = testDtoCard3.password
  ..other = testDtoCard3.other;

final testModelCard4 = XCard()
  ..url = testDtoCard4.url
  ..username = testDtoCard4.username
  ..password = testDtoCard4.password
  ..other = testDtoCard4.other;

final testModelFolder1 =
    Folder(name: testDtoFolder1.name, cards: [testModelCard1, testModelCard2]);
final testModelFolder2 =
    Folder(name: testDtoFolder2.name, cards: [testModelCard3, testModelCard4]);
final deletedModelFolder1 =
    Folder(name: 'Deleted Cards', cards: [testModelCard1, testModelCard2]);

final testModelVault = Vault(
    name: testDtoVault.name, folders: [testModelFolder1, testModelFolder2]);
