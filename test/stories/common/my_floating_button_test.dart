import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/stories/common/MyFloatingButton.dart';
import 'package:loginvault/stories/common/MyIcons.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  bool isPressed = false;
  void onPressedSpy() {
    isPressed = true;
  }

  group('MyFloatingButton', () {
    testWidgets('displays the icon', (WidgetTester tester) async {
      final widget = MyFloatingButton(
        icon: MyIcons.fingerprint,
        onPressed: onPressedSpy,
      );
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: widget)));

      final textFinder = find.byIcon(MyIcons.fingerprint);
      expect(textFinder, findsOneWidget);
    });

    testWidgets('onPressed is called', (WidgetTester tester) async {
      final widget = MyFloatingButton(
        icon: MyIcons.fingerprint,
        onPressed: onPressedSpy,
      );
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: widget)));

      widget.onPressed();
      expect(isPressed, isTrue);
    });
  });
}
