import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/stories/common/ActionButton.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const testIconData = Icons.ac_unit;

  bool isPressed = false;
  void onPressedSpy() {
    isPressed = true;
  }

  group('ActionButton', () {
    testWidgets('displays the icon', (WidgetTester tester) async {
      final widget = ActionButton(
        icon: const Icon(testIconData),
        onPressed: onPressedSpy,
      );
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: widget)));

      final iconFinder = find.byIcon(testIconData);
      expect(iconFinder, findsOneWidget);
    });

    testWidgets('onPressed() is called', (WidgetTester tester) async {
      final widget = ActionButton(
        icon: const Icon(Icons.access_alarm),
        onPressed: onPressedSpy,
      );
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: widget)));

      widget.onPressed!();
      expect(isPressed, isTrue);
    });
  });
}
