import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/stories/common/MyTextFormField.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  final formKey = GlobalKey<FormState>();
  const testText = 'bert';
  const testHintText = 'hint bert';

  bool wasValidated = false;
  String? validatorSpy(String? value) {
    wasValidated = true;
    return null;
  }

  void onChangedSpy(String value) {}

  group('MyTextFormField', () {
    testWidgets('displays initial text', (WidgetTester tester) async {
      final controller = TextEditingController(text: testText);
      final widget = MyTextFormField(
        controller: controller,
        hintText: testHintText,
      );
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: Form(key: formKey, child: widget),
        ),
      ));

      final textFinder = find.text(testText);
      expect(textFinder, findsOneWidget);
    });

    testWidgets('displays hint if no text', (WidgetTester tester) async {
      final controller = TextEditingController();
      final widget = MyTextFormField(
        controller: controller,
        hintText: testHintText,
      );
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: Form(key: formKey, child: widget),
        ),
      ));

      final hintFinder = find.text(testHintText);
      expect(hintFinder, findsOneWidget);
    });

    testWidgets('calls validator()', (WidgetTester tester) async {
      final controller = TextEditingController();
      final widget = MyTextFormField(
        controller: controller,
        onChanged: onChangedSpy,
        validator: validatorSpy,
      );
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: Form(key: formKey, child: widget),
        ),
      ));

      formKey.currentState!.validate();

      expect(wasValidated, isTrue);
    });
  });
}
