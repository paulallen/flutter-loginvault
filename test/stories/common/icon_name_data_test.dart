import 'package:loginvault/stories/common/IconNameData.dart';
import 'package:loginvault/utils/Logger.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  logger.minLevel = Level.warning;

  group('getIconNameData()', () {
    test('finds existing icon', () {
      final iconNameData = IconNameData.getIconNameData('restaurant');

      expect(iconNameData, isNotNull);
    });

    test('returns null if not found', () {
      final iconNameData = IconNameData.getIconNameData('no-such-bert');

      expect(iconNameData, isNull);
    });
  });
}
