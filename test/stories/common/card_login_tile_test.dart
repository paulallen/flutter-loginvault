import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/models3/XCard3.dart';
import 'package:loginvault/stories/common/CardLoginTile.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const testUrl = 'http://url';
  const testUsername = 'bert';

  final testCard = XCard3()
    ..url = testUrl
    ..username = testUsername
    ..icon = 'savings';

  bool wasTapped = false;
  void onTapSpy() {
    wasTapped = true;
  }

  group('CardLoginTile', () {
    testWidgets('displays icon, url, username', (WidgetTester tester) async {
      final widget = CardLoginTile(testCard, onTap: onTapSpy);
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: widget)));

      final urlFinder = find.text(testCard.url);
      expect(urlFinder, findsOneWidget);

      final usernameFinder = find.text(testCard.username);
      expect(usernameFinder, findsOneWidget);

      final iconFinder = find.byIcon(Icons.savings_outlined);
      expect(iconFinder, findsOneWidget);
    });

    testWidgets('onTap() gets tapped', (WidgetTester tester) async {
      final widget = CardLoginTile(testCard, onTap: onTapSpy);
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: widget)));

      final listTileFinder = find.byType(ListTile);
      expect(listTileFinder, findsOneWidget);

      widget.onTap();
      expect(wasTapped, isTrue);
    });
  });
}
