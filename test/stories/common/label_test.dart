import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/stories/common/Label.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const testLabelText = 'Bert';

  group('Label', () {
    testWidgets('displays the text', (WidgetTester tester) async {
      const widget = Label(testLabelText);
      await tester.pumpWidget(const MaterialApp(home: Scaffold(body: widget)));

      final textFinder = find.text(testLabelText);
      expect(textFinder, findsOneWidget);
    });
  });
}
