import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/stories/common/MyTextField.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const testText = 'bert';
  const testHintText = 'hint bert';

  group('MyTextField', () {
    testWidgets('displays initial text', (WidgetTester tester) async {
      final controller = TextEditingController(text: testText);
      final widget = MyTextField(
        controller: controller,
        hintText: testHintText,
      );
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: widget)));

      final textFinder = find.text(testText);
      expect(textFinder, findsOneWidget);
    });

    testWidgets('displays hint if no text', (WidgetTester tester) async {
      final controller = TextEditingController();
      final widget = MyTextField(
        controller: controller,
        hintText: testHintText,
      );
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: widget)));

      final hintFinder = find.text(testHintText);
      expect(hintFinder, findsOneWidget);
    });
  });
}
