import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/stories/common/EmptyListPane.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const testIconData = Icons.credit_card_outlined;
  const testIcon = Icon(testIconData);
  const testTitle = 'testTitle';
  const testText = 'testText';

  group('EmptyListPane', () {
    testWidgets('displays icon, title, and text', (WidgetTester tester) async {
      final widget = EmptyListPane(
        title: testTitle,
        text: testText,
        child: testIcon,
      );
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: widget)));

      final iconFinder = find.byIcon(testIconData);
      expect(iconFinder, findsOneWidget);

      final titleFinder = find.text(testTitle);
      expect(titleFinder, findsOneWidget);

      final textFinder = find.text(testTitle);
      expect(textFinder, findsOneWidget);
    });

    testWidgets('displays just icon and title', (WidgetTester tester) async {
      final widget = EmptyListPane(
        title: testTitle,
        child: testIcon,
      );
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: widget)));

      final iconFinder = find.byIcon(testIconData);
      expect(iconFinder, findsOneWidget);

      final titleFinder = find.text(testTitle);
      expect(titleFinder, findsOneWidget);
    });
  });
}
