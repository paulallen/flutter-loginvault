import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/stories/common/Validators.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  group('CardBloc', () {
    group('urlValidator()', () {
      test('null not allowed', () {
        final message = Validators.urlValidator(null);
        expect(message, isNotNull);
      });
      test('blank not allowed', () {
        final message = Validators.urlValidator('');
        expect(message, isNotNull);
      });
      test('anything is valid', () {
        final message = Validators.urlValidator('bert');
        expect(message, isNull);
      });
    });

    group('repeatValidator()', () {
      test('differences are not allowed', () {
        final message = Validators.repeatValidator('bert', 'BERT');
        expect(message, isNotNull);
      });
      test('both the same is valid', () {
        final message = Validators.repeatValidator('bert', 'bert');
        expect(message, isNull);
      });
    });
  });
}
