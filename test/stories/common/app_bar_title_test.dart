import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/stories/common/AppBarTitle.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const testTitle = 'Bert';

  group('AppBarTitle', () {
    testWidgets('displays the text', (WidgetTester tester) async {
      const widget = AppBarTitle(testTitle);
      await tester.pumpWidget(const MaterialApp(home: Scaffold(body: widget)));

      final textFinder = find.text(testTitle);
      expect(textFinder, findsOneWidget);
    });
  });
}
