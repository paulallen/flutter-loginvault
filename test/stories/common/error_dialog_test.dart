import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/stories/common/ErrorDialog.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const testErrorText = 'Bert';

  bool isPressed = false;
  void onPressedSpy() {
    isPressed = true;
  }

  group('ErrorDialog', () {
    testWidgets('displays the text', (WidgetTester tester) async {
      final widget = ErrorDialog(
        testErrorText,
        onPressed: onPressedSpy,
      );
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: widget)));

      final textFinder = find.text(testErrorText);
      expect(textFinder, findsOneWidget);
    });

    testWidgets('button works', (WidgetTester tester) async {
      final widget = ErrorDialog(
        testErrorText,
        onPressed: onPressedSpy,
      );
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: widget)));

      final buttonFinder = find.byType(TextButton);
      expect(buttonFinder, findsOneWidget);

      widget.onPressed();
      expect(isPressed, isTrue);
    });
  });
}
