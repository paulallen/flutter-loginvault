import 'package:flutter_test/flutter_test.dart';

import 'package:loginvault/services/Session3.dart';
import 'package:loginvault/stories/search/SearchBloc.dart';
import 'package:loginvault/utils/Logger.dart';

import '../../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  final testVault = testModel3Vault.clone();
  final activeCardsList = testVault.cards.where((c) => !c.isDeleted).toList();
  final firstCard = testVault.cards[0];

  group('SearchBloc', () {
    group('constructor', () {
      test('emits not-deleted cards', () async {
        // Assemble
        Session3.instance.setVault(testVault);

        // Act
        final bloc = SearchBloc();

        // Assert
        // Why the [] first?
        expect(
            bloc.cards,
            emitsInOrder([
              [],
              activeCardsList,
            ]));
      });
    });

    group('setFilterText()', () {
      test('emits cards matching url', () async {
        // Assemble
        Session3.instance.setVault(testVault);
        final bloc = SearchBloc();

        // Act
        bloc.setSearchText(firstCard.url);

        // Assert
        // Why the [] first?
        expect(
            bloc.cards,
            emitsInOrder([
              [],
              [firstCard]
            ]));
      });
    });

    group('dispose()', () {
      test('does not throw', () async {
        // Assemble
        Session3.instance.setVault(testVault);
        final bloc = SearchBloc();

        // Act
        await Future.delayed(const Duration(milliseconds: 500));
        bloc.dispose();

        // Assert
      });
    });
  });
}
