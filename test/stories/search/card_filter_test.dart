import 'package:flutter_test/flutter_test.dart';

import 'package:loginvault/models3/XCard3.dart';
import 'package:loginvault/stories/search/CardFilter.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const testFilter = 'bert';
  const testMatchingValue = "x${testFilter}x";

  group('CardFilter', () {
    final cardFilter = CardFilter();

    test('does not match a blank card', () {
      final card = XCard3();

      final matches = cardFilter.matches(card, testFilter);

      expect(matches, isFalse);
    });

    group('isDeleted', () {
      test('matches true', () {
        final card = XCard3()..url = testMatchingValue;
        card.isDeleted = true;

        final matches = cardFilter.matches(card, testFilter);

        expect(matches, isTrue);
      });

      test('matches false', () {
        final card = XCard3()..url = testMatchingValue;
        card.isDeleted = false;

        final matches = cardFilter.matches(card, testFilter);

        expect(matches, isTrue);
      });
    });

    test('matches url', () {
      final card = XCard3()..url = testMatchingValue;

      final matches = cardFilter.matches(card, testFilter);

      expect(matches, isTrue);
    });
    test('matches username', () {
      final card = XCard3()..username = testMatchingValue;

      final matches = cardFilter.matches(card, testFilter);

      expect(matches, isTrue);
    });
    test('matches password', () {
      final card = XCard3()..password = testMatchingValue;

      final matches = cardFilter.matches(card, testFilter);

      expect(matches, isTrue);
    });
    test('matches label', () {
      final card = XCard3()..label = testMatchingValue;

      final matches = cardFilter.matches(card, testFilter);

      expect(matches, isTrue);
    });
    test('matches other', () {
      final card = XCard3()..other = testMatchingValue;

      final matches = cardFilter.matches(card, testFilter);

      expect(matches, isTrue);
    });
  });
}
