import 'package:flutter_test/flutter_test.dart';

import 'package:loginvault/services/Session3.dart';
import 'package:loginvault/services/Sorting.dart';
import 'package:loginvault/stories/picklabel/PickLabelBloc.dart';
import 'package:loginvault/utils/Logger.dart';

import '../../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  final testVault = testModel3Vault.clone();

  group('PickLabelBloc', () {
    group('constructor', () {
      test('emits non deleted labels, sorted', () async {
        // Assemble
        Session3.instance.setVault(testVault);

        final expectedLabels =
            testVault.cards.map((c) => c.label).toSet().toList();
        expectedLabels.add('');
        expectedLabels.sort(Sorting.sortLabels);

        // Act
        final bloc = PickLabelBloc();

        // Assert
        expect(bloc.labels, emits(expectedLabels));
      });
    });

    group('dispose()', () {
      test('does not throw', () async {
        // Assemble
        Session3.instance.setVault(testVault);
        final bloc = PickLabelBloc();

        // Act
        await Future.delayed(const Duration(milliseconds: 500));
        bloc.dispose();

        // Assert
      });
    });
  });
}
