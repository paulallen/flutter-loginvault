import 'package:flutter_test/flutter_test.dart';

import 'package:loginvault/services/Session3.dart';
import 'package:loginvault/services/Sorting.dart';
import 'package:loginvault/stories/pickusername/PickUsernameBloc.dart';
import 'package:loginvault/utils/Logger.dart';

import '../../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  final testVault = testModel3Vault.clone();

  group('PickUsernameBloc', () {
    group('constructor', () {
      test('emits non deleted labels, sorted', () async {
        // Assemble
        Session3.instance.setVault(testVault);

        final expectedUsernames =
            testVault.cards.map((c) => c.username).toSet().toList();
        expectedUsernames.add('');
        expectedUsernames.sort(Sorting.sortAlphabetic);

        // Act
        final bloc = PickUsernameBloc();

        // Assert
        expect(bloc.usernames, emits(expectedUsernames));
      });
    });

    group('dispose()', () {
      test('does not throw', () async {
        // Assemble
        Session3.instance.setVault(testVault);
        final bloc = PickUsernameBloc();

        // Act
        await Future.delayed(const Duration(milliseconds: 500));
        bloc.dispose();

        // Assert
      });
    });
  });
}
