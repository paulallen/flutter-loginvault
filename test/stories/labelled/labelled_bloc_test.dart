import 'package:flutter_test/flutter_test.dart';

import 'package:loginvault/services/Session3.dart';
import 'package:loginvault/stories/labelled/LabelledBloc.dart';
import 'package:loginvault/utils/Logger.dart';

import '../../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  final testVault = testModel3Vault.clone();
  final testLabel = testVault.cards[0].label;
  final labelledCardsList = [testVault.cards[0]];

  group('LabelledBloc', () {
    group('constructor', () {
      test('emits matching cards', () async {
        // Assemble
        Session3.instance.setVault(testVault);
        Session3.instance.theSelectedLabel = testLabel;

        // Act
        final bloc = LabelledBloc();

        // Assert
        expect(bloc.cards, emits(labelledCardsList));
      });
    });

    group('dispose()', () {
      test('does not throw', () async {
        // Assemble
        Session3.instance.setVault(testVault);
        Session3.instance.theSelectedLabel = testLabel;
        final bloc = LabelledBloc();

        // Act
        await Future.delayed(const Duration(milliseconds: 500));
        bloc.dispose();

        // Assert
      });
    });
  });
}
