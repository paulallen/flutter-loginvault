import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/FileIo.dart';
import 'package:loginvault/services/Session3.dart';
import 'package:loginvault/stories/home/HomeBloc.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  final fileIo = FileIo();

  logger.minLevel = Level.warning;

  const String testVaultFolder = 'build';
  const String testVaultName = 'home-widget-test-vault';
  const String testVaultPath = '$testVaultFolder/$testVaultName.lv1';

  WidgetsFlutterBinding.ensureInitialized();

  final bloc = HomeBloc();

  group('HomeBloc', () {
    test('createit() works', () async {
      Session3.instance.thePath = testVaultFolder;

      await bloc.createit(testVaultName, testVaultName);
      expect(bloc.vault, isNotNull);

      final exists = await bloc.exists(testVaultPath);
      expect(exists, isTrue);

      // Cleanup
      await fileIo.deleteFile(testVaultPath);
    });
  });
}
