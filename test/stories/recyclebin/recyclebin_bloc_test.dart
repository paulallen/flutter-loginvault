import 'package:flutter_test/flutter_test.dart';

import 'package:loginvault/services/Session3.dart';
import 'package:loginvault/stories/recyclebin/RecycleBinBloc.dart';
import 'package:loginvault/utils/Logger.dart';

import '../../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  final testVault = testModel3Vault.clone();
  final deletedCardsList = testVault.cards.where((c) => c.isDeleted).toList();

  group('RecycleBinBloc', () {
    group('constructor', () {
      test('emits deleted cards', () async {
        // Assemble
        Session3.instance.setVault(testVault);

        // Act
        final bloc = RecycleBinBloc();

        // Assert
        // Why the [] first?
        expect(bloc.cards, emits(deletedCardsList));
      });
    });

    group('dispose()', () {
      test('does not throw', () async {
        // Assemble
        Session3.instance.setVault(testVault);
        final bloc = RecycleBinBloc();

        // Act
        await Future.delayed(const Duration(milliseconds: 500));
        bloc.dispose();

        // Assert
      });
    });
  });
}
