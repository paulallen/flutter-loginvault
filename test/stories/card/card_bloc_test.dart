import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/Session3.dart';
import 'package:loginvault/stories/card/CardBloc.dart';
import 'package:loginvault/utils/Logger.dart';

import '../../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  WidgetsFlutterBinding.ensureInitialized();

  final testVault = testModel3Vault.clone();
  const testNewUrl = ' google ';
  const testNewUsername = ' dave ';
  const testNewLabel = ' stuff ';

  final bloc = CardBloc();

  group('CardBloc', () {
    group('updateCard()', () {
      test('does url validation', () {
        final isUpdated = bloc.updateCard(
          '',
          '',
          'password',
          'repeat',
          '',
          '',
          '',
          isDeleted: false,
          isRevealed: false,
        );

        expect(isUpdated, isFalse);
      });

      test('does repeat password validation', () {
        final isUpdated = bloc.updateCard(
          'url',
          '',
          'password',
          'repeat',
          '',
          '',
          '',
          isDeleted: false,
          isRevealed: false,
        );

        expect(isUpdated, isFalse);
      });

      test('updates the card if valid', () {
        final testCard = testVault.cards[0];
        Session3.instance.setVault(testVault);
        Session3.instance.theCard = testCard;

        final isUpdated = bloc.updateCard(
          testNewUrl,
          testCard.username,
          testCard.password,
          testCard.password,
          testCard.label,
          testCard.icon,
          testCard.other,
          isDeleted: false,
          isRevealed: false,
        );

        expect(isUpdated, isTrue);
      });

      test('trims url, username, and label', () {
        final testCard = testVault.cards[0];
        Session3.instance.setVault(testVault);
        Session3.instance.theCard = testCard;

        final isUpdated = bloc.updateCard(
          testNewUrl,
          testNewUsername,
          testCard.password,
          testCard.password,
          testNewLabel,
          testCard.icon,
          testCard.other,
          isDeleted: false,
          isRevealed: false,
        );

        expect(isUpdated, isTrue);
        expect(testVault.cards[0].url, testNewUrl.trim());
        expect(testVault.cards[0].username, testNewUsername.trim());
        expect(testVault.cards[0].label, testNewLabel.trim());
      });
    });
  });

  test('deleteCard() works', () {
    final testCard = testVault.cards[0];
    Session3.instance.setVault(testVault);
    Session3.instance.theCard = testCard;

    bloc.deleteCard();

    expect(testCard.isDeleted, true);
  });

  test('undeleteCard() works', () {
    final testCard = testVault.cards[0];
    Session3.instance.setVault(testVault);
    Session3.instance.theCard = testCard;

    bloc.undeleteCard();

    expect(testCard.isDeleted, false);
  });
}
