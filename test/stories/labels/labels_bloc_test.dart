import 'package:flutter_test/flutter_test.dart';

import 'package:loginvault/services/Session3.dart';
import 'package:loginvault/stories/labels/LabelsBloc.dart';
import 'package:loginvault/utils/Logger.dart';

import '../../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  final testVault = testModel3Vault.clone();
  final testLabels = [testModel3Card1.label, testModel3Card3.label];

  group('LabelsBloc', () {
    group('constructor', () {
      test('emits initial values', () async {
        // Assemble
        Session3.instance.setVault(testVault);

        // Act
        final bloc = LabelsBloc();

        // Assert
        expect(bloc.labels, emits(testLabels));
      });
    });

    group('dispose()', () {
      test('does not throw', () async {
        // Assemble
        Session3.instance.setVault(testVault);
        final bloc = LabelsBloc();

        // Act
        await Future.delayed(const Duration(milliseconds: 500));
        bloc.dispose();

        // Assert
      });
    });
  });
}
