import 'package:mockito/annotations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/utils/Logger.dart';
import 'package:mockito/mockito.dart';
import 'logger_test.mocks.dart';

class Implementation {
  void print(Object o) {}
}

@GenerateMocks([Implementation])
void main() {
  test('global logger works', () {
    final mockImplentation = MockImplementation();
    logger.implementation = mockImplentation.print;

    logger.minLevel = Level.debug;

    logger.log(Level.error, 'Error');
    logger.log(Level.warning, 'Warning');
    logger.log(Level.info, 'Info');
    logger.log(Level.debug, 'Debug');

    verify(mockImplentation.print(any)).called(4);
  });

  test('logException works', () {
    final mockImplentation = MockImplementation();
    final logger = Logger();
    logger.implementation = mockImplentation.print;

    logger.minLevel = Level.debug;

    try {
      final list = <String>[];
      var _ = list.first;
    } catch (e, s) {
      logger.logException(Level.error, 'Bert', e, s);
    }

    verify(mockImplentation.print(any)).called(1);
  });

  group('levelName', () {
    final logger = Logger();

    test('none', () {
      final name = logger.levelName(Level.none);
      expect(name, '-');
    });

    test('error', () {
      final name = logger.levelName(Level.error);
      expect(name, 'E');
    });

    test('warning', () {
      final name = logger.levelName(Level.warning);
      expect(name, 'W');
    });

    test('info', () {
      final name = logger.levelName(Level.info);
      expect(name, 'I');
    });

    test('debug', () {
      final name = logger.levelName(Level.debug);
      expect(name, 'D');
    });
  });

  group('Log level', () {
    test('none', () {
      final mockImplentation = MockImplementation();
      final logger = Logger();
      logger.implementation = mockImplentation.print;

      logger.minLevel = Level.none;

      logger.log(Level.error, 'Error');
      logger.log(Level.warning, 'Warning');
      logger.log(Level.info, 'Info');
      logger.log(Level.debug, 'Debug');

      verifyNever(mockImplentation.print(any));
    });

    test('error', () {
      final mockImplentation = MockImplementation();
      final logger = Logger();
      logger.implementation = mockImplentation.print;

      logger.minLevel = Level.error;

      logger.log(Level.error, 'Error');
      logger.log(Level.warning, 'Warning');
      logger.log(Level.info, 'Info');
      logger.log(Level.debug, 'Debug');

      verify(mockImplentation.print(any)).called(1);
    });

    test('warning', () {
      final mockImplentation = MockImplementation();
      final logger = Logger();
      logger.implementation = mockImplentation.print;

      logger.minLevel = Level.warning;

      logger.log(Level.error, 'Error');
      logger.log(Level.warning, 'Warning');
      logger.log(Level.info, 'Info');
      logger.log(Level.debug, 'Debug');

      verify(mockImplentation.print(any)).called(2);
    });

    test('info', () {
      final mockImplentation = MockImplementation();
      final logger = Logger();
      logger.implementation = mockImplentation.print;

      logger.minLevel = Level.info;

      logger.log(Level.error, 'Error');
      logger.log(Level.warning, 'Warning');
      logger.log(Level.info, 'Info');
      logger.log(Level.debug, 'Debug');

      verify(mockImplentation.print(any)).called(3);
    });

    test('debug', () {
      final mockImplentation = MockImplementation();
      final logger = Logger();
      logger.implementation = mockImplentation.print;

      logger.minLevel = Level.debug;

      logger.log(Level.error, 'Error');
      logger.log(Level.warning, 'Warning');
      logger.log(Level.info, 'Info');
      logger.log(Level.debug, 'Debug');

      verify(mockImplentation.print(any)).called(4);
    });
  });
}
