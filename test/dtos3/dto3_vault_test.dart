import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/dtos3/DtoVault3.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  group('Vault3', () {
    test('toString() works', () {
      final string = testDto3Vault.toString();

      expect(string, '{DtoVault3 cards: ${testDto3Vault.cards.length}}');
    });

    test('Json round trip works', () {
      final json = testDto3Vault.toJson();

      final vault = DtoVault3.fromJson(json);

      expect(vault.cards.length, testDto3Vault.cards.length);

      for (var c = 0; c < vault.cards.length; ++c) {
        expect(vault.cards[c].url, testDto3Vault.cards[c].url);
        expect(vault.cards[c].username, testDto3Vault.cards[c].username);
        expect(vault.cards[c].password, testDto3Vault.cards[c].password);
        expect(vault.cards[c].other, testDto3Vault.cards[c].other);
        expect(vault.cards[c].label, testDto3Vault.cards[c].label);
        expect(vault.cards[c].icon, testDto3Vault.cards[c].icon);
        expect(vault.cards[c].isDeleted, testDto3Vault.cards[c].isDeleted);
      }
    });
  });
}
