import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/dtos3/DtoCard3.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  group('Card3', () {
    test('toString() works', () {
      final string = testDto3Card1.toString();

      expect(
          string,
          '{DtoCard3 url: ${testDto3Card1.url}, '
          'username: ${testDto3Card1.username}, '
          'password: ********, '
          'other: ${testDto3Card1.other}, '
          'label: ${testDto3Card1.label}, '
          'icon: ${testDto3Card1.icon}, '
          'isDeleted: ${testDto3Card1.isDeleted}}');
    });

    test('Json round trip works', () {
      final json = testDto3Card1.toJson();

      final card = DtoCard3.fromJson(json);

      expect(card.url, testDto3Card1.url);
      expect(card.username, testDto3Card1.username);
      expect(card.password, testDto3Card1.password);
      expect(card.other, testDto3Card1.other);
    });
  });
}
