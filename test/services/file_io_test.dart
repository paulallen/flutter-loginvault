import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/FileIo.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const testFolderPath = 'build';
  const testFilePath = 'build/file_io_test_datafile.txt';
  const testFileContent = 'what is that lorem ipsum text again?';

  final fileIo = FileIo();

  group('FileIo', () {
    test('listFileSystemEntities() works', () async {
      final list = await fileIo.listFileSystemEntities(testFolderPath);

      expect(list, isNotNull);
      expect(list.length, isNonZero);
    });

    test('writeFile(), readFile(), deleteFile(), exists() work', () async {
      await fileIo.writeFile(testFilePath, testFileContent);

      final isThere = await fileIo.exists(testFilePath);
      expect(isThere, true);

      final readContent = await fileIo.readFile(testFilePath);
      expect(readContent, testFileContent);

      await fileIo.deleteFile(testFilePath);
      final isStillThere = await fileIo.exists(testFilePath);
      expect(isStillThere, false);
    });
  });
}
