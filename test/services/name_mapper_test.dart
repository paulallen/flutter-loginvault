import 'package:flutter_test/flutter_test.dart';
import 'package:path/path.dart' as path;

import 'package:loginvault/services/NameMapper.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const testFolderName = 'dave';
  const testVaultName = 'bert';
  const testFileName = '$testVaultName.lv1';

  final namemapper = NameMapper();

  group('NameMapper', () {
    group('toFileName()', () {
      test('works', () {
        final fileName = namemapper.toFileName(testVaultName);

        expect(fileName, testFileName);
      });
    });

    group('toFilePath()', () {
      test('works', () {
        final filePath = namemapper.toFilePath(testFolderName, testVaultName);

        final expectedPath = path.join(testFolderName, testFileName);
        expect(filePath, expectedPath);
      });
    });

    group('toVaultName()', () {
      test('works', () {
        final vaultName = namemapper.toVaultName(testFileName);

        expect(vaultName, testVaultName);
      });
    });
  });
}
