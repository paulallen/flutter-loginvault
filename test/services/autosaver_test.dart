import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/AutoSaver.dart';
import 'package:loginvault/services/FileIo.dart';
import 'package:loginvault/services/Session3.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  final testVault = testModel3Vault.clone();

  const String testFilePath = 'build/autosaver-test-vault.lv1';
  const String testPassword = 'password';

  group('AutoSaver', () {
    test('run(), stop() does not throw', () {
      final autoSaver = AutoSaver(testVault, testFilePath, testPassword);

      autoSaver.run();
      autoSaver.stop();
    });

    group('AutoSaver', () {
      test('saves a changed vault', () async {
        final autoSaver = AutoSaver(testVault, testFilePath, testPassword);
        testVault.cards[0].url = 'changed';

        await autoSaver.saveIfChanged();

        final exists = await FileIo().exists(testFilePath);
        expect(exists, isTrue);
        expect(Session3.instance.messages, emits('Saved vault!'));
      });
    });
  });
}
