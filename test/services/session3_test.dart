import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/models3/Vault3.dart';
import 'package:loginvault/models3/XCard3.dart';
import 'package:loginvault/services/Session3.dart';
import 'package:loginvault/services/Sorting.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  const String testName = 'name';
  final testVault = testModel3Vault.clone();
  final testVault2 = Vault3(cards: [
    testModel3Card1,
    testModel3Card3,
  ]);

  group('Session3', () {
    group('setVault()', () {
      test('emits cards', () async {
        // Assemble
        final s = Session3();

        // Act
        s.setVault(testVault);

        // Assert
        expect(s.cardsStream, emits(testVault.cards));
      });
    });

    group('close()', () {
      test('nulls values', () async {
        // Assemble
        final s = Session3();
        s.openVault('filePath', 'password', 'vaultName', testVault);

        // Act
        await s.closeVault();

        // Assert
        expect(s.theVaultName, '');
        expect(s.theVault, null);
        expect(s.theAutoSaver, isNull);
      });
    });

    group('open()', () {
      test('sets values', () {
        // Assemble
        final s = Session3();

        // Act
        s.openVault('filePath', 'password', 'vaultName', testVault);

        // Assert
        expect(s.theVaultName, 'vaultName');
        expect(s.theVault, testVault);
        expect(s.theAutoSaver, isNotNull);
        expect(s.cardsStream, emits(testVault.cards));
      });

      test('emits cards', () {
        // Assemble
        final s = Session3();

        // Act
        s.openVault('filePath', 'password', 'vaultName', testVault);

        // Assert
        expect(s.cardsStream, emits(testVault.cards));
      });

      test('closes and reopens safely', () {
        // Assemble
        final s = Session3();
        s.openVault('filePath', 'password', 'vaultName', testVault);

        // Act
        s.openVault('filePath2', 'password2', 'vaultName2', testVault2);

        // Assert
        expect(s.theVaultName, 'vaultName2');
        expect(s.theVault, testVault2);
        expect(s.theAutoSaver, isNotNull);
        expect(s.cardsStream, emits(testVault2.cards));
      });
    });
  });

  group('Session3Api', () {
    bool areCardsSorted(Vault3 vault) {
      final sortedCards = vault.clone().cards;
      sortedCards.sort(Sorting.sortCards);

      for (var n = 0; n < vault.cards.length; ++n) {
        if (vault.cards[n].url != sortedCards[n].url) {
          return false;
        }
      }
      return true;
    }

    group('putCard()', () {
      test('adds a card', () {
        // Assemble
        final session = Session3();
        session.openVault('', '', testName, testVault);

        final newcard = XCard3()
          ..url = 'test'
          ..username = 'test user'
          ..label = 'label';

        // Act
        session.putCard(newcard, newcard);

        // Assert
        expect(testVault.cards.contains(newcard), isTrue);
        expect(areCardsSorted(testVault), isTrue);
      });

      test('updates a card', () {
        // Assemble
        final session = Session3();
        session.openVault('', '', testName, testVault);

        final card = testVault.cards[1];
        card.username = 'updated username';

        // Act
        session.putCard(card, card);

        // Assert
        expect(testVault.cards.contains(card), isTrue);
        expect(areCardsSorted(testVault), isTrue);
      });
    });

    group('deleteCard()', () {
      test('ignores a card that is not present', () {
        // Assemble
        final session = Session3();
        session.openVault('', '', testName, testVault);

        final newcard = XCard3()
          ..url = 'a test url'
          ..username = 'a test user'
          ..label = 'a test label'
          ..isDeleted = false;

        // Act
        session.deleteCard(newcard, delete: true);

        // Assert
        expect(testVault.cards.contains(newcard), isFalse);
        expect(newcard.isDeleted, isFalse);
        expect(areCardsSorted(testVault), isTrue);
      });

      test('undeletes', () {
        // Assemble
        final session = Session3();
        session.openVault('', '', testName, testVault);

        final card = testVault.cards[1];
        card.isDeleted = true;

        // Act
        session.deleteCard(card, delete: false);

        // Assert
        expect(card.isDeleted, isFalse);
        expect(areCardsSorted(testVault), isTrue);
      });

      test('deletes', () {
        // Assemble
        final session = Session3();
        session.openVault('', '', testName, testVault);

        final card = testVault.cards[1];
        card.isDeleted = false;

        // Act
        session.deleteCard(card, delete: true);

        // Assert
        expect(card.isDeleted, isTrue);
        expect(areCardsSorted(testVault), isTrue);
      });

      test('really deletes', () {
        // Assemble
        final session = Session3();
        session.openVault('', '', testName, testVault);

        final card = testVault.cards[1];
        card.isDeleted = true;

        // Act
        session.deleteCard(card, delete: true);

        // Assert
        expect(testVault.cards.contains(card), isFalse);
        expect(areCardsSorted(testVault), isTrue);
      });
    });
  });
}
