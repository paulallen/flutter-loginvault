import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/dtos/DtoCard.dart';
import 'package:loginvault/dtos/DtoFolder.dart';
import 'package:loginvault/dtos/DtoVault.dart';
import 'package:loginvault/services/Serializer1.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  final testCard = DtoCard()
    ..url = 'url'
    ..username = 'username'
    ..password = 'password'
    ..other = 'other';

  final testFolder = DtoFolder('bert', [testCard]);
  final testVault = DtoVault('ethel', [testFolder]);

  final serial = Serializer1();

  group('Serializer1', () {
    group('round trip', () {
      test('works', () {
        final string = serial.serialize(testVault);
        final vault = serial.deserialize(string);

        expect(vault.folders.length, testVault.folders.length);
        expect(vault.folders.first.cards.length,
            testVault.folders.first.cards.length);

        for (var nf = 0; nf < vault.folders.length; ++nf) {
          for (var nc = 0; nc < vault.folders[nf].cards.length; ++nc) {
            final c = vault.folders[nf].cards[nc];
            final tc = testVault.folders[nf].cards[nc];

            expect(c.url, tc.url);
            expect(c.username, tc.username);
            expect(c.password, tc.password);
            expect(c.other, tc.other);
          }
        }
      });
    });
  });
}
