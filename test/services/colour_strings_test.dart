import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/ColourStrings.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  final cs = ColourStrings();

  group('ColourStrings', () {
    group('fromHashArgb()', () {
      test('converts null to null', () {
        final hsvColour = cs.fromHashArgb(null);

        expect(hsvColour, isNull);
      });

      test('converts invalid string to null', () {
        final hsvColour = cs.fromHashArgb('12345678');

        expect(hsvColour, isNull);
      });

      test('converts invalid strings to null', () {
        final hsvColour = cs.fromHashArgb('#123456789');

        expect(hsvColour, isNull);
      });

      test('converts valid strings to something', () {
        final hsvColour = cs.fromHashArgb('#ffdaa520');

        expect(hsvColour, isNotNull);
      });
    });

    group('round trip', () {
      test('string to colour and back works', () {
        for (final testString in [
          '#00000000',
          '#00000080',
          '#00008000',
          '#00800000',
          '#80808080',
          '#ffffffff',
        ]) {
          final hsvColour = cs.fromHashArgb(testString);
          expect(hsvColour, isNotNull);

          final hargbString = cs.toHashArgb(hsvColour!);
          expect(hargbString, testString);
        }
      });
    });
  });
}
