import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/FileFormatterV3.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const shortPayload = 'BERTBERTBERT';
  const longPayload = 'Lorem ipsum dolor sit amet, '
      'consectetur adipiscing elit, sed do eiusmod tempor incididunt '
      'ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis '
      'nostrud exercitation ullamco laboris nisi ut aliquip ex ea '
      'commodo consequat. Duis aute irure dolor in reprehenderit in '
      'voluptate velit esse cillum dolore eu fugiat nulla pariatur. '
      'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui '
      'officia deserunt mollit anim id est laborum.';

  final formatter3 = FileFormatterV3();

  group('FileFormatterV3', () {
    group('format()', () {
      test('works with a short payload', () {
        final content = formatter3.format(shortPayload);

        expect(content.startsWith('====='), isTrue);
        expect(content.contains(shortPayload), isTrue);
        expect(content.endsWith('=====\n'), isTrue);
      });

      test('splits long lines', () {
        final content = formatter3.format(longPayload);

        final lines = content.split('\n');
        for (var n = 1; n < lines.length - 3; ++n) {
          expect(lines[n].length, 64);
        }
      });
    });

    group('removeCarriageReturn()', () {
      test('removes trailing carriage return', () {
        final lines = <String>[
          '=====\r',
          'BERTBERT\r',
          '=====\r',
        ];

        final normalized = formatter3.removeCarriageReturn(lines);

        expect(normalized.length, 3);
        expect(normalized[0], '=====');
        expect(normalized[1], 'BERTBERT');
        expect(normalized[2], '=====');
      });

      test('removes a trailing blank line', () {
        final lines = <String>[
          'BERTBERT\r',
          '\r',
        ];

        final normalized = formatter3.removeCarriageReturn(lines);

        expect(normalized.length, 1);
        expect(normalized[0], 'BERTBERT');
      });

      test('leaves other lines alone', () {
        final lines = <String>[
          '=====',
          'BERTBERT',
          '=====',
        ];

        final normalized = formatter3.removeCarriageReturn(lines);

        expect(normalized.length, 3);
        expect(normalized[0], '=====');
        expect(normalized[1], 'BERTBERT');
        expect(normalized[2], '=====');
      });
    });

    group('parse()', () {
      test('works roughly as expected', () {
        const content =
            "${FileFormatterV3.v3Header}\n$shortPayload\n${FileFormatterV3.v3Footer}\n";
        final payload = formatter3.parse(content);

        expect(payload, shortPayload);
      });

      test('handles DOS newlines', () {
        const content =
            "${FileFormatterV3.v3Header}\r\n$shortPayload\r\n${FileFormatterV3.v3Footer}\r\n";
        final payload = formatter3.parse(content);

        expect(payload, shortPayload);
      });
    });

    group('Round trip', () {
      test('works', () {
        final content = formatter3.format(longPayload);
        final extracted = formatter3.parse(content);

        expect(extracted, longPayload);
      });
    });
  });
}
