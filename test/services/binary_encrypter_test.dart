import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/BinaryEncrypter.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  final encrypter = BinaryEncrypter();

  group('BinaryEncrypter', () {
    test('round trip works', () {
      final testPayload = Uint8List.fromList([
        0,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19
      ]);
      final testPassword = Uint8List.fromList([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);

      final payload = encrypter.encryptBuffer(testPayload, testPassword);
      final decode = encrypter.decryptBuffer(payload, testPassword);

      expect(decode, testPayload);
    });
  });
}
