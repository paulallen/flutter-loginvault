import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/Model3Upgrader.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data.dart';

void main() {
  logger.minLevel = Level.warning;

  final upgrader = Model3Upgrader();

  group('Model3Upgrader', () {
    group('Card', () {
      test('toModel works', () {
        final model = upgrader.toModelCard(testDtoFolder1.name, testDtoCard1);

        expect(model.url, testModelCard1.url);
        expect(model.username, testModelCard1.username);
        expect(model.password, testModelCard1.password);
        expect(model.label, testDtoFolder1.name);
        expect(model.other, testModelCard1.other);
      });
    });

    group('Vault', () {
      test('toModel works', () {
        final model = upgrader.toModelVault(testDtoVault);

        var numberCards = 0;
        for (final folder in testDtoVault.folders) {
          numberCards += folder.cards.length;
        }

        expect(model.cards.length, numberCards);

        var n = 0;
        for (final dtoFolder in testDtoVault.folders) {
          for (final dtoCard in dtoFolder.cards) {
            expect(model.cards[n].url, dtoCard.url);
            expect(model.cards[n].username, dtoCard.username);
            expect(model.cards[n].password, dtoCard.password);
            expect(model.cards[n].label, dtoFolder.name);
            expect(model.cards[n].other, dtoCard.other);
            ++n;
          }
        }
      });
    });
  });
}
