import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/ModelMapper.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data.dart';

void main() {
  logger.minLevel = Level.warning;

  final modelmapper = ModelMapper();

  group('ModelMapper', () {
    group('Card', () {
      test('toDto works', () {
        final dto = modelmapper.toDtoCard(testModelCard1);

        expect(dto.url, testModelCard1.url);
        expect(dto.username, testModelCard1.username);
        expect(dto.password, testModelCard1.password);
        expect(dto.other, testModelCard1.other);
      });

      test('toModel works', () {
        final model = modelmapper.toModelCard(testDtoCard1);

        expect(model.url, testModelCard1.url);
        expect(model.username, testModelCard1.username);
        expect(model.password, testModelCard1.password);
        expect(model.other, testModelCard1.other);
      });
    });

    group('Folder', () {
      test('toDto works', () {
        final dto = modelmapper.toDtoFolder(testModelFolder1);

        expect(dto.name, testModelFolder1.name);
        expect(dto.cards.length, testModelFolder1.cards.length);
      });

      test('toModel works', () {
        final model = modelmapper.toModelFolder(testDtoFolder1);

        expect(model.name, testDtoFolder1.name);
        expect(model.cards.length, testDtoFolder1.cards.length);
      });
    });

    group('Vault', () {
      test('toDto works', () {
        final dto = modelmapper.toDtoVault(testModelVault);

        expect(dto.name, testModelVault.name);
        expect(dto.folders.length, testModelVault.folders.length);
        expect(dto.folders.first.name, testModelVault.folders.first.name);
      });

      test('toModel works', () {
        final model = modelmapper.toModelVault(testDtoVault);

        expect(model.name, testDtoVault.name);
        expect(model.folders.length, testDtoVault.folders.length);
        expect(model.folders.first.name, testDtoVault.folders.first.name);
      });
    });
  });
}
