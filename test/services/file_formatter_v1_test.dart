import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/FileFormatterV1.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const shortPayload = 'BERTBERTBERT';
  const longPayload = 'Lorem ipsum dolor sit amet, '
      'consectetur adipiscing elit, sed do eiusmod tempor incididunt '
      'ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis '
      'nostrud exercitation ullamco laboris nisi ut aliquip ex ea '
      'commodo consequat. Duis aute irure dolor in reprehenderit in '
      'voluptate velit esse cillum dolore eu fugiat nulla pariatur. '
      'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui '
      'officia deserunt mollit anim id est laborum.';

  final formatter = FileFormatterV1();

  group('FileFormatterV1', () {
    group('format()', () {
      test('works roughly as expected', () {
        final content = formatter.format(shortPayload);

        expect(content.startsWith('====='), isTrue);
        expect(content.contains(shortPayload), isTrue);
        expect(content.endsWith('=====\n'), isTrue);
      });

      test('splits long lines', () {
        final content = formatter.format(longPayload);

        final lines = content.split('\n');
        for (var n = 1; n < lines.length - 3; ++n) {
          expect(lines[n].length, 64);
        }
      });
    });

    group('parse()', () {
      test('works roughly as expected', () {
        const content =
            "${FileFormatterV1.v1Header}\n$shortPayload\n${FileFormatterV1.v1Footer}\n";
        final payload = formatter.parse(content);

        expect(payload, shortPayload);
      });

      test('handles DOS newlines', () {
        const content =
            "${FileFormatterV1.v1Header}\r\n$shortPayload\r\n${FileFormatterV1.v1Footer}\r\n";
        final payload = formatter.parse(content);

        expect(payload, shortPayload);
      });
    });

    group('Round trip', () {
      test('works', () {
        final content = formatter.format(longPayload);
        final extracted = formatter.parse(content);

        expect(extracted, longPayload);
      });
    });
  });
}
