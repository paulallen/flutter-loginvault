import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/Encrypter.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const testPlaintext = 'Lorem ipsum dolor sit amet, '
      'consectetur adipiscing elit, sed do eiusmod tempor incididunt '
      'ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis '
      'nostrud exercitation ullamco laboris nisi ut aliquip ex ea '
      'commodo consequat. Duis aute irure dolor in reprehenderit in '
      'voluptate velit esse cillum dolore eu fugiat nulla pariatur. '
      'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui '
      'officia deserunt mollit anim id est laborum.';
  const testPassword = 'BERTBERTBERT';

  final encrypter = Encrypter();

  group('Encrypter', () {
    test('round trip works', () {
      final ciphertext = encrypter.encrypt(testPlaintext, testPassword);
      final plaintext = encrypter.decrypt(ciphertext, testPassword);

      expect(plaintext, testPlaintext);
    });
  });
}
