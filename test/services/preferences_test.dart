import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:loginvault/services/Preferences.dart';
import 'package:loginvault/utils/Logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  logger.minLevel = Level.warning;

  const testKey = 'a-key';
  const testValue = 'a-value';

  final settings = Preferences();

  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.setMockInitialValues({});

  group('Settings()', () {
    test('reads, writes, and deletes', () async {
      final value1 = await settings.read(testKey);
      expect(value1, isNull);

      await settings.write(testKey, testValue);
      final value2 = await settings.read(testKey);
      expect(value2, testValue);

      await settings.delete(testKey);
      final value3 = await settings.read(testKey);
      expect(value3, isNull);
    });
  });
}
