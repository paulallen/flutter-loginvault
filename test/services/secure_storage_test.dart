import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:loginvault/services/SecureStorage.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const testKey = 'a-key';
  const testValue = 'a-value';

  final ss = SecureStorage();

  WidgetsFlutterBinding.ensureInitialized();

  group('SecureStorage()', () {
    test('canUseStorage() returns false', () async {
      final can = await ss.canUseStorage();

      expect(can, false);
    });

    test('reads, writes, and deletes safely returning nulls', () async {
      final value1 = await ss.read(testKey);
      expect(value1, isNull);

      await ss.write(testKey, testValue);
      final value2 = await ss.read(testKey);
      expect(value2, isNull);

      await ss.delete(testKey);
      final value3 = await ss.read(testKey);
      expect(value3, isNull);
    });
  });
}
