import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/Model3Mapper.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  final modelmapper = Model3Mapper();

  group('Model3Mapper', () {
    group('Card', () {
      test('toDto works', () {
        final dto = modelmapper.toDtoCard(testModel3Card1);

        expect(dto.url, testModel3Card1.url);
        expect(dto.username, testModel3Card1.username);
        expect(dto.password, testModel3Card1.password);
        expect(dto.label, testModel3Card1.label);
        expect(dto.icon, testModel3Card1.icon);
        expect(dto.other, testModel3Card1.other);
      });

      test('toModel works', () {
        final model = modelmapper.toModelCard(testDto3Card1);

        expect(model.url, testDto3Card1.url);
        expect(model.username, testDto3Card1.username);
        expect(model.password, testDto3Card1.password);
        expect(model.label, testDto3Card1.label);
        expect(model.icon, testDto3Card1.icon);
        expect(model.other, testDto3Card1.other);
      });
    });

    group('Vault', () {
      test('toDto works', () {
        final dto = modelmapper.toDtoVault(testModel3Vault);

        expect(dto.cards.length, testModel3Vault.cards.length);
        for (var n = 0; n < dto.cards.length; ++n) {
          expect(dto.cards[n].url, testModel3Vault.cards[n].url);
          expect(dto.cards[n].username, testModel3Vault.cards[n].username);
          expect(dto.cards[n].password, testModel3Vault.cards[n].password);
          expect(dto.cards[n].label, testModel3Vault.cards[n].label);
          expect(dto.cards[n].icon, testModel3Vault.cards[n].icon);
          expect(dto.cards[n].other, testModel3Vault.cards[n].other);
        }
      });

      test('toModel works', () {
        final model = modelmapper.toModelVault(testDto3Vault);

        expect(model.cards.length, testDto3Vault.cards.length);
        for (var n = 0; n < model.cards.length; ++n) {
          expect(model.cards[n].url, testDto3Vault.cards[n].url);
          expect(model.cards[n].username, testDto3Vault.cards[n].username);
          expect(model.cards[n].password, testDto3Vault.cards[n].password);
          expect(model.cards[n].label, testDto3Vault.cards[n].label);
          expect(model.cards[n].icon, testDto3Vault.cards[n].icon);
          expect(model.cards[n].other, testDto3Vault.cards[n].other);
        }
      });
    });
  });
}
