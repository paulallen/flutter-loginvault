import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:loginvault/services/Biometrics.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  final biometrics = Biometrics();

  WidgetsFlutterBinding.ensureInitialized();

  group('Biometrics()', () {
    test('canUseBiometrics() returns false', () async {
      final canUse = await biometrics.canUseBiometrics();

      expect(canUse, false);
    });

    test('listAvailableBiometricTypes() is empty', () async {
      final list = await biometrics.listAvailableBiometricTypes();

      expect(list, []);
    });

    test('authenticate() returns false', () async {
      final isAuthenticated = await biometrics.authenticate(
        reason: 'test',
      );

      expect(isAuthenticated, false);
    });
  });
}
