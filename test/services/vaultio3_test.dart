import 'package:flutter_test/flutter_test.dart';

import 'package:loginvault/services/VaultIo3.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  const testFilePath = 'build/vaultio3_test_datafile.txt';

  final vaultio3 = VaultIo3();

  group('VaultIo3', () {
    test('load V3 works', () async {
      final vault = await vaultio3.loadVault('test/example_v3.lv1', 'example');

      expect(vault, isNotNull);
      expect(vault.cards.length, 13);
    });

    test('load V2 works', () async {
      final vault = await vaultio3.loadVault('test/example_v2.lv1', 'example');

      expect(vault, isNotNull);
      expect(vault.cards.length, 12);
    });

    test('load V1 works', () async {
      final vault = await vaultio3.loadVault('test/example_v1.lv1', 'example');

      expect(vault, isNotNull);
      expect(vault.cards.length, 11);
    });

    test('saveVault() works', () async {
      await vaultio3.saveVault(testFilePath, testModel3Vault, 'saveme');

      final vault = await vaultio3.loadVault(testFilePath, 'saveme');
      expect(vault, testModel3Vault);

      await vaultio3.deleteVault(testFilePath);
    });
  });
}
