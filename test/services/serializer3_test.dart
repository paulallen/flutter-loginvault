import 'package:flutter_test/flutter_test.dart';

import 'package:loginvault/services/Serializer3.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data3.dart';

void main() {
  logger.minLevel = Level.warning;

  final serializer = Serializer3();

  group('Serializer3', () {
    group('round trip', () {
      test('works', () {
        final json = serializer.serialize(testDto3Vault);
        final vault = serializer.deserialize(json);

        expect(vault.cards.length, testDto3Vault.cards.length);
        for (var n = 0; n < vault.cards.length; ++n) {
          final c = vault.cards[n];
          final tc = testDto3Vault.cards[n];

          expect(c.url, tc.url);
          expect(c.username, tc.username);
          expect(c.password, tc.password);
          expect(c.label, tc.label);
          expect(c.icon, tc.icon);
          expect(c.other, tc.other);
          expect(c.isDeleted, tc.isDeleted);
        }
      });
    });
  });
}
