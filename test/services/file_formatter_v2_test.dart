import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/services/FileFormatterV2.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  const shortPayload = 'BERTBERTBERT';
  const longPayload = 'Lorem ipsum dolor sit amet, '
      'consectetur adipiscing elit, sed do eiusmod tempor incididunt '
      'ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis '
      'nostrud exercitation ullamco laboris nisi ut aliquip ex ea '
      'commodo consequat. Duis aute irure dolor in reprehenderit in '
      'voluptate velit esse cillum dolore eu fugiat nulla pariatur. '
      'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui '
      'officia deserunt mollit anim id est laborum.';

  final formatter = FileFormatterV2();

  group('FileFormatterV2', () {
    group('format()', () {
      test('works roughly as expected', () {
        final content = formatter.format(shortPayload);

        expect(content.startsWith('====='), isTrue);
        expect(content.contains(shortPayload), isTrue);
        expect(content.endsWith('=====\n'), isTrue);
      });

      test('splits long lines', () {
        final content = formatter.format(longPayload);

        final lines = content.split('\n');
        for (var n = 1; n < lines.length - 3; ++n) {
          expect(lines[n].length, 64);
        }
      });
    });

    group('parse()', () {
      test('works roughly as expected', () {
        const content =
            "${FileFormatterV2.v2Header}\n$shortPayload\n${FileFormatterV2.v2Footer}\n";
        final payload = formatter.parse(content);

        expect(payload, shortPayload);
      });

      test('handles DOS newlines', () {
        const content =
            "${FileFormatterV2.v2Header}\r\n$shortPayload\r\n${FileFormatterV2.v2Footer}\r\n";

        final payload = formatter.parse(content);

        expect(payload, shortPayload);
      });
    });

    group('Round trip', () {
      test('works', () {
        final content = formatter.format(longPayload);
        final extracted = formatter.parse(content);

        expect(extracted, longPayload);
      });
    });
  });
}
