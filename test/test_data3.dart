import 'package:loginvault/dtos3/DtoCard3.dart';
import 'package:loginvault/dtos3/DtoVault3.dart';
import 'package:loginvault/models3/Vault3.dart';
import 'package:loginvault/models3/XCard3.dart';

final testDto3Card1 = DtoCard3()
  ..url = 'url1'
  ..username = 'username1'
  ..password = 'password1'
  ..label = 'label1'
  ..icon = 'icon1'
  ..other = 'other1'
  ..isDeleted = false;

final testDto3Card2 = DtoCard3()
  ..url = 'url2'
  ..username = 'username2'
  ..password = 'password2'
  ..label = 'label1'
  ..icon = 'icon2'
  ..other = 'other2'
  ..isDeleted = true;

final testDto3Card3 = DtoCard3()
  ..url = 'url3'
  ..username = 'username3'
  ..password = 'password3'
  ..label = 'label2'
  ..icon = 'icon3'
  ..other = 'other3'
  ..isDeleted = false;

final testDto3Card4 = DtoCard3()
  ..url = 'url4'
  ..username = 'username4'
  ..password = 'password4'
  ..label = 'label2'
  ..icon = 'icon4'
  ..other = 'other4'
  ..isDeleted = true;

final testDto3Vault = DtoVault3([
  testDto3Card1,
  testDto3Card2,
  testDto3Card3,
  testDto3Card4,
]);

final testModel3Card1 = XCard3()
  ..url = testDto3Card1.url
  ..username = testDto3Card1.username
  ..password = testDto3Card1.password
  ..label = testDto3Card1.label
  ..other = testDto3Card1.other
  ..isDeleted = testDto3Card1.isDeleted;

final testModel3Card2 = XCard3()
  ..url = testDto3Card2.url
  ..username = testDto3Card2.username
  ..password = testDto3Card2.password
  ..label = testDto3Card2.label
  ..other = testDto3Card2.other
  ..isDeleted = testDto3Card2.isDeleted;

final testModel3Card3 = XCard3()
  ..url = testDto3Card3.url
  ..username = testDto3Card3.username
  ..password = testDto3Card3.password
  ..label = testDto3Card3.label
  ..other = testDto3Card3.other
  ..isDeleted = testDto3Card3.isDeleted;

final testModel3Card4 = XCard3()
  ..url = testDto3Card4.url
  ..username = testDto3Card4.username
  ..password = testDto3Card4.password
  ..label = testDto3Card4.label
  ..other = testDto3Card4.other
  ..isDeleted = testDto3Card4.isDeleted;

final testModel3Vault = Vault3(cards: [
  testModel3Card1,
  testModel3Card2,
  testModel3Card3,
  testModel3Card4,
]);
