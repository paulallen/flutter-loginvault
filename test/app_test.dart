import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/app/App.dart';
import 'package:loginvault/services/Session3.dart';
import 'package:loginvault/utils/Logger.dart';

void main() {
  logger.minLevel = Level.warning;

  group('app', () {
    testWidgets('starts', (WidgetTester tester) async {
      Session3.instance.initState('', const HSVColor.fromAHSV(1, 1, 1, 1));
      const app = App();
      await tester.pumpWidget(app);
      await tester.pumpAndSettle();

      expect(app, isNotNull);

      // final titleFinder = find.text('LoginVault');
      // expect(titleFinder, findsWidgets);
    });
  });
}
