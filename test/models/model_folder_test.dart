import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/models/Folder.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data.dart';

String changed(String original) => 'xx-$original-xx';

void main() {
  logger.minLevel = Level.warning;

  group('isSameAs()', () {
    test('works', () {
      final copy = Folder(
        name: testModelFolder1.name,
        cards: testModelFolder1.cards,
      );

      final isSame = copy == testModelFolder1;

      expect(isSame, isTrue);
    });

    test('spots name', () {
      final copy = Folder(
        name: changed(testModelFolder1.name),
        cards: testModelFolder1.cards,
      );

      final isSame = copy == testModelFolder1;

      expect(isSame, isFalse);
    });

    test('spots cards removed', () {
      final copy = Folder(
        name: testModelFolder1.name,
        cards: [],
      );

      final isSame = copy == testModelFolder1;

      expect(isSame, isFalse);
    });
  });

  test('clone() works', () {
    final copy = testModelFolder1.clone();

    expect(copy.hashCode == testModelFolder1.hashCode, isFalse);
    expect(copy.name, testModelFolder1.name);
    expect(copy.cards.length, testModelFolder1.cards.length);
  });

  test('toString() works', () {
    final string = testModelFolder1.toString();

    expect(
        string, '{Folder name: ${testModelFolder1.name}, cards: ${testModelFolder1.cards.length}}');
  });
}
