import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/models/Vault.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data.dart';

String changed(String original) => 'xx-$original-xx';

void main() {
  logger.minLevel = Level.warning;

  group('isSameAs()', () {
    test('works', () {
      final copy = Vault(
        name: testModelVault.name,
        folders: testModelVault.folders,
      );

      final isSame = copy == testModelVault;

      expect(isSame, isTrue);
    });

    test('spots folders removed', () {
      final copy = Vault(
        folders: [],
      );

      final isSame = copy == testModelVault;

      expect(isSame, isFalse);
    });
  });

  test('clone() works', () {
    final copy = testModelVault.clone();

    expect(copy.hashCode == testModelVault.hashCode, isFalse);
    expect(copy.folders.length, testModelVault.folders.length);
  });

  test('toString() works', () {
    final string = testModelVault.toString();

    expect(
      string,
      '{Vault name: ${testModelVault.name}, folders: ${testModelVault.folders.length}}',
    );
  });
}
