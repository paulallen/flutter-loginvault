import 'package:flutter_test/flutter_test.dart';
import 'package:loginvault/models/XCard.dart';
import 'package:loginvault/utils/Logger.dart';

import '../test_data.dart';

String changed(String original) => 'xx-$original-xx';

void main() {
  logger.minLevel = Level.warning;

  group('equality()', () {
    test('works', () {
      final copy = XCard()
        ..url = testModelCard1.url
        ..username = testModelCard1.username
        ..password = testModelCard1.password
        ..other = testModelCard1.other;

      final isSame = copy == testModelCard1;

      expect(isSame, isTrue);
    });

    test('spots url', () {
      final copy = XCard()
        ..url = changed(testModelCard1.url)
        ..username = testModelCard1.username
        ..password = testModelCard1.password
        ..other = testModelCard1.other;

      final isSame = copy == testModelCard1;

      expect(isSame, isFalse);
    });

    test('spots username', () {
      final copy = XCard()
        ..url = testModelCard1.url
        ..username = changed(testModelCard1.username)
        ..password = testModelCard1.password
        ..other = testModelCard1.other;

      final isSame = copy == testModelCard1;

      expect(isSame, isFalse);
    });

    test('spots password', () {
      final copy = XCard()
        ..url = testModelCard1.url
        ..username = testModelCard1.username
        ..password = changed(testModelCard1.password)
        ..other = testModelCard1.other;

      final isSame = copy == testModelCard1;

      expect(isSame, isFalse);
    });

    test('spots other', () {
      final copy = XCard()
        ..url = testModelCard1.url
        ..username = testModelCard1.username
        ..password = testModelCard1.password
        ..other = changed(testModelCard1.other);

      final isSame = copy == testModelCard1;

      expect(isSame, isFalse);
    });
  });

  test('clone() works', () {
    final copy = testModelCard1.clone();

    expect(copy.hashCode == testModelCard1.hashCode, isFalse);
    expect(copy.url, testModelCard1.url);
    expect(copy.username, testModelCard1.username);
    expect(copy.password, testModelCard1.password);
    expect(copy.other, testModelCard1.other);
  });

  test('toString() works', () {
    final string = testModelCard1.toString();

    expect(string,
        '{XCard url: ${testModelCard1.url}, username: ${testModelCard1.username}, password: ********, other: ${testModelCard1.other}}');
  });
}
