
[![pipeline status](https://gitlab.com/paulallen/flutter-loginvault/badges/develop/pipeline.svg)]() [![coverage report](https://gitlab.com/paulallen/flutter-loginvault/badges/develop/coverage.svg)](https://paulallen.gitlab.io/flutter-loginvault/coverage)

# loginvault

A simple password manager using only local storage.

- Multiple vaults, holdings username and passwords.
- Data encrypted using 256bit AES.
- One master password per vault.
- Unlocked with fingerprint or FaceID.
- Organize by folder.
- Comprehensive search.
- Double-enter data blind.
- Copy password without display.
- No network access at all.
